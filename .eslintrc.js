module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: ['import'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
        alias: {
          "@Components": "./src/components",
          "@Containers": "./src/containers",
          "@Firebase": "./src/firebase",
          "@Helpers": "./src/helpers",
          "@Redux": "./src/redux",
          "@Translations": "./src/Translations",
          "@Navigators": "./src/navigators"
        }
      }
    }
  },
  rules: { 
    "react-native/no-inline-styles": 0,
    "react/no-did-mount-set-state": 0 
  },
};