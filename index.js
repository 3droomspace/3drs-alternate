import {gestureHandlerRootHOC} from 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {typography} from './src/components/Utils/typography';

typography();
AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(App));
