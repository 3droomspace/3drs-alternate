# 3D Room Space

Please follow the react-native environment installation guide for react-native-cli before you start

DO NOT USE EXPO

https://reactnative.dev/docs/environment-setup

```
brew install node
brew install watchman
```

1. Install Cocoapods
2. ```cd threedrs```
3. ```npm install && cd ios && pod install && pod update```
5. ```cd ..```
6. ```npm start```
7. Open ListingApp.xcworkspace in /ios folder from Finder
8. Run on xcode for iOS
9. Open /android folder in Android Studio
10. Run on Android

All new commits must be made on a separate branch.

