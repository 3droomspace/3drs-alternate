import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import ServerConfiguration from '/ServerConfiguration';

const bookingRef = firebase
  .firestore()
  .collection(ServerConfiguration.database.collection.BOOKINGS);

export const addBooking = data => {
  const {
    message,
    pets,
    startDate,
    endDate,
    occupants,
    userID,
    listingID,
    ethTxn,
    docusignPDF,
  } = data;
  bookingRef
    .add({
      message: message,
      pets,
      startDate,
      endDate,
      occupants,
      userID,
      listingID,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      ethTxn, //eth transaction
      docusignPDF,
      status: 'Requested',
    })
    .then(docRef => {
      if (docRef.id) {
        bookingRef.doc(docRef.id).update({
          id: docRef.id,
        });
      }
      // callback({success: true});
    })
    .catch(error => {
      console.log(error);
      // callback({
      //   success: false,
      // });
    });
};

export const subscribeBookings = (listingID, callback) => {
  if (listingID) {
    return bookingRef
      .where('listingID', '==', listingID)
      .onSnapshot(querySnapshot => callback(querySnapshot));
  }
};
