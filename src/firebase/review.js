import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import ServerConfiguration from '/ServerConfiguration';

const usersRef = firebase.firestore().collection('users');

const reviewsListingsRef = firebase
  .firestore()
  .collection(ServerConfiguration.database.collection.REVIEWS);
const reviewsUsersRef = firebase
  .firestore()
  .collection(ServerConfiguration.database.collection.REVIEWS_USERS);
const listingsRef = firebase
  .firestore()
  .collection(ServerConfiguration.database.collection.LISTINGS);

const reviewsRef = field => {
  return field === 'listingID' ? reviewsListingsRef : reviewsUsersRef;
};

const targetRef = field => {
  return field === 'listingID' ? listingsRef : usersRef;
};

export const subscribeReviews = options => (id, callback) => {
  return reviewsRef(options.field)
    .where(options.field, '==', id)
    .onSnapshot(querySnapshot => callback(querySnapshot, usersRef));
};

export const postReview = options => (
  user,
  data,
  starCount,
  content,
  callback,
) => {
  reviewsRef(options.field)
    .add({
      authorID: user.id,
      [options.field]: data.id,
      starCount,
      content: content,
      firstName: user.firstName,
      lastName: user.lastName,
      profilePictureURL: user.profilePictureURL,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    })
    .then(docRef => {
      reviewsRef(options.field)
        .where(options.field, '==', data.id)
        .get()
        .then(reviewQuerySnapshot => {
          let totalStarCount = 0,
            count = 0;
          reviewQuerySnapshot.forEach(reviewDoc => {
            const review = reviewDoc.data();

            totalStarCount += review.starCount;
            count++;
          });

          if (count > 0) {
            data.starCount = totalStarCount / count;
          } else {
            data.starCount = 0;
          }

          targetRef(options.field)
            .doc(data.id)
            .set(data, {
              merge: true,
            });
          callback({
            success: true,
          });
        });
    })
    .catch(error => {
      console.log(error);
      callback({
        success: false,
      });
    });
};
