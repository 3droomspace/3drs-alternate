import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import '@react-native-firebase/storage';
import {Platform} from 'react-native';
import {ErrorCode} from '@Containers/onboarding/utils/ErrorCode';

export const uploadFileWithProgressTracking = async (
  filename,
  uploadUri,
  callbackSuccess,
  callbackError,
) => {
  // Success handler with SUCCESS is called multiple times on Android. We need work around that to ensure we only call it once
  var finished = false;
  firebase
    .storage()
    .ref(filename)
    .putFile(uploadUri)
    .on(
      firebase.storage().TaskEvent.STATE_CHANGED,
      snapshot => {
        if (snapshot.state === firebase.storage().TaskState.SUCCESS) {
          if (finished === true) {
            return;
          }
          finished = true;
        }
        callbackSuccess(snapshot, firebase.storage().TaskState.SUCCESS);
      },
      callbackError,
    );
};

export const uploadImage = uri => {
  return new Promise((resolve, _reject) => {
    const filename = uri.substring(uri.lastIndexOf('/') + 1);

    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
    firebase
      .storage()
      .ref(filename)
      .putFile(uploadUri)
      .then(() => {
        firebase
          .storage()
          .ref(filename)
          .getDownloadURL()
          .then(url =>
            resolve({
              downloadURL: url,
            }),
          )
          .catch(_error => {
            console.log('_error', _error);
            resolve({
              error: ErrorCode.photoUploadFailed,
            });
          });
      })
      .catch(_error => {
        console.log('_error', _error);
        resolve({
          error: ErrorCode.photoUploadFailed,
        });
      });
  });
};
