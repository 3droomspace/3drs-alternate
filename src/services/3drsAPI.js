import apisauce from 'apisauce';

export const baseURL =
  'https://us-central1-d-room-space-firebase.cloudfunctions.net/';

const req = apisauce.create({
  baseURL,
  timeout: 15000,
});

const apiCreatePaymentIntent = reqObj =>
  req.post('createPaymentIntent', reqObj);

const apiCreateCard = reqObj => req.post('createCard', reqObj);

const apiListCards = reqObj => req.post('listCard', reqObj);

const apiListPaymentMethods = reqObj => req.post('listPaymentMethods', reqObj);

export {
  apiCreatePaymentIntent,
  apiCreateCard,
  apiListCards,
  apiListPaymentMethods,
};
