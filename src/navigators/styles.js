import {DynamicStyleSheet} from 'react-native-dark-mode';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: appStyles.colorSet.whiteSmoke,
    },
  });
};

export default dynamicStyles;
