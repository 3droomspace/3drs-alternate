import {StyleSheet} from 'react-native';
import React from 'react';
import {connect} from 'react-redux';
import {createReduxContainer} from 'react-navigation-redux-helpers';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import HomeScreen from '@Containers/HomeScreen/HomeScreen';
import AdminDashboardScreen from '@Containers/AdminDashboardScreen/AdminDashboardScreen';
import DetailScreen from '@Containers/DetailScreen/DetailScreen';
import BookingDetailsScreen from '@Containers/BookingDetailsScreen/BookingDetailsScreen';
import MapScreen from '@Containers/MapScreen/MapScreen';
import ManagerCalendar from '@Components/ManagerCalendar';
import ManagerDashboard from '@Components/ManagerDashboard';
import SavedListingScreen from '@Containers/SavedListingScreen/SavedListingScreen';
import ConversationsScreen from '@Containers/ConversationsScreen/ConversationsScreen';
import IDVerificationScreen from '@Containers/IdVerification/IDVerificationScreen';
import DynamicAppStyles from '/DynamicAppStyles';
import {
  LoadScreen,
  WalkthroughScreen,
  LoginScreen,
  SignupScreen,
  SmsAuthenticationScreen,
  PromptScreen,
  SetupProfileScreen,
  ForgotPasswordScreen,
} from '@Containers/onboarding';
import DrawerContainer from '@Components/DrawerContainer';
import MyProfileScreen from '@Components/MyProfileScreen';
import MyListingModal from '@Components/MyListingModal';
import ListingProfileModal from '@Components/ListingProfileModal';
import ListingAppConfig from '/ListingAppConfig';
import {tabBarBuilder} from '@Components/TabBar/TabBar';
import {IMChatScreen} from '@Containers/chat';
import PostModal from '@Components/PostModal';
import AddButton from '@Components/AddButton';
import {
  IMEditProfileScreen,
  IMUserSettingsScreen,
  IMContactUsScreen,
  CurrentResidenceTab,
  HistoryTab,
  MyWalletScreen,
} from '@Containers/profile';
import MyBookingScreen from '@Components/MyBookingScreen';
import Error404Screen from '@Components/Error404Screen';
import PayScreen from '@Containers/PayScreen/PayScreen';
// import PaymentScreen from '@Containers/PayScreen/PaymentScreen';
// import CardListScreen from '@Containers/PayScreen/CardListScreen';
// import AddCardScreen from '@Containers/PayScreen/AddCardScreen';
import ApprovedDocusign from '@Containers/ApprovedDocusign/ApprovedDocusign';

import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import ViewPagerAdapter from 'react-native-tab-view-viewpager-adapter';
// login stack
const LoginStack = createStackNavigator(
  {
    Welcome: {
      screen: WalkthroughScreen,
      navigationOptions: {header: null},
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: () => ({
        headerStyle: authScreensStyles.headerStyle,
      }),
    },
    Signup: {
      screen: SignupScreen,
      navigationOptions: () => ({
        headerStyle: authScreensStyles.headerStyle,
      }),
    },
    Prompt: {
      screen: PromptScreen,
      navigationOptions: {header: null},
    },
    SetupProfile: {
      screen: SetupProfileScreen,
      navigationOptions: {header: null},
    },
    Sms: {
      screen: SmsAuthenticationScreen,
      navigationOptions: () => ({
        headerStyle: authScreensStyles.headerStyle,
      }),
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: {header: null},
    },
  },
  {
    initialRouteName: 'Welcome',
    initialRouteParams: {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    },
    headerMode: 'none',
    cardShadowEnabled: false,
  },
);

const HomeStack = createStackNavigator(
  {
    Landing: {screen: HomeScreen},
    Detail: {
      screen: DetailScreen,
    },
    PersonalChat: {screen: IMChatScreen},
    Map: {screen: MapScreen},
    ListingProfileModal: {screen: ListingProfileModal},
    ListingProfileModalDetailsScreen: {screen: DetailScreen},
    // PayScreen: {screen: PayScreen},
    //
    MyProfile: {screen: MyProfileScreen},
    MyListingModal: {screen: MyListingModal},
    ManagerCalendar: {screen: ManagerCalendar},
    ManagerDashboard: {screen: ManagerDashboard},
    MyListingDetailModal: {screen: DetailScreen},
    BookingDetails: {screen: BookingDetailsScreen},
    // PersonalChat: { screen: IMChatScreen },
    Contact: {screen: IMContactUsScreen},
    Settings: {screen: IMUserSettingsScreen},
    AdminDashboard: {screen: AdminDashboardScreen},
    AccountDetail: {screen: IMEditProfileScreen},
    ApprovedDocusign: {screen: ApprovedDocusign},
    //
  },
  {
    initialRouteName: 'Map',
    headerMode: 'float',
    headerLayoutPreset: 'left',
    navigationOptions: ({navigation}) => ({
      headerTintColor: 'black',
      headerTitleStyle: {
        fontFamily: 'CircularStd-Book',
        fontSize: 30,
        color: '#383838',
        fontWeight: '100',
      },
    }),
  },
);

HomeStack.navigationOptions = ({navigation}) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      if (route.routeName === 'PersonalChat') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }
  return {
    tabBarVisible,
  };
};

const SavedCollection = createStackNavigator(
  {
    SavedListing: {screen: SavedListingScreen},
    Detail: {screen: DetailScreen},
    ListingProfileModalDetailsScreen: {screen: DetailScreen},
    PersonalChat: {screen: IMChatScreen},
    ListingProfileModal: {screen: ListingProfileModal},
  },
  {
    initialRouteName: 'SavedListing',
    headerMode: 'float',
    headerLayoutPreset: 'center',
    navigationOptions: ({navigation}) => ({
      headerTintColor: '#0394fc',
      headerTitleStyle: styles.headerTitleStyle,
    }),
  },
);

const BookingCollection = createStackNavigator(
  {
    Booking: {screen: MyBookingScreen},
    Detail: {screen: DetailScreen},
    ListingProfileModalDetailsScreen: {screen: DetailScreen},
    PersonalChat: {screen: IMChatScreen},
    ListingProfileModal: {screen: ListingProfileModal},
    Map: {screen: MapScreen},
  },
  {
    initialRouteName: 'Booking',
    headerMode: 'float',
    headerLayoutPreset: 'center',
    navigationOptions: ({navigation}) => ({
      headerTintColor: '#0394fc',
      headerTitleStyle: styles.headerTitleStyle,
    }),
  },
);

const MessageStack = createStackNavigator(
  {
    Message: {
      screen: ConversationsScreen,
      navigationOptions: {
        header: null,
      },
    },
    PersonalChat: {
      screen: IMChatScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Message',
    headerMode: 'float',
    initialRouteParams: {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    },
    headerLayoutPreset: 'center',
    navigationOptions: ({navigation}) => ({
      headerTintColor: '#0394fc',
      header: null,
      headerTitleStyle: styles.headerTitleStyle,
    }),
  },
);

const SwipeStack = createMaterialTopTabNavigator(
  {
    'Current Residence': {
      name: 'Residence',
      screen: CurrentResidenceTab,
    },
    History: {
      name: 'History',
      screen: HistoryTab,
    },
  },
  {
    pagerComponent: ViewPagerAdapter,
    labelStyle: {
      fontSize: 12,
    },
    tabStyle: {
      width: 100,
    },
    style: {
      backgroundColor: 'tranparent',
    },
    tabBarOptions: {
      upperCaseLabel: false,
      tabStyle: {
        backgroundColor: 'transparent',
      },
      style: {
        backgroundColor: 'transparent',
        borderBottomWidth: 1,
        borderBottomColor: '#dad9e2',
      },
      activeTintColor: '#44d7b6',
      inactiveTintColor: '#dad9e2',
      labelStyle: {
        fontSize: 20,
      },
      indicatorStyle: {
        backgroundColor: '#44d7b6',
      },
    },
  },
);

const ProfileStack = createStackNavigator(
  {
    // Prompt: {screen: PromptScreen},
    // SetupProfile: {screen: SetupProfileScreen},
    MyProfile: {screen: MyProfileScreen},
    IdVerification: {screen: IDVerificationScreen},
    MyListingModal: {screen: MyListingModal},
    ManagerCalendar: {screen: ManagerCalendar},
    ManagerDashboard: {screen: ManagerDashboard},
    MyListingDetailModal: {screen: DetailScreen},
    BookingDetails: {screen: BookingDetailsScreen},
    PersonalChat: {screen: IMChatScreen},
    Contact: {screen: IMContactUsScreen},
    Settings: {screen: IMUserSettingsScreen},
    AdminDashboard: {screen: AdminDashboardScreen},
    AccountDetail: {screen: IMEditProfileScreen},
    RentalHistory: {
      screen: SwipeStack,
      navigationOptions: {
        headerLeft: null,
        title: 'Rental History',
      },
    },
    MyWallet: {
      screen: MyWalletScreen,
      navigationOptions: {
        headerLeft: null,
        title: 'My Wallet',
      },
    },
    // PaymentScreen: {
    //   screen: PaymentScreen,
    //   navigationOptions: {
    //     header: null,
    //   },
    // },
    // CardListScreen: {
    //   screen: CardListScreen,
    //   navigationOptions: {
    //     header: null,
    //   },
    // },
    // AddCardScreen: {
    //   screen: AddCardScreen,
    //   navigationOptions: {
    //     header: null,
    //   },
    // },
  },
  {
    initialRouteName: 'MyProfile',
    headerMode: 'float',
  },
);

const Error404Stack = createStackNavigator(
  {
    error404: {screen: Error404Screen},
  },
  {
    initialRouteName: 'error404',
    headerMode: 'none',
    navigationOptions: ({navigation}) => ({
      headerTintColor: '#0394fc',
      headerTitleStyle: styles.headerTitleStyle,
    }),
  },
);

const TabNavigator = createBottomTabNavigator(
  {
    MenuDrawer: {screen: Error404Stack},
    Profile: {screen: ProfileStack},
    Home: {screen: HomeStack},
    Bookings: {screen: BookingCollection},
    Messages: {screen: MessageStack},
    Favorites: {screen: SavedCollection},
  },
  {
    initialRouteName: 'Home',
    tabBarComponent: tabBarBuilder(ListingAppConfig.tabIcons, DynamicAppStyles),
    initialRouteParams: {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    },
    navigationOptions: ({navigation}) => {
      const {routeName} = navigation.state.routes[navigation.state.index];
      return {
        headerTitle: routeName,
        header: null,
      };
    },
  },
);

const MainNavigator = createDrawerNavigator(
  {
    Tab: TabNavigator,
  },
  {
    // drawerPosition: 'left',
    initialRouteName: 'Tab',
    drawerType: 'slide',
    // swipeEnabled: false,
    // drawerWidth: 300,
    contentComponent: DrawerContainer,
    // headerMode: 'screen',
    navigationOptions: ({navigation}) => {
      const routeIndex = navigation.state.index;

      return {
        title: navigation.state.routes[routeIndex].key,
        header: null,
        headerBackTitle: null,
      };
    },
  },
);

const RootNavigator = createSwitchNavigator(
  {
    LoadScreen,
    LoginStack,
    MainStack: MainNavigator,
  },
  {
    initialRouteName: 'LoadScreen',
    initialRouteParams: {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    },
    headerMode: 'none',
  },
);

const styles = StyleSheet.create({
  headerTitleStyle: {
    fontFamily: 'CircularStd-Book',
    fontSize: 30,
    color: '#383838',
    fontWeight: '100',
  },
  bottomTab: {
    height: 150,
  },
});

const authScreensStyles = StyleSheet.create({
  headerStyle: {
    borderBottomWidth: 0,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    elevation: 0, // remove shadow on Android
  },
});

const AppContainer = createReduxContainer(RootNavigator);
const mapStateToProps = state => ({
  state: state.nav,
});
const AppNavigator = connect(mapStateToProps)(AppContainer);

export {RootNavigator, AppNavigator};
