import {IMLocalized} from '@Containers/localization/IMLocalization';
import {setI18nConfig} from '@Containers/localization/IMLocalization';

setI18nConfig();

const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;

const ListingAppConfig = {
  isSMSAuthEnabled: true,
  appIdentifier: 'rn-real-estate-android',
  onboardingConfig: {
    welcomeTitle: IMLocalized('3D Room Space'),
    welcomeCaption: IMLocalized('A solution for medium-term rentals'),
    walkthroughScreens: [
      {
        icon: require('../assets/icons/Splash Screen - End.png'),
        title: IMLocalized('Find your new home'),
        description: IMLocalized(
          'Search from houses, apartments and commercial units',
        ),
      },
      {
        icon: require('../assets/icons/map2.png'),
        title: IMLocalized('Medium Term Leases'),
        description: IMLocalized('A new way to find a place to live'),
      },
      {
        icon: require('../assets/icons/tenant.png'),
        title: IMLocalized('Putting the tenant first'),
        description: IMLocalized(
          'Take control of your data and have it work for your best interest',
        ),
      },
      {
        icon: require('../assets/icons/COMPLEXSEARCH.png'),
        title: IMLocalized('Complex Search Filters'),
        description: IMLocalized(
          'Custom dynamic filters to accommodate all markets and all customer needs.',
        ),
      },
      {
        icon: require('../assets/icons/addnewlisting.png'),
        title: IMLocalized('Add New Listings'),
        description: IMLocalized(
          'Add new listings directly from the app, including photo gallery and filters.',
        ),
      },
      {
        icon: require('../assets/icons/chat2.png'),
        title: IMLocalized('Chat'),
        description: IMLocalized(
          'Communicate with your renters and landlords in real-time.',
        ),
      },
      {
        icon: require('../assets/icons/notification2.png'),
        title: IMLocalized('Get Notified'),
        description: IMLocalized(
          'Stay on top of your game with real-time push notifications.',
        ),
      },
    ],
  },

  tabIcons: {
    MenuDrawer: {
      focus: 'bars',
      unFocus: 'bars',
    },
    Profile: {
      focus: 'user-circle',
      unFocus: 'user-circle',
    },
    Home: {
      focus: 'search',
      unFocus: 'search',
    },
    Messages: {
      focus: 'comment-dots',
      unFocus: 'comment-dots',
    },
    Favorites: {
      focus: 'heart',
      unFocus: 'heart',
    },
    // Profile: {
    //   focus: 'heart',
    //   unFocus: 'heart',
    // }
  },
  reverseGeoCodingAPIKey: 'AIzaSyCraOUykoNdoA9TFp3gg1UbdQltGS9uRk8',
  tosLink: 'http://www.3droomspace.com/terms.pdf',
  ppLink: 'http://www.3droomspace.com/privacy.pdf',
  editProfileFields: {
    sections: [
      {
        title: IMLocalized('PUBLIC PROFILE'),
        fields: [
          {
            displayName: IMLocalized('First Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'firstName',
            placeholder: 'Your first name',
          },
          {
            displayName: IMLocalized('Last Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'lastName',
            placeholder: 'Your last name',
          },
          {
            displayName: IMLocalized('Bio'),
            type: 'text',
            multiline: true,
            numberOfLines: 3,
            editable: true,
            key: 'bio',
            placeholder: 'Your bio here',
          },
        ],
      },
      {
        title: IMLocalized('PRIVATE DETAILS'),
        fields: [
          {
            displayName: IMLocalized('E-mail Address'),
            type: 'text',
            editable: false,
            key: 'email',
            placeholder: 'Your email address',
          },
          {
            displayName: IMLocalized('Phone Number'),
            type: 'text',
            editable: true,
            regex: regexForPhoneNumber,
            key: 'phone',
            placeholder: 'Your phone number',
          },
          {
            displayName: IMLocalized('User Type'),
            type: 'select',
            editable: true,
            key: 'userType',
            displayOptions: ['Manager', 'Renter'],
            options: ['manager', 'renter'],
          },
        ],
      },
    ],
  },
  payFields: {
    sections: [
      {
        title: IMLocalized('Pay'),
        fields: [
          {
            displayName: IMLocalized('First Name'),
            type: 'text',
            value: '$1200 due by 9/16',
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('Pay now'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  userSettingsFields: {
    sections: [
      {
        title: IMLocalized('GENERAL'),
        fields: [
          {
            displayName: IMLocalized('Allow Push Notifications'),
            type: 'switch',
            editable: true,
            key: 'push_notifications_enabled',
            value: false,
          },
          {
            displayName: IMLocalized('Enable Face ID / Touch ID'),
            type: 'switch',
            editable: true,
            key: 'face_id_enabled',
            value: false,
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('Save'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  contactUsFields: {
    sections: [
      {
        title: IMLocalized('Contact 3DRS'),
        fields: [
          {
            displayName: IMLocalized('First Name'),
            type: 'text',
            editable: true,
            key: 'contacus',
            value: '',
            placeholder: 'Your first name',
          },
          {
            displayName: IMLocalized('Last Name'),
            value: '',
            type: 'text',
            editable: true,
            key: 'email',
            placeholder: 'Your Last Name',
          },
          {
            displayName: IMLocalized('E-mail'),
            value: '',
            type: 'text',
            editable: true,
            key: 'email',
            placeholder: 'Your email address',
          },
          {
            displayName: IMLocalized('Attachments'),
            value: '',
            type: 'text',
            editable: true,
            key: 'email',
            placeholder: 'Upload',
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('CONTACT US'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  contactUsPhoneNumber: '+16504859694',
  homeConfig: {
    mainCategoryID: 'tae6JC8GZZaKOSSaZmKA',
    mainCategoryName: 'Houses',
  },
};

export default ListingAppConfig;
