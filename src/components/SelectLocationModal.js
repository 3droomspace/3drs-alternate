import React from 'react';
import {Modal, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {AppStyles} from '/AppStyles';
import {Configuration} from '/Configuration';
import Geolocation from '@react-native-community/geolocation';
import Geocode from 'react-geocode';
import AutoComplete from './AutoComplete';
import GPSButton from './GPSButton';

// set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
Geocode.setApiKey('AIzaSyCraOUykoNdoA9TFp3gg1UbdQltGS9uRk8');

// set response language. Defaults to english.
Geocode.setLanguage('en');

// set response region. Its optional.
// A Geocoding request with region=es (Spain) will return the Spanish city.
Geocode.setRegion('us');

// Enable or disable logs. Its optional.
Geocode.enableDebug();

class SelectLocationModal extends React.Component {
  constructor(props) {
    super(props);
    const location = this.props.location;

    this.state = {
      latitude: parseFloat(location.latitude),
      longitude: parseFloat(location.longitude),
      latitudeDelta: Configuration.map.delta.latitude,
      longitudeDelta: Configuration.map.delta.longitude,
      showSearch: true,
      shouldUseOwnLocation: true, // Set this to false to hide the user's location
      address: this.props.address,
    };
  }

  componentDidMount() {
    if (this.state.shouldUseOwnLocation) {
      Geolocation.setRNConfiguration({authorizationLevel: 'always'});
      Geolocation.getCurrentPosition(
        position => {
          this.props.onChangeLocation(position.coords);
          console.log('current position');
          console.log(position.coords);
        },
        error => console.log(error),
        {enableHighAccuracy: false, timeout: 5000},
      );
    }
  }

  getPosition = () => {
    Geolocation.setRNConfiguration({
      authorizationLevel: 'always',
    });
    Geolocation.getCurrentPosition(
      position => {
        this.onChangeLocation(position.coords);
      },
      error => console.log(error),
      {
        enableHighAccuracy: false,
        timeout: 5000,
      },
    );
  };

  onDone = () => {
    this.props.onDone({
      latitude: parseFloat(this.state.latitude),
      longitude: parseFloat(this.state.longitude),
    });
  };

  onCancel = () => {
    this.props.onCancel();
  };

  onPress = event => {
    let location = {
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude,
    };
    this.props.onChangeLocation(location);
    this.setState({
      latitude: parseFloat(location.latitude),
      longitude: parseFloat(location.longitude),
    });
  };

  onRegionChange = region => {
    this.setState({
      latitude: parseFloat(region.latitude),
      longitude: parseFloat(region.longitude),
      latitudeDelta: region.latitudeDelta,
      longitudeDelta: region.longitudeDelta,
    });
  };

  onChangeLocation = location => {
    this.props.onChangeLocation(location);

    this.setState({
      latitude: parseFloat(location.latitude),
      longitude: parseFloat(location.longitude),
    });
  };

  onChangeText = text => {
    this.setState({
      location: text,
    });
  };

  getCoordFromAddress = () => {
    // Get latidude & longitude from address.
    Geocode.fromAddress(this.state.location).then(
      response => {
        const {lat, lng} = response.results[0].geometry.location;
        console.log(lat, lng);
      },
      error => {
        console.error(error);
      },
    );
  };

  render() {
    return (
      <Modal
        animationType="fade"
        transparent={false}
        onRequestClose={this.onCancel}>
        <View style={styles.body}>
          <MapView
            ref={map => (this.map = map)}
            onPress={this.onPress}
            style={styles.mapView}
            // onRegionChangeComplete={this.onRegionChange}
            region={{
              latitude: parseFloat(this.state.latitude),
              longitude: parseFloat(this.state.longitude),
              latitudeDelta: this.state.latitudeDelta,
              longitudeDelta: this.state.longitudeDelta,
            }}>
            <Marker
              draggable
              coordinate={{
                latitude: parseFloat(this.state.latitude),
                longitude: parseFloat(this.state.longitude),
              }}
              onDragEnd={this.onPress}
            />
          </MapView>
          <View
            style={{
              marginTop: 30,
              width: '95%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '90%', paddingTop: 20}}>
              <AutoComplete
                onChangeLocation={this.onChangeLocation}
                showSearch={this.state.showSearch}
                updateAddress={this.props.updateAddress}
              />
            </View>
            <TouchableOpacity
              onPress={this.onDone}
              //      style={{ postion: 'absolute', right: 0 }}
            >
              <View style={{padding: 10, marginTop: 30}}>
                <Text style={{fontSize: 18, color: '#1281dd'}}>Done</Text>
              </View>
            </TouchableOpacity>
          </View>
          <GPSButton getPosition={this.getPosition} />
          <Text style={styles.address}>{this.props.address}</Text>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    width: '100%',
    height: '100%',
  },
  rightButton: {
    // paddingRight: 10,
    // marginTop: 500
  },
  mapView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: AppStyles.color.grey,
  },
  address: {
    position: 'absolute',
    bottom: 30,
    color: 'black',
    backgroundColor: 'white',
    width: '75%',
    marginLeft: 30,
    borderWidth: 0.3,
    borderColor: 'gray',
    padding: 10,
    borderRadius: 10,
  },
});

export default SelectLocationModal;
