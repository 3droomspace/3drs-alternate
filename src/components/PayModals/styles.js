import {Dimensions, StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';

//======= Define constant sizes.
export const SCREEN_PADDING = 23;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const CARD_WIDTH = (Dimensions.get('screen').width - SCREEN_PADDING * 3) / 2;
const CARD_HEIGHT = 110;
const HEADER_HEIGHT = isIphoneX() ? 80 : 56;
const BOTTOM_TAB_HEIGHT = 100;
const BODY_OVERLAP = 12;
const BODY_HEIGHT = Dimensions.get('screen').height - HEADER_HEIGHT;
// BOTTOM_TAB_HEIGHT +
// BODY_OVERLAP;
//=======

const styles = StyleSheet.create({
  container: {
    flex: 1.0,
  },
  header: {
    height: HEADER_HEIGHT,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: isIphoneX() ? 24 : 0,
  },
  body: {
    width: SCREEN_WIDTH,

    height: BODY_HEIGHT,
    paddingHorizontal: SCREEN_PADDING,
    borderTopLeftRadius: BODY_OVERLAP,
    borderTopRightRadius: BODY_OVERLAP,
    backgroundColor: '#f4f4f4',
    paddingBottom: 30,
  },
  title1: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: '600',
    marginVertical: 16,
  },
  title2: {
    fontSize: 18,
    // marginVertical: 8,
    alignSelf: 'center',
  },
  title3: {
    fontSize: 16,
    color: '#6f6f6f',
    marginBottom: 8,
  },
  cardStyle: {
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    margin: 4,
    justifyContent: 'center',
  },
  cardText: {
    color: 'white',
    alignSelf: 'center',
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  recommendBtn: {
    marginLeft: 24,
    backgroundColor: 'white',
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    borderTopRightRadius: 6,
  },
  searchBox: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderRadius: 6,
  },
  inputText: {
    padding: 0,
    paddingLeft: 4,
    fontSize: 16,
  },
  btn: {
    marginTop: 24,
    backgroundColor: '#3bcdc5',
    borderRadius: 8,
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  btnText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    textTransform: 'uppercase',
  },
  inputCard: {
    padding: 20,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
    borderRadius: 12,
  },
  inputCardTitle: {
    fontSize: 14,
    width: 105,
  },
  inputCardText: {
    backgroundColor: 'white',
    padding: 0,
    paddingHorizontal: 4,
    fontSize: 14,
    borderRadius: 12,
    flex: 1,
  },
  placeHolder: {
    width: 25,
  },
});

export default styles;
