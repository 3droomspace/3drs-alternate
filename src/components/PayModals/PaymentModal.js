import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  Modal,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import commonStyles from './styles';
import stripe from 'tipsi-stripe';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {apiCreatePaymentIntent} from '../../services/3drsAPI';
import {getCustomerData} from '../../firebase/stripe_customers';
import {showAlert} from './utils';

export default function PaymentScreen(props) {
  const user = props.user;
  const pm = props.paymentMethod;
  const price = props.price;
  const [loading, setLoading] = useState(false);
  // const [amount, setAmount] = useState(price);

  const onLinkCard = async () => {
    // if (!amount || amount < 0) {
    //   showAlert('', 'Please enter correct amount');
    //   return;
    // }

    try {
      setLoading(true);
      // Retrieve stripe customer data;
      const stripeCustomerData = await getCustomerData(user.id);
      const stripeCustomerId = stripeCustomerData.data.customer_id;
      // console.log('stripeCustomerData ===>>>', stripeCustomerId);

      const intentReqObj = {
        customerId: stripeCustomerData.data.customer_id,
        amount: parseFloat(price) * 100,
        // paymentMethodId: paymentMethod.id,
        paymentMethodId: pm.id,
      };
      const res = await apiCreatePaymentIntent(intentReqObj);
      const paymentIntent = res.data;

      // console.log('paymentIntent ===>>>', paymentIntent);
      if (paymentIntent.status === 'requires_action') {
        const result = await stripe.confirmPaymentIntent({
          clientSecret: paymentIntent.client_secret,
        });
        if (result.status === 'succeeded') {
          showAlert('', 'Payment Successful!');
        }
      } else {
        showAlert('', 'Payment Successful!');
      }

      setLoading(false);
      props.onCloseBookModal(true);
    } catch (error) {
      showAlert('', error.toString());
      setLoading(false);
      props.onCloseBookModal(false);
    }
  };

  return (
    <Modal
      visible={true}
      animationType="slide"
      transparent={false}
      onRequestClose={this.onCancel}>
      <View style={commonStyles.container}>
        <LinearGradient
          style={commonStyles.header}
          colors={['#009aca', '#069dc9', '#aeefaa']}
          locations={[0.1, 0.2, 1]}
        />

        <View style={commonStyles.body}>
          <View style={[commonStyles.row, {justifyContent: 'space-between'}]}>
            <TouchableOpacity onPress={props.onCloseModal}>
              <MaterialIcon name={'arrow-back'} size={25} />
            </TouchableOpacity>

            <Text style={commonStyles.title1}>Make a payment</Text>
            <View style={commonStyles.placeHolder} />
          </View>

          <View style={styles.cardInformation}>
            <Text style={commonStyles.title2}>Card Information</Text>
            <ListItem
              title={'CARD NUMBER'}
              description={`XXXX ${pm.card.last4}`}
            />
            <ListItem
              title={'CARD OWNER'}
              description={pm.billing_details.name}
            />
            <ListItem
              title={'EXPIRE DATE'}
              description={`${pm.card.exp_month}/${pm.card.exp_year}`}
            />
          </View>

          <Card
            title={'Amount($)'}
            placeHolder={''}
            value={price}
            keyboardType={'numeric'}
            maxLength={5}
          />

          <TouchableOpacity
            style={commonStyles.btn}
            onPress={!loading ? onLinkCard : undefined}>
            {loading ? (
              <ActivityIndicator animating size="small" color={'white'} />
            ) : (
              <Text style={commonStyles.btnText}>Pay</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

function ListItem({title, description}) {
  return (
    <View style={[commonStyles.row, styles.infoItem]}>
      <Text>{`${title}: `}</Text>
      <Text>{description}</Text>
    </View>
  );
}

function Card({
  title,
  placeHolder,
  onChangeText,
  value,
  maxLength,
  keyboardType,
}) {
  return (
    <View style={commonStyles.inputCard}>
      <Text style={commonStyles.inputCardTitle}>{title}</Text>
      <TextInput
        placeholder={placeHolder}
        placeholderTextColor="#3a3a3a"
        style={commonStyles.inputCardText}
        value={value}
        onChangeText={onChangeText}
        maxLength={maxLength}
        keyboardType={keyboardType}
        autoFocus
        editable={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  cardInformation: {
    backgroundColor: 'white',
    padding: 20,
  },
  infoItem: {
    justifyContent: 'space-between',
    marginTop: 12,
  },
});
