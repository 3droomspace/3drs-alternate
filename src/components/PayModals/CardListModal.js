import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Modal,
  FlatList,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {apiListPaymentMethods} from '../../services/3drsAPI';
import {getCustomerData} from '../../firebase/stripe_customers';
import {showAlert} from './utils';
import commonStyles from './styles';
import PaymentModal from './PaymentModal';
import AddCardModal from './AddCardModal';

export default function CardListScreen(props) {
  const {user, price, isFromPay} = props;
  const [loading, setLoading] = useState(false);

  // Payment method list of stripe => By using this, we can get the all registered cards
  const [pmList, setPmList] = useState([]);
  // By using this, we can pay with pre saved card
  const [payMethod, setPayMethod] = useState([]);

  // Show MakePayment modal
  const [showPayModal, setShowPayModal] = useState(false);
  // Show AddCard Modal
  const [showAddModal, setShowAddModal] = useState(false);

  useEffect(() => {
    async function fetchData() {
      setLoading(true);

      try {
        // Retrieve stripe customer data;
        const sUserRes = await getCustomerData(user.id);
        const sCustomerId = sUserRes.data.customer_id;

        // Get the list of payment methods
        const pMListReqObj = {
          customerId: sCustomerId,
        };
        const pMListRes = await apiListPaymentMethods(pMListReqObj);
        if (!pMListRes.data.data) {
          showAlert(
            '',
            "It doesn't work with the older users\nPlease signup again.",
          );
          props.navigation.goBack();
        } else {
          // console.log('fetch data ===>>>', pMListRes.data.data);
          setPmList(pMListRes.data.data);
        }
      } catch (error) {
        // console.log('fetch error ===>>>', error);
      } finally {
        setLoading(false);
      }
    }
    fetchData();
  }, [props.navigation, user.id]);

  const onMakePayment = selIdex => {
    setPayMethod(pmList[selIdex]);
    setShowPayModal(true);
  };

  const onAddCard = () => {
    setShowAddModal(true);
  };

  const onCloseModal = async () => {
    setShowAddModal(false);
    setLoading(true);
    try {
      // Retrieve stripe customer data;
      const sUserRes = await getCustomerData(user.id);
      const sCustomerId = sUserRes.data.customer_id;

      // Get the list of payment methods
      const pMListReqObj = {
        customerId: sCustomerId,
      };
      const pMListRes = await apiListPaymentMethods(pMListReqObj);
      if (!pMListRes.data.data) {
        showAlert(
          '',
          "It doesn't work with the older users\nPlease signup again.",
        );
        props.navigation.goBack();
      } else {
        // console.log('fetch data ===>>>', pMListRes.data.data);
        setPmList(pMListRes.data.data);
      }
    } catch (error) {
      // console.log('fetch error ===>>>', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal
      visible={true}
      animationType="slide"
      transparent={false}
      onRequestClose={this.onCancel}>
      <View style={commonStyles.container}>
        <LinearGradient
          style={commonStyles.header}
          colors={['#009aca', '#069dc9', '#aeefaa']}
          locations={[0.1, 0.2, 1]}
        />

        {loading ? (
          <View style={styles.indicator}>
            <ActivityIndicator animating size="large" color={'black'} />
          </View>
        ) : (
          <View style={commonStyles.body}>
            <View style={[commonStyles.row, {justifyContent: 'space-between'}]}>
              <TouchableOpacity onPress={props.onCloseModal}>
                <MaterialIcon name={'arrow-back'} size={25} />
              </TouchableOpacity>

              <Text style={commonStyles.title1}>Cards List</Text>
              <View style={commonStyles.placeHolder} />
            </View>

            <View style={{flex: 1}}>
              <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                data={pmList}
                renderItem={({item, index}) => (
                  <Card
                    name={item.billing_details.name || ''}
                    lastNumber={item.card.last4}
                    key={'card key' + index}
                    onPress={() => onMakePayment(index)}
                    disabled={isFromPay}
                  />
                )}
                keyExtractor={item => `${item.id}`}
              />
            </View>

            {/** Show only this button on Profile page */}
            {(isFromPay || (!isFromPay && pmList.length === 0)) && (
              <TouchableOpacity style={commonStyles.btn} onPress={onAddCard}>
                <Text style={commonStyles.btnText}>Add new card</Text>
              </TouchableOpacity>
            )}
          </View>
        )}
      </View>

      {showPayModal && (
        <PaymentModal
          user={user}
          paymentMethod={payMethod}
          onCloseModal={() => setShowPayModal(false)}
          // Below props are for only booking modal.
          price={price}
          onCloseBookModal={props.onCloseBookModal} // By using this func, we can dismiss all opened modals
        />
      )}

      {showAddModal && <AddCardModal user={user} onCloseModal={onCloseModal} />}
    </Modal>
  );
}

function Card({name, lastNumber, onPress, disabled}) {
  return (
    <TouchableOpacity
      style={commonStyles.inputCard}
      onPress={onPress}
      disabled={disabled}>
      <View style={styles.cardIcon}>
        <FontAwesomeIcon name={'credit-card-alt'} size={25} />
      </View>

      <View>
        <Text>{`XXXX ${lastNumber}`}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardIcon: {
    width: 50,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
