import {Alert} from 'react-native';

export const showAlert = (title, body) => {
  Alert.alert(title, body, [{text: 'OK', onPress: () => {}}], {
    cancelable: true,
  });
};
