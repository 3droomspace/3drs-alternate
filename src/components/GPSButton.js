import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import DynamicAppStyles from '/DynamicAppStyles';

const GPSButton = props => {
  return (
    <TouchableOpacity onPress={props.getPosition} style={styles.getLocation}>
      <FastImage source={DynamicAppStyles.iconSet.gps} style={styles.gpsIcon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  getLocation: {
    position: 'absolute',
    bottom: 30,
    right: 10,
    backgroundColor: 'lightgray',
    padding: 10,
    borderRadius: 5,
  },
  gpsIcon: {
    width: 16,
    height: 16,
  },
});

export default GPSButton;
