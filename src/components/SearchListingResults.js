import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import Moment from 'moment';

import {firebaseListing} from '@Firebase';
import {setFilter} from '@Redux/app';
import {TwoColumnListStyle} from '/AppStyles';
import SavedButton from '@Components/SavedButton';

import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';

import SelectInputBlue from '@Components/Forms/SelectInput/SelectInputBlue';
import {ShowCategory} from '@Helpers/Filters';

import DateRangeSelect from './DateRangeSelect';
import LocationSelect from './LocationSelect';
import FilterViewModal from '@Components/FilterViewModal';
import {filterItem, filterLocation} from '@Helpers/Filters';

class SearchList extends React.Component {
  constructor(props) {
    super(props);

    this.listingItemActionSheet = React.createRef();

    this.state = {
      categories: [],
      listings: [],
      allListings: [],
      savedListings: [],
      filter: props.filter,
      selectedItem: null,
      orderBy: ShowCategory[0].label,
      showedAll: false,
      showDateRangeSelect: false,
      showLocationSelect: false,
      filterModalVisible: false,
    };
  }

  componentDidMount() {
    this.categoriesUnsubscribe = firebaseListing.subscribeListingCategories(
      this.onCategoriesCollectionUpdate,
    );

    this.savedListingsUnsubscribe = firebaseListing.subscribeSavedListings(
      this.props.user.id,
      this.onSavedListingsCollectionUpdate,
    );

    this.loadItems();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      ...prevState,
      filter: nextProps.filter,
    };
  }

  loadItems = () => {
    this.listingsUnsubscribe = firebaseListing.subscribeListings(
      {orderBy: [['createdAt', 'desc']]},
      this.onListingsCollectionUpdate,
    );
  };

  componentDidUpdate(prevProps) {
    if (this.props.filter !== prevProps.filter) {
      this.loadItems();
    }
  }

  componentWillUnmount() {
    this.categoriesUnsubscribe();
    this.listingsUnsubscribe();
    this.savedListingsUnsubscribe();
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onSelectFilter = () => {
    this.setState({filterModalVisible: true});
  };

  onSelectFilterCancel = () => {
    this.setState({filterModalVisible: false});
  };

  onSelectFilterDone = filter => {
    this.setState({filter});
    this.setState({filterModalVisible: false});

    this.props.setFilter(filter);
    this.loadItems();
  };

  onCategoriesCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const category = doc.data();
      data.push({...category, id: doc.id});
    });
    this.setState({
      categories: data,
      loading: false,
    });
  };

  onListingsCollectionUpdate = querySnapshot => {
    const data = [];
    const {filter, orderBy} = this.state;
    // const {addressComponents} = this.props.location;

    let {userID} = this.props.user;

    querySnapshot.forEach(doc => {
      const listing = doc.data();

      let listingOwner = listing.authorID;

      if (userID === listingOwner) {
      }

      if (!filterItem(listing, filter)) {
        return;
      }
      // const isInLocation = filterLocation(listing.address, addressComponents);
      // if (!isInLocation) {
      //   return;
      // }

      if (this.state.savedListings.findIndex(k => k === doc.id) >= 0) {
        listing.saved = true;
      } else {
        listing.saved = false;
      }

      data.push({...listing, id: doc.id});
    });

    data.sort((a, b) => {
      switch (orderBy) {
        case ShowCategory[0].label: // Latest
          return a.createdAt && b.createdAt
            ? a.createdAt.seconds - b.createdAt.seconds
            : 0;

        case ShowCategory[1].label: // Price: Low to High
          return parseFloat(a.price) - parseFloat(b.price);

        case ShowCategory[2].label: // Price: High to Low
          return parseFloat(b.price) - parseFloat(a.price);

        default:
          return a.createdAt.seconds - b.createdAt.seconds;
      }
    });

    this.setState({
      listings: data,
      allListings: data,
      loading: false,
      showedAll: data,
    });
  };

  onSavedListingsCollectionUpdate = querySnapshot => {
    const savedListingdata = [];
    querySnapshot.forEach(doc => {
      const savedListing = doc.data();
      savedListingdata.push(savedListing.listingID);
    });
    const listingsData = [];
    this.state.listings.forEach(listing => {
      const temp = listing;
      if (savedListingdata.findIndex(k => k === temp.id) >= 0) {
        temp.saved = true;
      } else {
        temp.saved = false;
      }
      listingsData.push(temp);
    });

    this.setState({
      savedListings: savedListingdata,
      listings: listingsData,
      loading: false,
    });
  };

  onPressListingItem = item => {
    this.props.openItem(item);
  };

  onLongPressListingItem = item => {
    if (item.authorID === this.props.user.id) {
      this.props.selectItem(item);

      this.setState({selectedItem: item}, () => {
        this.listingItemActionSheet.current.show();
      });
    }
  };

  onPressSavedIcon = item => {
    firebaseListing.saveUnsaveListing(item, this.props.user.id);
  };

  onAddListing = () => {
    this.onModal('isMyListingVisible', this.onModal('isAddListingVisible'));
  };

  onLisingItemActionDone = index => {
    if (index === 0) {
      this.props.showModal();
    }

    if (index === 1) {
      Alert.alert(
        IMLocalized('Delete Listing'),
        IMLocalized('Are you sure you want to remove this listing?'),
        [
          {
            text: IMLocalized('Yes'),
            onPress: this.removeListing,
            style: 'destructive',
          },
          {text: IMLocalized('No')},
        ],
        {cancelable: false},
      );
    }
  };

  removeListing = () => {
    firebaseListing.removeListing(this.state.selectedItem.id, ({success}) => {
      if (!success) {
        Alert.alert(
          IMLocalized(
            'There was an error deleting the listing. Please try again',
          ),
        );
      }
    });
  };

  renderCategorySeparator = () => {
    return (
      <View
        style={{
          width: 10,
          height: '100%',
        }}
      />
    );
  };

  renderListingItem = ({item}) => {
    // const categoryName = this.state.categories.filter(
    //   c => c.id === item.categoryID,
    // ).name;

    return (
      <TouchableOpacity
        onPress={() => this.onPressListingItem(item)}
        onLongPress={() => this.onLongPressListingItem(item)}
        style={[
          TwoColumnListStyle.mainConatiner,
          {
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 3,
          },
        ]}>
        <View
          style={[
            TwoColumnListStyle.listingItemContainer1,
            {
              borderRadius: 12,
              overflow: 'hidden',
              alignSelf: 'center',
              elevation: 4,
            },
          ]}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto1}
            source={{uri: item.photo}}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon1}
            onPress={() => this.onPressSavedIcon(item)}
            item={item}
          />
          <View style={{paddingLeft: 10, paddingBottom: 10}}>
            <Text style={TwoColumnListStyle.dollar}>
              <Text style={{fontSize: 13, color: '#737373'}}>$ </Text>
              {item.price}
              <Text style={{fontSize: 11, color: '#777677'}}> per month.</Text>
            </Text>
            <Text
              style={{
                ...TwoColumnListStyle.listingName,
                maxHeight: 40,
                marginTop: 4,
              }}>
              {item.title}
            </Text>
            <Text style={TwoColumnListStyle.listingPlace1}>
              {`${
                item.address.street_number
                  ? item.address.street_number.long_name
                  : ''
              } ${item.address.street ? item.address.street.long_name : ''}, ${
                item.address.city ? item.address.city.long_name : ''
              }, ${item.address.county ? item.address.county.long_name : ''}, ${
                item.address.state ? item.address.state.short_name : ''
              }, ${
                item.address.country ? item.address.country.short_name : ''
              }`}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <View
                style={[TwoColumnListStyle.blueContainer, {marginRight: 10}]}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  {item.filters.Bedrooms === 'Any' ? 1 : item.filters.Bedrooms}{' '}
                  Bed,{' '}
                  {item.filters.Bathrooms === 'Any'
                    ? 1
                    : item.filters.Bathrooms}{' '}
                  Bath
                </Text>
              </View>
              <View style={TwoColumnListStyle.blueContainer}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  <Icon name="star" size={12} color="#324cf2" /> 4.6
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  renderListingItem1 = ({item}) => {
    // const categoryName = this.state.categories.filter(
    //   c => c.id === item.categoryID,
    // ).name;

    return (
      <TouchableOpacity
        onPress={() => this.onPressListingItem(item)}
        onLongPress={() => this.onLongPressListingItem(item)}
        style={[
          TwoColumnListStyle.mainConatiner1,
          {
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 3,
          },
        ]}>
        <View
          style={[
            TwoColumnListStyle.listingItemContainer,
            {borderRadius: 12, overflow: 'hidden', alignSelf: 'center'},
          ]}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto}
            source={{uri: item.photo}}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon}
            onPress={() => this.onPressSavedIcon(item)}
            item={item}
          />
          <View style={{paddingLeft: 10, paddingBottom: 10}}>
            <Text style={TwoColumnListStyle.dollar}>
              <Text style={{fontSize: 13, color: '#737373'}}>$ </Text>
              {item.price}
              <Text style={{fontSize: 11, color: '#777677'}}> per month.</Text>
            </Text>
            <Text
              style={{
                ...TwoColumnListStyle.listingName,
                maxHeight: 40,
                marginTop: 4,
              }}>
              {item.title}
            </Text>
            <Text style={TwoColumnListStyle.listingPlace1}>
              {`${
                item.address.street_number
                  ? item.address.street_number.long_name
                  : ''
              } ${item.address.street ? item.address.street.long_name : ''}, ${
                item.address.city ? item.address.city.long_name : ''
              }, ${item.address.county ? item.address.county.long_name : ''}, ${
                item.address.state ? item.address.state.short_name : ''
              }, ${
                item.address.country ? item.address.country.short_name : ''
              }`}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <View
                style={[TwoColumnListStyle.blueContainer, {marginRight: 10}]}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  {item.filters.Bedrooms === 'Any' ? 1 : item.filters.Bedrooms}{' '}
                  Bed,{' '}
                  {item.filters.Bathrooms === 'Any'
                    ? 1
                    : item.filters.Bathrooms}{' '}
                  Bath
                </Text>
              </View>
              <View style={TwoColumnListStyle.blueContainer}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  <Icon name="star" size={12} color="#324cf2" /> 4.6
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  toggleDateRangeSelect = e => {
    this.loadItems();

    this.setState({
      showDateRangeSelect: !this.state.showDateRangeSelect,
    });
  };

  toggleLocationSelect = e => {
    this.loadItems();

    this.setState({
      showLocationSelect: !this.state.showLocationSelect,
    });
  };

  changeOrderBy = orderBy => {
    this.setState({orderBy});
    this.loadItems();
  };

  render() {
    const {startDate, endDate, location, Type} = this.props;
    const {
      showDateRangeSelect,
      showLocationSelect,
      listings,
      orderBy,
    } = this.state;

    const dateRange =
      startDate !== ''
        ? Moment(startDate).format('MMMM Do') +
          (endDate !== '' ? ' - ' + Moment(endDate).format('MMMM Do') : '')
        : 'Select Dates';

    return (
      <ScrollView style={styles.container}>
        {/* <Text style={[styles.title, styles.listingTitle, { marginTop: 30 }]}>Recent Listings</Text> */}
        <View style={styles.row}>
          <View style={{justifyContent: 'center', marginRight: 10, flex: 1}}>
            <Icon
              name="search"
              size={25}
              color="#3a3a3a"
              style={{
                position: 'absolute',
                marginLeft: 30,
                marginBottom: 15,
                bottom: 7,
                zIndex: 10,
              }}
            />
            <Input
              underlineColorAndroid="transparent"
              placeholder="San Francisco, CA"
              placeholderTextColor="#2E3E69"
              defaultValue={location?.address}
              onFocus={this.toggleLocationSelect}
              keyboardType={Type}
              {...this.props}
            />
          </View>
          <FilterBtn title="Press me" onPress={this.onSelectFilter}>
            <Icon2 name="filter" size={25} color="#2E3E69" />
            <Text style={styles.filterFont}>Filter</Text>
          </FilterBtn>
        </View>
        {/* <View
          style={{marginLeft: 21, flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.sortText}>Date range </Text>
          <TouchableOpacity
            onPress={this.toggleDateRangeSelect}
            style={styles.rangeSelect}>
            <Text>{dateRange}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginLeft: 21,
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 5,
          }}>
          <Text style={styles.sortText}>Showing results by </Text>
          <SelectInputBlue
            data={ShowCategory}
            placeholder="Latest"
            value={orderBy}
            onChange={this.changeOrderBy}
          />
        </View> */}

        <View
          style={[
            styles.listings,
            {alignContent: 'center', alignItems: 'center', marginTop: 10},
          ]}>
          <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            numColumns={1}
            data={listings}
            renderItem={this.renderListingItem}
            keyExtractor={item => `${item.id}`}
            contentContainerStyle={{paddingTop: 20}}
          />
        </View>

        {/* <LinearGradient
          style={{
            marginLeft: 20,
            borderTopLeftRadius: 25,
            borderBottomLeftRadius: 25,
            paddingBottom: 20,
            paddingLeft: 20,
            marginBottom: 160,
          }}
          start={{x: 0.4, y: 1}}
          end={{x: 0.7, y: 0}}
          locations={[0, 1]}
          colors={['#FFECD2', '#FDC4AC']}>
          <View style={{marginLeft: 0}}>
            <Text style={styles.title}>{IMLocalized('New Listings')}</Text>
            <View style={styles.categories}>
              <FlatList
                horizontal
                initialNumToRender={4}
                ItemSeparatorComponent={() => this.renderCategorySeparator()}
                data={listings}
                renderItem={this.renderListingItem1}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => `${item.id}`}
              />
            </View>
          </View>
        </LinearGradient> */}

        {showDateRangeSelect && (
          <DateRangeSelect hideModal={this.toggleDateRangeSelect} />
        )}

        {showLocationSelect && (
          <LocationSelect hideModal={this.toggleLocationSelect} />
        )}

        {this.state.filterModalVisible && (
          <FilterViewModal
            value={this.state.filter}
            onCancel={this.onSelectFilterCancel}
            onDone={this.onSelectFilterDone}
          />
        )}

        <ActionSheet
          ref={this.listingItemActionSheet}
          title={IMLocalized('Confirm')}
          options={[
            IMLocalized('Edit Listing'),
            IMLocalized('Remove Listing'),
            IMLocalized('Cancel'),
          ]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={index => {
            this.onLisingItemActionDone(index);
          }}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    // fontWeight: "bold",
    fontFamily: 'CircularStd-Book',
    color: '#2a3ebd',
    fontSize: 27,
    marginBottom: 15,
    marginLeft: 5,
    marginTop: 10,
  },
  listingTitle: {
    marginTop: 10,
    marginBottom: 10,
  },
  categories: {
    marginBottom: 7,
  },
  categoryItemContainer: {
    borderRadius: 50,
  },
  categoryItemPhoto: {
    height: 80,
    borderRadius: 50,
    width: 80,
  },
  categoryItemTitle: {
    fontFamily: 'CircularStd-Book',
    color: '#525252',
    marginTop: 7,
    fontSize: 18,
    marginLeft: 2,
  },
  userPhoto: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginLeft: 10,
    marginRight: 10,
  },
  mapButton: {
    marginRight: 13,
    marginLeft: 7,
  },
  composeButton: {marginRight: 13},
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
  row: {
    flexDirection: 'row',
  },
  filterFont: {
    fontSize: 12,
    color: '#2E3E69',
  },
  sortContainer: {
    backgroundColor: '#E7EAF7',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 6,
    paddingBottom: 5,
    alignSelf: 'flex-start',
    borderRadius: 100,
    marginTop: 1,
    marginLeft: -5,
  },
  sortText: {
    color: '#454343',
    fontSize: 17,
    marginRight: 8,
  },
  rangeSelect: {
    borderRadius: 11.7,
    padding: 5,
    paddingHorizontal: 10,
    backgroundColor: 'rgb(231, 234, 247)',
  },
});

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  background: #e5e4ea;
  border-radius: 8px;
  height: 50px;
  padding-left: 45px;
  font-size: 17px;
  margin-left: 20px;
  margin-bottom: 10px;
`;

const FilterBtn = styled.TouchableOpacity`
  border-radius: 12px;
  height: 50px;
  background-color: rgba(61, 74, 152, 0.18);
  padding: 5px 15px;
  width: auto;
  align-items: center;
  align-self: flex-start;
  margin-right: 25px;
`;

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.app.location,
  startDate: state.app.startDate,
  endDate: state.app.endDate,
  filter: state.app.filter,
});

const mapDispatchToProps = {
  setFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchList);
