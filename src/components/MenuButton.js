import React from 'react';
import {Image, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {AppStyles} from '/AppStyles';
import {FontIcon} from '@Helpers/FontAwesome';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
export default class MenuButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableHighlight
        onPress={this.props.onPress}
        style={styles.btnClickContain}
        underlayColor="rgba(128, 128, 128, 0.1)">
        <View style={styles.btnContainer}>
          <View style={styles.iconWrapper}>
            {this.props.source === 'map2' ? (
              <FontAwesomeIcon name={'map'} size={25} color="#ffffff" />
            ) : this.props.source === 'wallet2' ? (
              <FontAwesome5Icon name={'wallet'} size={25} color="#ffffff" />
            ) : this.props.source === 'bank' ? (
              <FontAwesomeIcon
                name={this.props.source}
                size={25}
                color="#ffffff"
              />
            ) : (
              <FontIcon name={this.props.source} size={25} color="#ffffff" />
            )}
          </View>
          <Text style={styles.btnText}>{this.props.title}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  btnClickContain: {
    flexDirection: 'row',
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconWrapper: {
    width: 30,
    alignItems: 'flex-start',
  },
  btnText: {
    fontFamily: AppStyles.fontName.bold,
    fontSize: 17,
    color: '#ffffff',
    marginLeft: 5,
  },
});
