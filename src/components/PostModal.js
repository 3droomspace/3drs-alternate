import React from 'react';
import {
  Modal,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  NativeModules,
  Text,
  View,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import TextButton from 'react-native-button';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import Geocoder from 'react-native-geocoding';
import styled from 'styled-components/native';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

import SelectLocationModal from './SelectLocationModal';
import FilterViewModal from './FilterViewModal';
import {Configuration} from '../Configuration';
import {AppStyles, ModalHeaderStyle} from '/AppStyles';

import ServerConfiguration from '/ServerConfiguration';
import ListingAppConfig from '/ListingAppConfig';
import {firebaseListing, firebaseStorage} from '@Firebase';
import Geolocation from '@react-native-community/geolocation';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';

import BaseInput from './Forms/BaseInput/BaseInput';
import SelectInput from './Forms/SelectInput/SelectInput';
import CheckBox from './Forms/Checkbox/Checkbox';

import {RentPeriod, ListingType} from '@Helpers/Filters';
import AutoLocationInput from './Forms/LocationInput/AutoLocationInput';

import {Input} from '../components/Forms/BaseInput/BaseInput';

const ImagePickerC = NativeModules.ImageCropPicker;

// Icon.loadFont();

class PostModal extends React.Component {
  constructor(props) {
    super(props);

    Geocoder.init(ListingAppConfig.reverseGeoCodingAPIKey);

    const {selectedItem, categories} = this.props;
    let category = {name: IMLocalized('Select...')};
    let title = '';
    let description = '';
    let location = {
      latitude: Configuration.map.origin.latitude,
      longitude: Configuration.map.origin.longitude,
    };
    let localPhotos = [];
    let photoUrls = [];
    let price = '$1';
    const textInputValue = '';
    let filter = {
      Bathrooms: '',
      Bedrooms: '',
      'Close to Public Transportation': 'No',
      'Listing Type': '',
      'New Construction': 'No',
      'Price Range': '',
      'Rent Period': '',
      'Square Feet': '',
      'Year Built': '',
      Remodeled: 'No',
    };
    const filterValue = IMLocalized('Select...');
    let address = 'Checking...';

    // if (categories.length > 0) category = categories[0];
    if (selectedItem) {
      if (selectedItem.categoryID) {
        category = categories.find(c => selectedItem.categoryID === c.id).name;
      }
      title = selectedItem.title;
      description = selectedItem.description;
      localPhotos = selectedItem.photos;
      photoUrls = selectedItem.photos;
      price = selectedItem.price;
      filter = selectedItem.filters;
      address = selectedItem.address;
    }

    this.state = {
      categories: [
        {
          id: 'tae6JC8GZZaKOSSaZmKA',
          name: 'Houses',
          order: '1',
          photo:
            'https://firebasestorage.googleapis.com/v0/b/d-room-space-firebase.appspot.com/o/house.jpg?alt=media&token=130e6f8a-6d52-488d-94df-5de942d9e14d',
        },
        {
          id: '1SllkjWAI6pQxc5I0rPt',
          name: 'Apartments',
          order: '2',
          photo:
            'https://firebasestorage.googleapis.com/v0/b/d-room-space-firebase.appspot.com/o/apartment.jpg?alt=media&token=66d45bbf-48f9-4ab9-895e-445609474c12',
        },
        {
          id: 'ORdEColSbNEOxnEFnCbH',
          name: 'Commercial',
          order: '3',
          photo:
            'https://firebasestorage.googleapis.com/v0/b/d-room-space-firebase.appspot.com/o/commercial.jpg?alt=media&token=7df086c3-5e04-48eb-8e88-32641e9e7c30',
        },
      ],
      category,
      title,
      description,
      location,
      localPhotos,
      photoUrls,
      price,
      textInputValue,
      filter,
      filterValue,
      address,
      filterModalVisible: false,
      locationModalVisible: false,
      loading: false,
      currentLocation: {},
    };

    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeInputCategory = this.onChangeInputCategory.bind(this);
    this.onChangeInputFilter = this.onChangeInputFilter.bind(this);
    this.onToggleSwitch = this.onToggleSwitch.bind(this);
    this.onChangeRentPeriod = this.onChangeRentPeriod.bind(this);
  }

  async componentDidMount() {
    this.setFilterString(this.state.filter);
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Location Access Required',
        message: 'This App needs to Access your location',
      },
    );
    this._getCurrentLocation();
    // if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //   this._getCurrentLocation();
    // } else {
    //   // eslint-disable-next-line no-alert
    //   alert('Permission Denied');
    // }
  }

  _getCurrentLocation() {
    Geolocation.getCurrentPosition(
      info => {
        this.setState(
          {
            currentLocation: {
              latitude: info.coords.latitude,
              longitude: info.coords.longitude,
            },
          },
          () => console.log(this.state.currentLocation),
        );
      },
      error => console.log(error),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
  }

  selectLocation = () => {
    this.setState({locationModalVisible: true});
  };

  onChangeLocation = loc => {
    const newAddress = {
      ...loc.details,
      location: {
        geopoint: new firebase.firestore.GeoPoint(loc.latitude, loc.longitude),
        coordinates: {
          latitude: loc.latitude,
          longitude: loc.longitude,
        },
      },
    };

    this.setState({
      address: newAddress,
    });
  };

  onChangeInputCategory = (property, input) => {
    this.setState({category: input});
  };

  onChangeInputFilter = (property, input) => {
    this.setState({
      filter: {
        ...this.state.filter,
        [property]: input,
      },
    });
  };

  onChangeRentPeriod = (property, input) => {
    this.setState({
      filter: {
        ...this.state.filter,
        'Rent Period': input,
      },
    });
  };

  onToggleSwitch = property => {
    if (
      this.state.filter[property] === 'No' ||
      this.state.filter[property] === ''
    ) {
      this.setState({
        filter: {
          ...this.state.filter,
          [property]: 'Yes',
        },
      });
    } else {
      this.setState({
        filter: {
          ...this.state.filter,
          [property]: 'No',
        },
      });
    }
  };

  setFilterString = filter => {
    let filterValue = '';
    Object.keys(filter).forEach(function(key) {
      if (filter[key] !== 'Any' && filter[key] !== 'All') {
        filterValue += ` ${filter[key]}`;
      }
    });

    if (filterValue === '') {
      if (Object.keys(filter).length > 0) {
        filterValue = 'Any';
      } else {
        filterValue = IMLocalized('Select...');
      }
    }

    this.setState({filterValue});
  };

  onSelectLocationDone = location => {
    this.setState({location});
    this.setState({locationModalVisible: false});
    this.onChangeLocation(location);
  };

  onSelectLocationCancel = () => {
    this.setState({locationModalVisible: false});
  };

  selectFilter = () => {
    this.setState({filterModalVisible: true});
  };

  onSelectFilterCancel = () => {
    this.setState({filterModalVisible: false});
  };

  onSelectFilterDone = filter => {
    this.setState({filter});
    this.setState({filterModalVisible: false});
    this.setFilterString(filter);
  };

  pickMultiple() {
    ImagePickerC.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      sortOrder: 'desc',
      includeExif: true,
      forceJpg: true,
    })
      .then(images => {
        const tempArray = images.map(i => i.path);

        this.setState({
          localPhotos: [...this.state.localPhotos, ...tempArray],
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  onCancel = () => {
    this.props.onCancel();
  };

  onPost = () => {
    if (!this.state.title) {
      Alert.alert(IMLocalized('Title was not provided.'));
      return;
    }
    if (!this.state.description) {
      Alert.alert(IMLocalized('Description was not set.'));
      return;
    }
    if (!this.state.price) {
      Alert.alert(IMLocalized('Price is empty.'));
      return;
    }
    if (this.state.localPhotos.length === 0) {
      Alert.alert(IMLocalized('Please choose at least one photo.'));
      return;
    }
    if (!this.state.address.location) {
      Alert.alert(IMLocalized('Incorrect address.'));
      return;
    }
    if (!this.state.address.location.coordinates) {
      Alert.alert(IMLocalized('Incorrect address.'));
      return;
    }

    this.setState({loading: true});

    let photoUrls = [];

    if (this.props.selectedItem) {
      photoUrls = [...this.props.selectedItem.photos];
    }

    let uploadPromiseArray = [];
    this.state.localPhotos.forEach(uri => {
      if (!uri.startsWith('https://')) {
        uploadPromiseArray.push(
          new Promise((resolve, reject) => {
            firebaseStorage.uploadImage(uri).then(response => {
              if (response.downloadURL) {
                photoUrls.push(response.downloadURL);
              }
              resolve();
            });
          }),
        );
      }
    });

    Promise.all(uploadPromiseArray)
      .then(values => {
        const uploadObject = {
          isApproved: !ServerConfiguration.isApprovalProcessEnabled,
          authorID: this.props.user.id,
          categoryID: this.state.categories.find(
            c => c.name === this.state.category,
          ).id,
          description: this.state.description,
          filters: this.state.filter,
          title: this.state.title,
          price: this.state.price,
          address: this.state.address,
          photos: photoUrls,
        };
        firebaseListing.postListing(
          this.props.selectedItem,
          uploadObject,
          photoUrls,
          ({success, error}) => {
            if (success) {
              this.setState({loading: false}, () => {
                this.onCancel();
                this.props.navigation.navigate('Landing');
              });
            } else {
              console.error(error);
            }
          },
        );
      })
      .catch(reason => {
        console.log(reason);
      });
  };

  showActionSheet = index => {
    this.setState({
      selectedPhotoIndex: index,
    });
    this.ActionSheet.show();
  };

  onActionDone = index => {
    if (index === 0) {
      const array = [...this.state.localPhotos];
      array.splice(this.state.selectedPhotoIndex, 1);
      this.setState({localPhotos: array});
    }
  };

  render() {
    const categoryData = this.state.categories.map((category, index) => ({
      key: category.id,
      label: category.name,
    }));
    categoryData.unshift({key: 'section', label: 'Category', section: true});

    const photos = this.state.localPhotos.map((photo, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          this.showActionSheet(index);
        }}>
        <FastImage style={styles.photo} source={{uri: photo}} />
      </TouchableOpacity>
    ));

    return (
      <Modal
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        <ScrollView style={styles.screen}>
          <View style={ModalHeaderStyle.bar}>
            <Text style={ModalHeaderStyle.title}>
              {this.props.selectedItem ? 'Edit Listing' : 'Add Listing'}
            </Text>
            <TextButton
              style={[ModalHeaderStyle.rightButton, styles.rightButton]}
              onPress={this.onCancel}>
              Cancel
            </TextButton>
          </View>
          <ScrollView style={styles.body}>
            <PaddedContainer style={{marginTop: 0}}>
              <InputContainer>
                <BaseInput
                  Label="Title"
                  Placeholder="Enter Title for the Listing"
                  value={this.state.title}
                  onChangeText={text => this.setState({title: text})}
                />
              </InputContainer>
              <InputContainer>
                <BaseInput
                  Label="Details"
                  style={{height: 130, paddingTop: 15}}
                  Placeholder="Enter Details"
                  value={this.state.description}
                  onChangeText={text => this.setState({description: text})}
                  multiline
                />
              </InputContainer>
              <InputContainer>
                <BaseInput
                  Label="Price per month"
                  Placeholder="$"
                  keyboardType="numeric"
                  value={this.state.price}
                  onChangeText={text => {
                    this.setState({price: text});
                  }}
                />
              </InputContainer>
              <Split style={{zIndex: 1}}>
                <HalfWidth style={{marginRight: '5%'}}>
                  <InputContainer>
                    <SelectInput
                      Label="Category"
                      value={this.state.category}
                      data={categoryData}
                      Placeholder="Select Category of Listing"
                      onChange={this.onChangeInputCategory}
                    />
                  </InputContainer>
                </HalfWidth>
                <HalfWidth style={{marginRight: '5%'}}>
                  <InputContainer>
                    <SelectInput
                      Label="Listing Type"
                      data={ListingType}
                      value={this.state.filter['Listing Type']}
                      Placeholder="Select Listing Type"
                      onChange={this.onChangeInputFilter}
                    />
                  </InputContainer>
                </HalfWidth>
              </Split>
              <InputContainer style={{zIndex: 100}}>
                <AutoLocationInput
                  onChangeLocation={this.onChangeLocation}
                  value={this.state.address.full_address}
                  showSearch={true}
                />
              </InputContainer>
              <View>
                <Label>Add Photos</Label>
              </View>
              <ScrollView style={styles.photoList} horizontal>
                {photos}
                <TouchableOpacity onPress={this.pickMultiple.bind(this)}>
                  <View style={[styles.addButton, styles.photo]}>
                    <Icon2 name="add-a-photo" size={30} color="white" />
                  </View>
                </TouchableOpacity>
              </ScrollView>
            </PaddedContainer>
            <View style={styles.divider} />

            <PaddedContainer style={{marginTop: 0}}>
              <Split style={{zIndex: 1}}>
                <HalfWidth style={{marginRight: '5%'}}>
                  <InputContainer>
                    <BaseInput
                      Label="Bedrooms"
                      Placeholder="No. of Bedrooms"
                      keyboardType="numeric"
                      value={this.state.filter.Bedrooms}
                      onChangeText={text =>
                        this.setState({
                          filter: {
                            ...this.state.filter,
                            Bedrooms: text,
                          },
                        })
                      }
                    />
                  </InputContainer>
                </HalfWidth>
                <HalfWidth>
                  <InputContainer>
                    <BaseInput
                      Label="Bathrooms"
                      value={this.state.filter.Bathrooms}
                      Placeholder="No. of Bathrooms"
                      keyboardType="numeric"
                      onChangeText={text =>
                        this.setState({
                          filter: {
                            ...this.state.filter,
                            Bathrooms: text,
                          },
                        })
                      }
                    />
                  </InputContainer>
                </HalfWidth>
              </Split>
              <InputContainer>
                <BaseInput
                  Label="Area"
                  value={this.state.filter['Square Feet']}
                  Placeholder="Area in Sq. Ft."
                  keyboardType="numeric"
                  onChangeText={text =>
                    this.setState({
                      filter: {
                        ...this.state.filter,
                        'Square Feet': text,
                      },
                    })
                  }
                />
                <Input
                  value={this.state.filter['Apt No']}
                  placeholder="Apt No"
                  placeholderTextColor="#929AAF"
                  onChangeText={text =>
                    this.setState({
                      filter: {
                        ...this.state.filter,
                        'Apt No': text,
                      },
                    })
                  }
                />
                <Input
                  value={this.state.filter.Floor}
                  placeholder="Floor"
                  placeholderTextColor="#929AAF"
                  onChangeText={text =>
                    this.setState({
                      filter: {
                        ...this.state.filter,
                        Floor: text,
                      },
                    })
                  }
                />
              </InputContainer>
              <InputContainer>
                <SelectInput
                  Label="Rent Period"
                  value={this.state.filter['Rent Period']}
                  data={RentPeriod}
                  Placeholder="Select Rent Period"
                  onChange={this.onChangeInputFilter}
                />
              </InputContainer>
              <Split style={{zIndex: 1}}>
                <HalfWidth style={{marginRight: '5%'}}>
                  <InputContainer style={{zIndex: 100}}>
                    <BaseInput
                      Label="Year built"
                      value={this.state.filter['Year Built']}
                      Placeholder="Enter year of construction"
                      keyboardType="numeric"
                      onChangeText={text => {
                        this.setState({
                          filter: {
                            ...this.state.filter,
                            'Year Built': text,
                          },
                        });
                      }}
                    />
                  </InputContainer>
                </HalfWidth>
                <HalfWidth>
                  <TextLight style={{marginTop: 3}}>Remodeled?</TextLight>
                  <CheckBox
                    style={{height: 25, alignSelf: 'center', marginTop: 18}}
                    onToggleSwitch={this.onToggleSwitch}
                    value={this.state.filter.Remodeled === 'Yes' ? true : false}
                    Label="Remodeled"
                  />
                </HalfWidth>
              </Split>
              <InputContainer>
                <TextLight style={{marginTop: 3}}>
                  Close to Public transportation?
                </TextLight>
                <CheckBox
                  style={{height: 25, marginTop: 10, marginLeft: 10}}
                  onToggleSwitch={this.onToggleSwitch}
                  Label="Close to Public Transportation"
                  value={
                    this.state.filter['Close to Public Transportation']
                      ? true
                      : false
                  }
                />
              </InputContainer>
            </PaddedContainer>
            {this.state.filterModalVisible && (
              <FilterViewModal
                value={this.state.filter}
                onCancel={this.onSelectFilterCancel}
                onDone={this.onSelectFilterDone}
                category={this.state.category}
              />
            )}
            {this.state.locationModalVisible && (
              <SelectLocationModal
                location={this.state.location}
                onCancel={this.onSelectLocationCancel}
                onDone={this.onSelectLocationDone}
              />
            )}
            {this.state.loading ? (
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color={AppStyles.color.main} />
              </View>
            ) : (
              <TextButton
                containerStyle={styles.addButtonContainer}
                onPress={this.onPost}
                style={styles.addButtonText}>
                {this.props.selectedItem ? 'Save Listing' : 'Add Listing'}
              </TextButton>
            )}
            <ActionSheet
              ref={o => (this.ActionSheet = o)}
              title={IMLocalized('Confirm to delete?')}
              options={[IMLocalized('Confirm'), IMLocalized('Cancel')]}
              cancelButtonIndex={1}
              destructiveButtonIndex={0}
              onPress={index => {
                this.onActionDone(index);
              }}
            />
          </ScrollView>
        </ScrollView>
      </Modal>
    );
  }
}
// const actionSheetStyles = {
//   titleBox: {
//     backgroundColor: 'pink',
//   },
//   titleText: {
//     fontSize: 16,
//     color: '#000fff',
//   },
// };

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: AppStyles.color.white,
    paddingBottom: '20%',
  },
  divider: {
    backgroundColor: AppStyles.color.background,
    height: 10,
  },
  container: {
    justifyContent: 'center',
    height: 65,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: AppStyles.color.grey,
  },
  rightButton: {
    marginRight: 10,
  },
  sectionTitle: {
    textAlign: 'left',
    alignItems: 'center',
    color: AppStyles.color.title,
    fontSize: 19,
    padding: 10,
    paddingTop: 15,
    paddingBottom: 7,
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    // borderBottomWidth: 2,
    // borderBottomColor: AppStyles.color.grey
  },
  input: {
    width: '100%',
    fontSize: 19,
    padding: 10,
    textAlignVertical: 'top',
    justifyContent: 'flex-start',
    paddingRight: 0,
    fontFamily: AppStyles.fontName.main,
    color: AppStyles.color.text,
  },
  priceInput: {
    flex: 1,
    borderRadius: 5,
    borderColor: AppStyles.color.grey,
    borderWidth: 0.5,
    height: 40,
    textAlign: 'right',
    paddingRight: 5,
    fontFamily: AppStyles.fontName.main,
    color: AppStyles.color.text,
  },
  title: {
    flex: 2,
    textAlign: 'left',
    alignItems: 'center',
    color: AppStyles.color.title,
    fontSize: 19,
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
  },
  value: {
    textAlign: 'right',
    color: AppStyles.color.description,
    fontFamily: AppStyles.fontName.main,
  },
  section: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
  row: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  addPhotoTitle: {
    color: AppStyles.color.title,
    fontSize: 19,
    paddingLeft: 10,
    marginTop: 10,
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
  },
  photoList: {
    height: 70,
    marginTop: 20,
    marginRight: 10,
  },
  location: {
    alignItems: 'center',
    width: '50%',
  },
  photo: {
    marginLeft: 10,
    width: 60,
    height: 60,
    borderRadius: 10,
  },

  addButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#bac5e0',
  },

  photoIcon: {
    width: 50,
    height: 50,
  },
  addButtonContainer: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    borderRadius: 5,
    padding: 15,
    margin: 10,
    marginVertical: 27,
  },
  activityIndicatorContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  addButtonText: {
    color: AppStyles.color.white,
    fontWeight: 'bold',
    fontSize: 15,
  },
  // screen: {
  //   height: '70%'
  // }
});

const PaddedContainer = styled.View`
  padding: 20px 16px 10px;
  background: white;
`;

const Label = styled.Text`
  font-family: Poppins-SemiBold;
  font-size: 13px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #929aaf;
`;

const Split = styled.View`
  flex-direction: row;
  width: 100%;
  position: relative;
  flex: 1 1 auto;
  margin: 0 5px;
`;

const InputContainer = styled.View`
  margin-bottom: 18px;
`;

const HalfWidth = styled.View`
  ${'' /* margin-bottom: 18px; */}
  width: 47%;
`;

const TextLight = styled.Text`
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #636d85;
`;

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(PostModal);
