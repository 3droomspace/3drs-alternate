import React from 'react';
import {Modal, StyleSheet, Text, View} from 'react-native';
import Moment from 'moment';

import {connect} from 'react-redux';
import {setStartDate, setEndDate} from '@Redux/app';

import TextButton from 'react-native-button';
import CalendarPicker from 'react-native-calendar-picker';
import {AppStyles, ModalHeaderStyle} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

export class DateRangeSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedStartDate: this.props.startDate,
      selectedEndDate: this.props.endDate,
      pets: false,
      occupants: 1,
      message: '',
    };
  }

  onDateChange = (date, type) => {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  };

  setDateRange = () => {
    const {selectedStartDate, selectedEndDate} = this.state;

    this.props.setStartDate(selectedStartDate ? selectedStartDate : '');
    this.props.setEndDate(selectedEndDate ? selectedEndDate : '');

    if (this.props.hideModal) {
      this.props.hideModal();
    }
  };

  render() {
    const {selectedStartDate, selectedEndDate} = this.state;
    const minDate = new Date(); // Today
    // const maxDate = new Date(2017, 6, 3);
    const startDate = selectedStartDate
      ? Moment(selectedStartDate).format('MMMM Do YYYY')
      : '';
    const endDate = selectedEndDate
      ? Moment(selectedEndDate).format('MMMM Do YYYY')
      : '';

    return (
      <Modal animationType="slide" transparent={false}>
        <View style={{...ModalHeaderStyle.body, ...styles.modalContainer}}>
          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Start Date: {startDate}</Text>
            <Text style={styles.sectionTitle}>End Date: {endDate}</Text>
          </View>

          <CalendarPicker
            onDateChange={this.onDateChange}
            allowRangeSelection={true}
            minDate={minDate}
            todayBackgroundColor="#f2e6ff"
            selectedDayColor={
              DynamicAppStyles.colorSet.mainThemeForegroundColor
            }
            selectedStartDate={selectedStartDate}
            selectedEndDate={selectedEndDate}
            selectedDayTextColor="#FFFFFF"
          />

          <TextButton
            containerStyle={styles.continueButton}
            style={styles.continueButtonText}
            onPress={this.setDateRange}>
            Done
          </TextButton>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    padding: 30,
  },
  section: {
    marginBottom: 20,
  },
  sectionTitle: {
    color: AppStyles.color.text,
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 10,
  },
  continueButton: {
    marginTop: 20,
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    borderRadius: 5,
    padding: 15,
    zIndex: -1,
  },
  continueButtonText: {
    color: AppStyles.color.white,
    fontWeight: 'bold',
    fontSize: 15,
  },
});

const mapStateToProps = state => ({
  startDate: state.app.startDate,
  endDate: state.app.endDate,
});

const mapDispatchToProps = {
  setStartDate,
  setEndDate,
};

export default connect(mapStateToProps, mapDispatchToProps)(DateRangeSelect);
