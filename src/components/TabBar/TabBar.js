import React from 'react';
import {SafeAreaView} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import Tab from './Tab';

export const tabBarBuilder = (tabIcons, appStyles) => {
  return props => {
    const {navigation} = props;

    const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
    return (
      <SafeAreaView style={styles.tabBarContainer}>
        {navigation.state.routes.map((route, index) => {
          if (route.routeName !== 'Bookings') {
            return (
              <Tab
                key={index + ''}
                route={route}
                tabIcons={tabIcons}
                appStyles={appStyles}
                focus={navigation.state.index === index}
                onPress={() => {
                  // console.log('route.routeName ===>>>', route.routeName);
                  if (route.routeName === 'MenuDrawer') {
                    navigation.toggleDrawer();
                  } else {
                    if (route.routeName === 'Home') {
                      navigation.navigate({routeName: 'Map'});
                    } else if (route.routeName === 'Profile') {
                      navigation.navigate({routeName: 'MyProfile'});
                    } else if (route.routeName === 'Favorites') {
                      navigation.navigate({routeName: 'Favorites'});
                    } else {
                      navigation.navigate(route.routeName);
                    }
                  }
                }}
              />
            );
          } else {
            return;
          }
        })}
      </SafeAreaView>
    );
  };
};

export default tabBarBuilder;
