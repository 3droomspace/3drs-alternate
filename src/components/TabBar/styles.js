import {DynamicStyleSheet} from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    // container: {
    //   ...ifIphoneX(
    //     {
    //       height: 140,
    //     },
    //     {
    //       height: 70,
    //     },
    //   ),
    //   backgroundColor: 'transparent',
    // },
    tabBarContainer: {
      ...ifIphoneX(
        {
          height: 140,
        },
        {
          height: 50,
        },
      ),
      // position: 'absolute',
      // left: 0,
      // right: 0,
      // bottom: 0,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      overflow: 'visible',
      height: 100,
    },
    tabContainer: {
      backgroundColor: '#ffffff',
      // marginTop: 10,
      flex: 1,
      justifyContent: 'center',
      // marginBottom: -20,
      alignItems: 'center',
      overflow: 'visible',
      // borderWidth: 1
      paddingTop: 10,
    },
    plusStyle: {
      borderRadius: 40,
      paddingRight: 15,
      paddingLeft: 15,
      paddingTop: 15,
      paddingBottom: 15,
      marginBottom: 30,
      zIndex: 1000000,
    },
    tabIcon: {
      ...ifIphoneX(
        {
          width: 20,
          height: 20,
        },
        {
          width: 20,
          height: 20,
        },
      ),
    },
    activeContainer: {
      borderRadius: 25,
      // backgroundColor: '#2a3ebd',
      justifyContent: 'center',
      alignItems: 'center',
      // paddingHorizontal: 20,

      // paddingVertical: 15,
      height: '100%',
      width: '100%',
    },
    routeText: {
      fontFamily: 'CircularStd-Book',
      fontSize: 12,
      color: '#2a3ebd',
      marginTop: 2,
    },
    routeTextActive: {
      color: '#ffe5c8',
    },
    Home: {
      backgroundColor: 'blue',
    },
  });
};

export default dynamicStyles;
