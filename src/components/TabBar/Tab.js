import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import {FontIcon} from '@Helpers/FontAwesome';
import AddButton from '@Components/AddButton';
import Feather from 'react-native-vector-icons/Feather';

function Tab({route, onPress, focus, tabIcons, appStyles}) {
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  return (
    <TouchableOpacity style={styles.tabContainer} onPress={onPress}>
      {route.routeName == 'Home' ? (
        <LinearGradient
          colors={['#009aca', '#069dc9', '#aeefaa', '#fbffc1']}
          locations={[0, 0.1, 0.9, 1]}
          style={styles.plusStyle}>
          <View
            style={{
              width: 30,
              height: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* <FontIcon
              name={tabIcons[route.routeName].focus}
              size={25}
              color="#ffffff"
            /> */}
            <Feather name="home" size={32} color="#ffffff" />
          </View>
        </LinearGradient>
      ) : focus ? (
        <View style={styles.activeContainer}>
          <FontIcon
            name={tabIcons[route.routeName].focus}
            size={23}
            color="#aeefaa"
          />
        </View>
      ) : (
        <FontIcon
          name={tabIcons[route.routeName].unFocus}
          size={23}
          color="#919db5"
        />
      )}
    </TouchableOpacity>
  );
}

export default Tab;
