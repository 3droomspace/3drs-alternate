import React, {PureComponent} from 'react';
import {Keyboard, TouchableOpacity, StyleSheet, Text} from 'react-native';

class LocationItem extends PureComponent {
  constructor(props) {
    super(props);
  }

  _handlePress = async () => {
    const res = await this.props.fetchDetails(this.props.place_id);

    const location = {
      latitude: parseFloat(res.geometry.location.lat),
      longitude: parseFloat(res.geometry.location.lng),
      address: this.props.description,
      fetchDetails: this.props.fetchDetails,
      place_id: this.props.place_id,
      details: {},
    };

    location.details.full_address = location.address;

    for (let i = 0; i < res.address_components.length; i++) {
      let detail = res.address_components[i];
      if (detail.types.indexOf('neighborhood') > -1) {
        location.details.neighborhood = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
        let address_comp = location.details.full_address.split(',');
        address_comp.splice(1, 0, location.details.neighborhood.long_name);
        location.details.address_neighborhood = address_comp.join(',');
      } else if (detail.types.indexOf('locality') > -1) {
        location.details.city = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('country') > -1) {
        location.details.country = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('administrative_area_level_2') > -1) {
        location.details.county = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('postal_code') > -1) {
        location.details.postal_code = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('administrative_area_level_1') > -1) {
        location.details.state = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('route') > -1) {
        location.details.street = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      } else if (detail.types.indexOf('street_number') > -1) {
        location.details.street_number = {
          long_name: detail.long_name,
          short_name: detail.short_name,
        };
      }
    }

    this.props.fetchDetails(this.props.place_id);
    this.props.clearSearch();
    this.props.handleTextChange(location.address);
    this.props.onChangeLocation(location);
    Keyboard.dismiss();
    this.props.clearSearch();
  };

  render() {
    return (
      <TouchableOpacity style={styles.root} onPress={this._handlePress}>
        <Text>{this.props.description}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    height: 40,
    marginHorizontal: 10,
    borderTopWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
  },
});

export default LocationItem;
