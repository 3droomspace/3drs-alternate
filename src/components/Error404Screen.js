import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {connect} from 'react-redux';
import '@react-native-firebase/firestore';

import DynamicAppStyles from '/DynamicAppStyles';

class Error404Screen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={DynamicAppStyles.iconSet.error404}
          style={styles.imageStyle}
        />
        <Text style={styles.textStyle1}>There's a glitch in the matirix</Text>
        <Text style={styles.textStyle2}>
          Sorry, looks like you ran into an error,
        </Text>
        <Text style={styles.textStyle2}>Plese refresh and try again.</Text>
        <Image
          source={DynamicAppStyles.iconSet.logo}
          style={styles.logoStyle}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle1: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  textStyle2: {
    fontSize: 18,
  },
  imageStyle: {
    width: '60%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  logoStyle: {
    width: 100,
    height: 120,
    resizeMode: 'contain',
    marginTop: 30,
    alignSelf: 'center',
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(Error404Screen);
