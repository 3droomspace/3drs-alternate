import React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  BackHandler,
  Alert,
} from 'react-native';

import {connect} from 'react-redux';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FastImage from 'react-native-fast-image';
import ActionSheet from 'react-native-actionsheet';

import PostModal from './PostModal';
import SavedButton from '@Components/SavedButton';
import ServerConfiguration from '/ServerConfiguration';
import {AppStyles, TwoColumnListStyle} from '/AppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {firebaseListing} from '@Firebase';
import DynamicAppStyles from '/DynamicAppStyles';

class MyBookingScreen extends React.Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('Bookings'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {
        color: currentTheme.fontColor,
      },
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      listings: [],
      bookings: [],
      savedListings: [],
      selectedItem: null,
      postModalVisible: false,
      categories: [],
    };

    this.listingItemActionSheet = React.createRef();

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.listingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.LISTINGS);

    console.log('~~~~~', this.props.userType);
    if (this.props.userType === 'renter') {
      this.bookingsRef = firebase
        .firestore()
        .collection(ServerConfiguration.database.collection.BOOKINGS)
        .where('userID', '==', this.props.user.id);
    } else {
      this.bookingsRef = firebase
        .firestore()
        .collection(ServerConfiguration.database.collection.BOOKINGS)
        .where('managerID', '==', this.props.user.id);
    }

    this.categoriesRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.CATEGORIES);
  }

  componentDidMount() {
    this.savedListingsUnsubscribe = firebaseListing.subscribeSavedListings(
      this.props.user.id,
      this.onSavedListingsCollectionUpdate,
    );

    this.bookingsUnsubscribe = this.bookingsRef.onSnapshot(
      this.onBookingsCollectionUpdate,
    );

    this.categoriesUnsubscribe = this.categoriesRef.onSnapshot(
      this.onCategoriesCollectionUpdate,
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }

    if (this.bookingsUnsubscribe) {
      this.bookingsUnsubscribe();
    }

    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onPressSavedIcon = item => {
    firebaseListing.saveUnsaveListing(
      {
        ...item,
        saved:
          this.state.savedListings.findIndex(
            ({listingID}) => listingID === item.id,
          ) >= 0,
      },
      this.props.user.id,
    );
  };

  onSavedListingsCollectionUpdate = querySnapshot => {
    const savedListingList = [];
    querySnapshot.forEach(doc => {
      const savedListing = doc.data();
      savedListingList.push(savedListing);
    });

    this.setState({
      savedListings: savedListingList,
      loading: false,
    });
  };

  onBookingsCollectionUpdate = querySnapshot => {
    const bookings = [];
    querySnapshot.forEach(doc => {
      const booking = doc.data();
      bookings.push(booking);
    });

    this.setState({
      bookings,
    });

    this.listingsUnsubscribe = this.listingsRef.onSnapshot(
      this.onListingsCollectionUpdate,
    );
  };

  onCategoriesCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const category = doc.data();
      data.push({
        ...category,
        id: doc.id,
      });
    });
    this.setState({
      categories: data,
    });
  };

  onListingsCollectionUpdate = querySnapshot => {
    const data = [];
    const {bookings} = this.state;

    querySnapshot.forEach(doc => {
      const listing = doc.data();
      if (bookings.find(booking => booking.listingID === listing.id)) {
        data.push({
          ...listing,
          id: doc.id,
        });
      }
    });

    this.setState({
      listings: data,
      loading: false,
    });
  };

  onPressListingItem = item => {
    this.props.navigation.navigate('Detail', {
      item,
    });
  };

  onLongPressListingItem = item => {
    if (item.authorID === this.props.user.id) {
      this.setState(
        {
          selectedItem: item,
        },
        () => {
          this.listingItemActionSheet.current.show();
        },
      );
    }
  };

  onLisingItemActionDone = index => {
    if (index === 0) {
      this.setState({
        postModalVisible: true,
      });
    }

    if (index === 1) {
      Alert.alert(
        IMLocalized('Delete listing?'),
        IMLocalized('Are you sure you want to remove this listing?'),
        [
          {
            text: IMLocalized('Yes'),
            onPress: this.removeListing,
            style: 'destructive',
          },
          {
            text: IMLocalized('No'),
          },
        ],
        {
          cancelable: false,
        },
      );
    }
  };

  removeListing = () => {
    firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.LISTINGS)
      .doc(this.state.selectedItem.id)
      .delete()
      .then(function() {
        const realEstateSavedQuery = firebase
          .firestore()
          .collection(ServerConfiguration.database.collection.SAVED_LISTINGS)
          .where('listingID', '==', this.state.selectedItem.id);
        realEstateSavedQuery.get().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
            doc.ref.delete();
          });
        });
      })
      .catch(function(error) {
        console.log('Error deleting listing: ', error);
        Alert.alert(
          IMLocalized(
            'Oops! an error while deleting listing. Please try again later.',
          ),
        );
      });
  };

  onPostCancel = () => {
    this.setState({
      postModalVisible: false,
    });
  };

  renderListingItem = ({item}) => {
    // const categoryName = this.state.categories.filter(
    //   c => c.id === item.categoryID,
    // ).name;

    return (
      <TouchableOpacity
        onPress={() => this.onPressListingItem(item)}
        onLongPress={() => this.onLongPressListingItem(item)}
        style={[
          TwoColumnListStyle.mainConatiner,
          {
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 3,
          },
        ]}>
        <View
          style={[
            TwoColumnListStyle.listingItemContainer1,
            {
              borderRadius: 20,
              overflow: 'hidden',
              alignSelf: 'center',
            },
          ]}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto1}
            source={{
              uri: item.photo,
            }}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon1}
            onPress={() => this.onPressSavedIcon(item)}
            item={{
              saved:
                this.state.savedListings.findIndex(
                  ({listingID}) => listingID === item.id,
                ) >= 0,
            }}
          />
          <View
            style={{
              paddingLeft: 10,
              paddingBottom: 10,
            }}>
            <Text style={TwoColumnListStyle.dollar}>
              <Text
                style={{
                  fontSize: 13,
                  color: '#737373',
                }}>
                ${' '}
              </Text>
              {item.price}
              <Text
                style={{
                  fontSize: 11,
                  color: '#777677',
                }}>
                {' '}
                per month.
              </Text>
            </Text>
            <Text
              style={{
                ...TwoColumnListStyle.listingName,
                maxHeight: 40,
                marginTop: 4,
              }}>
              {item.title}
            </Text>
            <Text style={TwoColumnListStyle.listingPlace1}>
              {`${
                item.address.street_number
                  ? item.address.street_number.long_name
                  : ''
              } ${item.address.street ? item.address.street.long_name : ''}, ${
                item.address.city ? item.address.city.long_name : ''
              }, ${item.address.county ? item.address.county.long_name : ''}, ${
                item.address.state ? item.address.state.short_name : ''
              }, ${
                item.address.country ? item.address.country.short_name : ''
              }`}
            </Text>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={[
                  TwoColumnListStyle.blueContainer,
                  {
                    marginRight: 10,
                  },
                ]}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  {item.filters.Bedrooms === 'Any' ? 1 : item.filters.Bedrooms}{' '}
                  Bed,{' '}
                  {item.filters.Bathrooms === 'Any'
                    ? 1
                    : item.filters.Bathrooms}{' '}
                  Bath
                </Text>
              </View>
              <View style={TwoColumnListStyle.blueContainer}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  <Icon name="star" size={12} color="#324cf2" /> 4.6
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          data={this.state.listings}
          renderItem={this.renderListingItem}
          keyExtractor={item => `${item.id}`}
          contentContainerStyle={{
            paddingTop: 20,
          }}
        />
        {this.state.postModalVisible && (
          <PostModal
            selectedItem={this.state.selectedItem}
            categories={this.state.categories}
            onCancel={this.onPostCancel}
          />
        )}
        <ActionSheet
          ref={this.listingItemActionSheet}
          title={'Confirm'}
          options={['Edit Listing', 'Remove Listing', 'Cancel']}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={index => {
            this.onLisingItemActionDone(index);
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  rightButton: {
    marginRight: 10,
    color: AppStyles.color.main,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
  userType: state.app.userType,
});

export default connect(mapStateToProps)(MyBookingScreen);
