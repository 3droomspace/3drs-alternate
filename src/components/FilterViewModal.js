import React from 'react';
import {
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import TextButton from 'react-native-button';
import Icon from 'react-native-vector-icons/Feather';

import {firebaseFilter} from '@Firebase';

import {AppStyles, ModalHeaderStyle} from '/AppStyles';

import RangeSlider from '@Components/Forms/Slider/RangeSlider';
import CheckBox from '@Components/Forms/Checkbox/Checkbox';
import SelectInputCarousel from '@Components/Forms/SelectInput/SelectInputCarousel';
import {Input} from '@Components/Forms/BaseInput/BaseInput';

import {MAX_FILTER_VALUE, FILTER_STEP} from '@Helpers/Filters';

class FilterViewModal extends React.Component {
  constructor(props) {
    super(props);

    this.unsubscribe = null;

    this.state = {
      data: [],
      filter: this.props.value,
      filterIndex: {},
    };
  }

  componentDidMount() {
    this.unsubscribe = firebaseFilter.subscribeFilters(this.onCollectionUpdate);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onCollectionUpdate = querySnapshot => {
    var data = [];
    querySnapshot.forEach(doc => {
      const filter = doc.data();

      const isFilterCategory = this.getIsFilterCategory(filter);
      if (isFilterCategory) {
        data.push({...filter, id: doc.id});
      }

      if (this.state.filter[filter.name]) {
        if (filter.name !== 'Price Range' && filter.name !== 'Square Feet') {
          const index = filter.options.indexOf(this.state.filter[filter.name]);
          this.setState({
            filterIndex: {
              ...this.state.filterIndex,
              [filter.name]: index > 0 ? index : 0,
            },
          });
        }
      } else {
        let value = filter.options[0];
        if (filter.name === 'Price Range' || filter.name === 'Square Feet') {
          value = {
            min: 0,
            max: MAX_FILTER_VALUE,
          };
        }

        this.setState({
          filter: {...this.state.filter, [filter.name]: value},
          filterIndex: {...this.state.filterIndex, [filter.name]: 0},
        });
      }
    });

    data.push({
      name: 'Keyword',
      value: '',
    });

    this.setState({
      data,
    });
  };

  getIsFilterCategory = filter => {
    if (filter.categories && this.props.category) {
      return filter.categories.includes(this.props.category.id);
    } else {
      return true;
    }
  };

  onDone = () => {
    this.props.onDone(this.state.filter);
  };

  onCancel = () => {
    this.props.onCancel();
  };

  renderItem = item => {
    let filter_key = item.name;
    const {filter, filterIndex} = this.state;

    let initIndex = 0;
    if (filterIndex[filter_key]) {
      initIndex = filterIndex[filter_key];
    }

    switch (filter_key) {
      case 'Close to Public Transportation':
        return (
          <View style={styles.container}>
            <View style={styles.section}>
              <Text style={styles.title}>{filter_key}</Text>
              <CheckBox
                style={{height: 25, alignSelf: 'center'}}
                value={filter[filter_key] === 'Yes' ? true : false}
                onValueChanged={() => {
                  this.setState({
                    filter: {
                      ...filter,
                      [filter_key]:
                        filter[filter_key] === 'Yes' ? 'Any' : 'Yes',
                    },
                  });
                }}
              />
            </View>
          </View>
        );

      case 'Price Range':
      case 'Square Feet':
        const prefix = filter_key === 'Price Range' ? '$' : '';
        let valueStr = prefix + filter[filter_key].min;
        if (
          filter[filter_key].min === 0 &&
          filter[filter_key].max === MAX_FILTER_VALUE
        ) {
          valueStr = 'Any';
        } else if (filter[filter_key].max === MAX_FILTER_VALUE) {
          valueStr += '+';
        } else {
          valueStr += '-' + prefix + filter[filter_key].max;
        }

        return (
          <View style={styles.container}>
            <View style={styles.section}>
              <Text style={styles.title}>{filter_key}</Text>
              <Text style={styles.value}>{valueStr}</Text>
            </View>
            <RangeSlider
              style={styles.rangeSlider}
              min={0}
              max={MAX_FILTER_VALUE}
              initialLowValue={filter[filter_key].min}
              initialHighValue={filter[filter_key].max}
              step={FILTER_STEP}
              onValueChanged={(low, high, fromUser) => {
                this.setState({
                  filter: {
                    ...filter,
                    [filter_key]: {
                      min: low,
                      max: high,
                    },
                  },
                });
              }}
            />
          </View>
        );

      case 'Keyword':
        return (
          <View style={styles.container}>
            <View style={styles.section}>
              <Text style={styles.title}>Keyword</Text>
              <Input
                placeholder="Enter Keyword"
                style={styles.searchInput}
                value={filter.Keyword}
                onChangeText={text =>
                  this.setState({
                    filter: {
                      ...filter,
                      Keyword: text,
                    },
                  })
                }
              />
            </View>
          </View>
        );

      default:
        return (
          <View style={styles.container}>
            <View style={styles.section}>
              <Text style={styles.title}>{item.name}</Text>
              <SelectInputCarousel
                data={item.options}
                initIndex={initIndex}
                onChangeValue={(index, value) => {
                  this.setState({
                    filter: {...filter, [filter_key]: value},
                    filterIndex: {...filterIndex, [filter_key]: index},
                  });
                }}
              />
            </View>
          </View>
        );
    }
  };

  render() {
    const selectorArray = this.state.data.map(item => {
      return this.renderItem(item);
    });

    return (
      <Modal
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        <ScrollView style={styles.body}>
          <View
            style={[
              ModalHeaderStyle.bar,
              {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              },
            ]}>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => this.props.onCancel()}>
              <Icon name={'chevron-left'} size={28} color={'#2169c0'} />
              <Text style={styles.backButtonLabel}>{'Cancel'}</Text>
            </TouchableOpacity>

            <Text style={ModalHeaderStyle.title}>Filters</Text>
            <TextButton
              style={ModalHeaderStyle.rightButton}
              onPress={this.onDone}>
              Done
            </TextButton>
          </View>
          <View style={styles.listBody}>{selectorArray}</View>
        </ScrollView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  container: {
    justifyContent: 'center',
    minHeight: 65,
    paddingVertical: 5,
    alignItems: 'center',
    flexDirection: 'column',
    borderBottomWidth: 0.5,
    borderBottomColor: AppStyles.color.border,
  },
  section: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginVertical: 5,
  },
  rangeSlider: {
    width: '100%',
    height: 50,
  },
  listBody: {
    marginTop: 20,
  },
  title: {
    flex: 2,
    textAlign: 'left',
    alignItems: 'center',
    color: AppStyles.color.title,
    fontSize: 17,
    fontFamily: AppStyles.fontName.main,
  },
  value: {
    textAlign: 'right',
    // color: AppStyles.color.description,
    color: 'black',
    fontFamily: AppStyles.fontName.main,
    fontWeight: 'bold',
  },
  searchInput: {
    minWidth: '40%',
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    left: -10,
    zIndex: 10000,
  },
  backButtonLabel: {
    fontSize: 16,
    marginLeft: -5,
    marginTop: -2.5,
    color: '#2169c0',
    fontFamily: AppStyles.fontName.main,
  },
});

export default FilterViewModal;
