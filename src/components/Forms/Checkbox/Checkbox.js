import React from 'react';

import CheckBox from '@react-native-community/checkbox';

export default function App(props) {
  // const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => {
    // setIsEnabled(previousState => !previousState);
    props.onToggleSwitch(props.Label);
  };
  return (
    <CheckBox
      value={props.value || false}
      onValueChange={props.onValueChanged || toggleSwitch}
      onAnimationDidStop={() => {}}
      lineWidth={2}
      hideBox={false}
      boxType="circle"
      tintColor="#929AAF"
      onCheckColor="white"
      onFillColor="#511AF3"
      onTintColor="white"
      animationDuration={0.5}
      disabled={false}
      onAnimationType="bounce"
      offAnimationType="stroke"
      style={props.style}
    />
  );
}
