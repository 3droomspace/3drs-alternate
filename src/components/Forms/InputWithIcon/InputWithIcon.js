import React, {useState} from 'react';
import {TextInput, View, StyleSheet, I18nManager} from 'react-native';
import {Icon} from 'native-base';

const InputWithIcon = props => {
  const [focused, setFocused] = useState(false);
  const {
    iconName,
    iconType,
    secureTextEntry,
    placeholder,
    onChangeText,
    value,
    underlineColorAndroid,
    autoCapitalize,
    placeholderTextColor,
    activeColor,
    blurColor,
    autoCompleteType,
    keyboardType,
  } = props;

  return (
    <View
      style={[
        styles.GridContainer,
        {
          borderColor: focused ? activeColor : blurColor,
        },
      ]}>
      <Icon
        name={iconName}
        type={iconType}
        style={[
          styles.iconStyle,
          {
            color: focused ? activeColor : blurColor,
          },
        ]}
      />
      <TextInput
        style={styles.InputContainer}
        autoCompleteType={autoCompleteType}
        keyboardType={keyboardType}
        placeholderTextColor={placeholderTextColor}
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        onChangeText={onChangeText}
        value={value}
        underlineColorAndroid={underlineColorAndroid}
        autoCapitalize={autoCapitalize}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  GridContainer: {
    width: '80%',
    alignSelf: 'center',
    alignItems: 'flex-end',
    display: 'flex',
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
  },
  iconStyle: {
    fontSize: 25,
    paddingVertical: 8,
  },
  InputContainer: {
    height: 42,
    paddingLeft: 10,
    width: '90%',
    color: '#000',
    alignItems: 'center',
    textAlign: I18nManager.isRTL ? 'right' : 'left',
  },
});

export default InputWithIcon;
