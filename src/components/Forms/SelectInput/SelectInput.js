import React, {Component} from 'react';
import ModalSelector from 'react-native-modal-selector';

import {ModalSelectorStyle} from '/AppStyles';

import {Label, Container, Input} from '../BaseInput/BaseInput';

export default class Inputs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textInputValue: props.value || '',
    };
  }

  render() {
    return (
      <Container>
        <Label>{this.props.Label}</Label>
        <ModalSelector
          data={this.props.data}
          accessible
          sectionTextStyle={ModalSelectorStyle.sectionTextStyle}
          optionTextStyle={ModalSelectorStyle.optionTextStyle}
          optionContainerStyle={ModalSelectorStyle.optionContainerStyle}
          cancelContainerStyle={ModalSelectorStyle.cancelContainerStyle}
          cancelTextStyle={ModalSelectorStyle.cancelTextStyle}
          selectedItemTextStyle={ModalSelectorStyle.selectedItemTextStyle}
          backdropPressToClose
          scrollViewAccessibilityLabel="Scrollable options"
          cancelText="Cancel"
          cancelButtonAccessibilityLabel="Cancel Button"
          onChange={option => {
            this.props.onChange(this.props.Label, option.label);
            this.setState({textInputValue: option.label});
          }}>
          <Input
            editable={false}
            placeholder={this.props.Placeholder}
            placeholderTextColor="#929AAF"
            value={this.state.textInputValue}
          />
        </ModalSelector>
      </Container>
    );
  }
}
