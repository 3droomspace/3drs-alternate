import React, {Component} from 'react';
import {Text} from 'react-native';
import styled from 'styled-components/native';
import ModalSelector from 'react-native-modal-selector';
import Icon from 'react-native-vector-icons/FontAwesome';

import {Container} from '../BaseInput/BaseInput';

export default class Inputs extends Component {
  render() {
    const {value, placeholder, style, data, onChange} = this.props;

    return (
      <Container>
        <ModalSelector
          optionContainerStyle={{backgroundColor: 'white', borderRadius: 8}}
          cancelContainerStyle={{backgroundColor: 'white', borderRadius: 8}}
          data={data}
          initValue="Select something yummy!"
          accessible
          scrollViewAccessibilityLabel="Scrollable options"
          cancelButtonAccessibilityLabel="Cancel Button"
          onChange={option => onChange(option.label)}>
          <Input
            editable={false}
            placeholder={placeholder}
            placeholderTextColor="#0c0c0c"
            value={value}
            style={style}>
            <Text style={{fontSize: 14}}>
              {value} {'  '}
              <Icon name="caret-down" size={13} color="#2E3E69" />
            </Text>
          </Input>
        </ModalSelector>
      </Container>
    );
  }
}

export const Input = styled.View`
  background-color: #e7eaf7;
  padding: 4px 10px 3px 10px;
  align-self: flex-start;
  border-radius: 50;
  margin-top: 1px;
  margin-left: -5px;
  color: #0c0c0c;
  font-size: 15;
  font-family: CircularStd-Book;
`;
