import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import DynamicAppStyles from '/DynamicAppStyles';

export default class SelectInputCarousel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.data,
      index: props.initIndex,
    };
  }

  onNext = () => {
    let index = this.state.index + 1;
    const {data} = this.state;

    if (index >= data.length) {
      index = 0;
    }

    this.setState({
      index,
    });

    this.props.onChangeValue(index, data[index]);
  };

  onPrevious = () => {
    let index = this.state.index - 1;
    const {data} = this.state;

    if (index < 0) {
      index = data.length - 1;
    }

    this.setState({
      index,
    });

    this.props.onChangeValue(index, data[index]);
  };

  render() {
    const {style} = this.props;
    const {data, index} = this.state;

    return (
      <View
        style={{
          ...style,
          ...styles.container,
        }}>
        <TouchableOpacity onPress={this.onPrevious} style={styles.button}>
          <Icon
            name="chevron-left"
            size={15}
            color={DynamicAppStyles.colorSet.mainThemeForegroundColor}
          />
        </TouchableOpacity>

        <Text style={styles.value}>{data[index]}</Text>

        <TouchableOpacity onPress={this.onNext} style={styles.button}>
          <Icon
            name="chevron-right"
            size={15}
            color={DynamicAppStyles.colorSet.mainThemeForegroundColor}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    paddingHorizontal: 5,
  },
  value: {
    fontSize: 18,
    paddingHorizontal: 10,
    paddingBottom: 3,
  },
  label: {
    color: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    fontSize: 25,
  },
});
