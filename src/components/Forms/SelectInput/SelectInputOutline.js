import React, {Component} from 'react';
import styled from 'styled-components/native';
import ModalSelector from 'react-native-modal-selector';

import {Container} from '../BaseInput/BaseInput';

export default class Inputs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textInputValue: '',
    };
  }

  render() {
    return (
      <Container>
        <ModalSelector
          optionContainerStyle={{backgroundColor: 'white', borderRadius: 8}}
          cancelContainerStyle={{backgroundColor: 'white', borderRadius: 8}}
          data={this.props.data}
          initValue="Select something yummy!"
          accessible
          scrollViewAccessibilityLabel="Scrollable options"
          cancelButtonAccessibilityLabel="Cancel Button"
          onChange={option => {
            this.setState({textInputValue: option.label});
          }}>
          <Input
            editable={false}
            placeholder={this.props.Placeholder}
            placeholderTextColor="#929AAF"
            value={this.state.textInputValue}
            style={this.props.style}
          />
        </ModalSelector>
      </Container>
    );
  }
}

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  border: 1px #d0d3d9;
  border-radius: 8px;
  height: 40px;
  padding-left: 10px;
  font-size: 14px;
`;
