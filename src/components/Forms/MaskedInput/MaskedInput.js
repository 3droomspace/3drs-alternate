import React, {Component} from 'react';
import {TextInputMask} from 'react-native-masked-text';
import styled from 'styled-components/native';

export default class Inputs extends Component {
  render() {
    return (
      <Container>
        <Label>{this.props.Label}</Label>
        <TextInputMask
          type="custom"
          options={{
            /**
             * mask: (String | required | default '')
             * the mask pattern
             * 9 - accept digit.
             * A - accept alpha.
             * S - accept alphanumeric.
             * * - accept all, EXCEPT white space.
             */
            mask: '999 AAA SSS ***',
          }}
          underlineColorAndroid="transparent"
          placeholder={this.props.Placeholder}
          placeholderTextColor="#929AAF"
          onChangeText={this.handleEmail}
          keyboardType={this.props.Type}
          {...this.props}
        />
      </Container>
    );
  }
}

Inputs.propTypes = {};

export const Label = styled.Text`
  font-family: Poppins-SemiBold;
  font-size: 12px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #929aaf;
`;

export const Container = styled.View``;

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  background: #f5f6f7;
  border-radius: 8px;
  height: 50px;
  padding-left: 10px;
  font-size: 14px;
  margin-top: 8px;
`;
