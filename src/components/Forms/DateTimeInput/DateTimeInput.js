import React, {useState} from 'react';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import styled from 'styled-components/native';
import {format} from 'date-fns';

// const [DatePickerVisibility, setDatePickerVisibility] = useState(false);

const Example = props => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  // const [Date, setDate] = useState(false); // To store in DB
  const [DateShow, setDateShow] = useState(false); // To show in input

  const Label = styled.Text`
    font-family: Poppins-SemiBold;
    font-size: 12px;
    line-height: 20px;
    letter-spacing: 0.3px;
    color: #929aaf;
  `;

  const Container = styled.View`
    width: 100%;
  `;

  const Input = styled.TextInput`
    font-family: CircularStd-Book;
    background-color: #f5f6f7;
    flex-direction: row;
    border-radius: 8px;
    height: 50px;
    padding-left: 10px;

    margin-top: 8px;
  `;

  const InputLabel = styled.Text`
    align-self: center;
    font-size: 14px;
  `;

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = date => {
    // setDate(date);
    setDateShow(format(date, 'MM/dd/yyyy hh:mma'));
    hideDatePicker();
  };

  return (
    <Container>
      <Label>{props.Label}</Label>
      <Input
        onFocus={showDatePicker}
        placeholder={props.Placeholder}
        placeholderTextColor="#929AAF">
        <InputLabel>{DateShow}</InputLabel>
      </Input>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="datetime"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </Container>
  );
};

export default Example;
