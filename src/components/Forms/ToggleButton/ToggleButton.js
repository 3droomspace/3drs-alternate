import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {AppStyles} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

export default class ToggleButton extends React.Component {
  render() {
    const {label, style, textStyle, onToggle, value} = this.props;

    return (
      <TouchableOpacity
        onPress={onToggle}
        style={{
          ...style,
          ...styles.container,
          backgroundColor: value
            ? DynamicAppStyles.colorSet.mainThemeForegroundColor
            : AppStyles.color.lightgrey,
        }}
        activeOpacity={0.5}>
        <Text
          style={{
            ...textStyle,
            color: value
              ? AppStyles.color.white
              : DynamicAppStyles.colorSet.mainTextColor,
          }}>
          {label}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 30,
  },
});
