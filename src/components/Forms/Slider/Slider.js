import React from 'react';
import {Slider as RNSlider} from 'react-native-elements';
import {AppStyles} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

export default class Slider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <RNSlider
        value={this.props.value}
        onValueChange={this.props.onValueChange}
        style={this.props.style}
        thumbStyle={{
          ...this.props.thumbStyle,
          borderColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
          borderWidth: 4,
          backgroundColor: AppStyles.color.white,
        }}
        minimumTrackTintColor={
          this.props.minimumTrackTintColor ||
          DynamicAppStyles.colorSet.mainThemeForegroundColor
        }
        maximumTrackTintColor={
          this.props.maximumTrackTintColor || AppStyles.color.lightgrey
        }
        trackStyle={{
          ...this.props.trackStyle,
          height: 5,
        }}
      />
    );
  }
}
