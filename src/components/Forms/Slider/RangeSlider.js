import React from 'react';
import RNRangeSlider from 'rn-range-slider';
import {AppStyles} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

export default class RangeSlider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      style,
      gravity,
      min,
      max,
      initialLowValue,
      initialHighValue,
      step,
      selectionColor,
      blankColor,
      thumbBorderColor,
      onValueChanged,
    } = this.props;

    return (
      <RNRangeSlider
        style={{
          ...style,
        }}
        gravity={gravity || 'center'}
        min={min}
        max={max}
        initialLowValue={initialLowValue}
        initialHighValue={initialHighValue}
        step={step || 1}
        labelStyle="none"
        selectionColor={
          selectionColor || DynamicAppStyles.colorSet.mainThemeForegroundColor
        }
        blankColor={blankColor || AppStyles.color.lightgrey}
        thumbBorderColor={
          thumbBorderColor || DynamicAppStyles.colorSet.mainThemeForegroundColor
        }
        thumbBorderWidth={3}
        onValueChanged={onValueChanged}
      />
    );
  }
}
