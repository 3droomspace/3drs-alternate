import React, {Component} from 'react';
import styled from 'styled-components/native';

export default class Inputs extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Container>
        <Label>{this.props.Label}</Label>
        <Input
          underlineColorAndroid="transparent"
          placeholder={this.props.Placeholder}
          placeholderTextColor="#929AAF"
          onChangeText={this.props.onChangeText}
          keyboardType={this.props.Type}
          {...this.props}
        />
      </Container>
    );
  }
}

Inputs.propTypes = {};

export const Label = styled.Text`
  font-family: Poppins-SemiBold;
  font-size: 15px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #929aaf;
`;

export const Container = styled.View``;

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  background: #f5f6f7;
  border-radius: 8px;
  height: 50px;
  padding-left: 10px;
  font-size: 14px;
  margin-top: 8px;
`;
