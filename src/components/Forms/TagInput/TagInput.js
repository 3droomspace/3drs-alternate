import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import styled from 'styled-components/native';

import {Label, Container} from '../BaseInput/BaseInput';

const categories = [
  'Party',
  'Food',
  'Music',
  'Sports',
  'Learning',
  'Film',
  'TV',
];

export default function App(props) {
  const [activeTag, selectTag] = useState(false);
  const [input, setInput] = useState(false);
  return (
    <Container>
      <Label>{props.Label}</Label>
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        {categories.map(tag => (
          <Outline
            style={activeTag === tag ? styles.active : {}}
            key={tag}
            onPress={() => selectTag(tag)}>
            <BtnText style={activeTag === tag ? styles.activeText : {}}>
              {tag}
            </BtnText>
          </Outline>
        ))}
        <Input
          underlineColorAndroid="transparent"
          placeholder="+"
          placeholderTextColor="#511AF3"
          placeholderStyle={{fontSize: 25}}
          value={input ? activeTag : ''}
          onChangeText={value => {
            selectTag(value);
            setInput(true);
          }}
          keyboardType={props.Type}
        />
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  active: {
    backgroundColor: '#511AF3',
  },
  activeText: {
    color: 'white',
    fontFamily: 'Poppins-SemiBold',
  },
});

const Outline = styled.TouchableOpacity`
  background: rgba(81, 26, 243, 0.1);
  border-radius: 100px;
  padding: 7px 15px;
  width: auto;
  flex-direction: row;
  align-items: center;
  align-self: flex-start;
  margin-left: 10px;
  margin-top: 13px;
`;

const BtnText = styled.Text`
  font-size: 15px;
  letter-spacing: 0.3px;
  color: #511af3;
`;

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  border: 1px solid #929aaf;
  border-radius: 150px;
  height: 37px;
  padding: 0 20px;
  font-size: 25px;
  margin-left: 10px;
  margin-top: 13px;
`;
