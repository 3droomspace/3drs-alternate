import React, {Component} from 'react';
import styled from 'styled-components/native';

import LocationInputCore from './LocationInputCore';

export default class Inputs extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Container>
        <Label>{this.props.Label}</Label>
        <LocationInputCore keyboardShouldPersistTaps="always" />
      </Container>
    );
  }
}

const Label = styled.Text`
  font-family: Poppins-SemiBold;
  font-size: 12px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #929aaf;
  margin-bottom: 8px;
`;

// TODO fix zIndex
const Container = styled.View``;

// const Input = styled.TextInput`
//   font-family: CircularStd-Book;
//   background: #f5f6f7;
//   border-radius: 8px;
//   height: 50px;
//   padding-left: 10px;
//   font-size: 14px;
//   margin-top: 8px;
// `;
