import React from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const GOOGLE_PLACES_API_KEY = 'AIzaSyD2wA--ZbqhaslfWiE3Xak1GR6N3uv8EM0'; // never save your real api key in a snack!

export default function GooglePlacesInput({
  onFocus,
  onBlur,
  onChangeTextFromFormik,
  value,
  addressRef,
  focus,
}) {
  return (
    <GooglePlacesAutocomplete
      ref={addressRef}
      placeholder="Enter Address for Listing"
      placeholderTextColor="#929AAF"
      minLength={1} // minimum length of text to search
      autoFocus={false}
      returnKeyType="search" // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      keyboardAppearance="light" // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      fetchDetails
      listViewDisplayed={false}
      textContentType="fullStreetAddress"
      autoCapitalize="words"
      selectionColor="red"
      clearButtonMode="always"
      autoCompleteType="street-address"
      keyboardShouldPersistTaps="always"
      enablePoweredByContainer={false}
      // blurOnSubmit={false}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        // const lat = JSON.stringify(details.geometry.location.lat)
        // const lng = JSON.stringify(details.geometry.location.lng)
        const {description, types} = data && data;
        const address = {
          lat: details.geometry.location.lat,
          lng: details.geometry.location.lng,
          location: description,
          addressType: types,
        };
        console.log(address);
      }}
      textInputProps={{
        onChangeText: text => {},
        onFocus,
        keyboardShouldPersistTaps: 'always',
      }}
      value={value}
      getDefaultValue={() => ''}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: GOOGLE_PLACES_API_KEY,
        language: 'en', // language of the results
        region: 'US',
        // types: '(cities)' // default: 'geocode'
        // types: '', // default: 'geocode'
        // location: '43.70011, -79.4163',
        // radius: '100000', // 100 km
        // components: 'country:ca',
        // strictbounds: true
      }}
      styles={{
        container: {
          width: '100%',
          backgroundColor: '#f5f6f7',
        },
        listView: {
          position: 'absolute',
          top: 50,
          left: 0,
          right: 0,
          backgroundColor: '#f5f6f7',
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
          flex: 1,
          elevation: 10,
          zIndex: 100,
          margin: 0,
        },
        textInputContainer: {
          backgroundColor: 'transparent',
          marginTop: 0,
          width: '100%',
          padding: 0,
          borderTopWidth: 0,
          borderBottomWidth: 0,
        },
        textInput: {
          backgroundColor: '#f5f6f7',
          borderRadius: 8,
          width: '100%',
          height: 50,
          marginLeft: 0,
          marginRight: 0,
          marginTop: 0,
          paddingLeft: 10,
          fontSize: 14,
          fontFamily: 'Poppins-Regular',
        },
        description: {
          // color: '#ac879a',
          fontWeight: '300',
        },
        predefinedPlacesDescription: {
          color: '#1faadb',
        },
      }}
      // currentLocation // Will add a 'Current location' button at the top of the predefined places list
      // currentLocationLabel="Current location"
      // nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      // GoogleReverseGeocodingQuery={
      //   {
      //     // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      //   }
      // }
      // GooglePlacesSearchQuery={{
      //   // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
      //   rankby: 'distance',
      //   type: 'food'
      // }}
      // GooglePlacesDetailsQuery={{ fields: 'geometry' }}
      // // GooglePlacesDetailsQuery={{
      // //     // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
      // //     fields: 'formatted_address',
      // // }}
      //
      // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      // predefinedPlaces={[]}
      // debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      // renderLeftButton={() => {}}
      // renderRightButton={() => {}}
    />
  );
}
