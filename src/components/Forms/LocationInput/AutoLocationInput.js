import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {GoogleAutoComplete} from 'react-native-google-autocomplete';
import LocationItem from '@Components/LocationItem';

class AutoLocationInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: this.props.value || '',
      showSearch: this.props.showSearch,
    };
  }

  render() {
    if (this.state.showSearch) {
      return (
        <View style={{...styles.wrapper, ...this.props.style}}>
          <View style={styles.container}>
            <GoogleAutoComplete
              apiKey="AIzaSyCyyfSMl88xGTwh9DXBIqXS67DUzLUOmZk"
              components="country:us"
              debounce={300}
              minLength={2}>
              {({
                handleTextChange,
                handleTextChangeDone,
                locationResults,
                fetchDetails,
                isSearching,
                clearSearch,
              }) => (
                <React.Fragment>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Search for address..."
                      placeholderTextColor="#9A9EAB"
                      onChangeText={text => {
                        this.setState({inputValue: text});
                        handleTextChange(text);
                      }}
                      value={this.state.inputValue}
                      borderWidth={0}
                    />
                  </View>

                  {isSearching && (
                    <ActivityIndicator size="large" color="white" />
                  )}
                  <ScrollView
                    keyboardShouldPersistTaps={true}
                    keyboardDismissMode="interactive">
                    {locationResults.map(el => (
                      <LocationItem
                        {...el}
                        key={el.id}
                        handleTextChange={text =>
                          this.setState({inputValue: text})
                        }
                        selectedPlaceHandler={this.selectedPlaceHandler}
                        fetchDetails={fetchDetails}
                        onChangeLocation={this.props.onChangeLocation}
                        clearSearch={clearSearch}
                        inputValue={this.state.inputValue}
                        updateAddress={this.props.updateAddress}
                      />
                    ))}
                  </ScrollView>
                </React.Fragment>
              )}
            </GoogleAutoComplete>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    color: 'black',
    backgroundColor: '#f5f6f7',
    marginBottom: 18,
  },
  textInput: {
    height: 50,
    color: 'black',
    fontFamily: 'CircularStd-Book',
    fontSize: 14.4,
    borderRadius: 8,
    paddingLeft: 10,
    width: '90%',
  },
  inputWrapper: {
    flexDirection: 'row',
  },
});

export default AutoLocationInput;
