import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {GoogleAutoComplete} from 'react-native-google-autocomplete';
import LocationItem from '@Components/LocationItem';
import {Configuration} from '/Configuration';

class AutoLocationInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: this.props.initialValue ? this.props.initialValue : '',
      showSearch: this.props.showSearch,
    };
  }

  render() {
    if (this.state.showSearch) {
      return (
        <View style={styles.wrapper}>
          <View style={styles.container}>
            <GoogleAutoComplete
              GoogleAutoComplete
              apiKey={Configuration.google_map_key}
              components="country:us"
              queryTypes="geocode"
              debounce={300}
              minLength={2}>
              {({
                handleTextChange,
                handleTextChangeDone,
                locationResults,
                fetchDetails,
                inputValue,
                isSearching,
                clearSearch,
              }) => (
                <React.Fragment>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Enter City or Zip Code"
                      placeholderTextColor="#9A9EAB"
                      onChangeText={handleTextChange}
                      value={inputValue}
                      borderWidth={0}
                    />
                  </View>

                  {isSearching && (
                    <ActivityIndicator size="large" color="white" />
                  )}
                  <ScrollView
                    keyboardShouldPersistTaps={true}
                    keyboardDismissMode="interactive">
                    {locationResults.map(el => (
                      <LocationItem
                        {...el}
                        key={el.id}
                        handleTextChange={handleTextChangeDone}
                        selectedPlaceHandler={this.selectedPlaceHandler}
                        fetchDetails={fetchDetails}
                        onChangeLocation={this.props.onChangeLocation}
                        clearSearch={clearSearch}
                        inputValue={inputValue}
                        updateAddress={this.props.updateAddress}
                      />
                    ))}
                  </ScrollView>
                </React.Fragment>
              )}
            </GoogleAutoComplete>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    color: 'black',
    backgroundColor: '#ffffff',
    marginBottom: 18,
  },
  textInput: {
    height: 50,
    color: 'black',
    fontFamily: 'CircularStd-Book',
    fontSize: 18,
    borderRadius: 8,
    // paddingLeft: 10,
    backgroundColor: '#ffffff',
    borderBottomWidth: 2,
    borderBottomColor: '#3bcdc5',
    width: '100%',
  },
  inputWrapper: {
    flexDirection: 'row',
  },
});

export default AutoLocationInput;
