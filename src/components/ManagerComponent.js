import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import ManagerItemView from './ManagerItemView';

const ManagerComponent = props => {
  const {appStyles, menuItems} = props;
  const {firstName, lastName, fullname} = props.user;

  const displayName = () => {
    if (
      (firstName && firstName.length > 0) ||
      (lastName && lastName.length > 0)
    ) {
      return firstName + ' ' + lastName;
    }
    return fullname || '';
  };

  const renderMenuItem = menuItem => {
    const {title, icon, onPress, tintColor} = menuItem;
    return (
      <ManagerItemView
        title={title}
        icon={icon}
        iconStyle={{tintColor}}
        onPress={onPress}
        appStyles={appStyles}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.userName}>{displayName()}</Text>
      {menuItems.map(menuItem => {
        return renderMenuItem(menuItem);
      })}
      {/* <Text
          onPress={onLogout}
          style={styles.logout}
        >
          {IMLocalized('Logout')}
        </Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: 50,
    alignItems: 'center',
    // backgroundColor: 'appStyles.colorSet.mainThemeBackgroundColor',
  },
  buttonContainer: {
    height: 53,
    width: '98%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    // height: imageSize,
    // width: imageSize,
    marginTop: 50,
  },
  closeButton: {
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginRight: 15,
    // backgroundColor: appStyles.colorSet.grey0,
    width: 28,
    height: 28,
    borderRadius: 20,
    overflow: 'hidden',
  },
  closeIcon: {
    width: 27,
    height: 27,
  },
  userName: {
    marginTop: 5,
    // color: appStyles.colorSet.mainTextColor,
    fontSize: 17,
    marginBottom: 40,
  },
  logout: {
    width: '90%',
    borderWidth: 1,
    // color: appStyles.colorSet.mainTextColor,
    fontSize: 15,
    paddingVertical: 10,
    // borderColor: appStyles.colorSet.grey3,
    borderRadius: 5,
    textAlign: 'center',
  },
});

export default ManagerComponent;
