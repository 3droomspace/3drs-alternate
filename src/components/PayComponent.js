import React from 'react';
import PropTypes from 'prop-types';
import {FlatList, StyleSheet} from 'react-native';
import ThreadItem from './ThreadItem';
import {
  Modal,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  NativeModules,
  Text,
  View,
  Alert,
} from 'react-native';
import TextButton from 'react-native-button';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import '@react-native-firebase/firestore';
import {ModalHeaderStyle} from '/AppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import PaymentsHelper from '../helpers/PaymentsHelper';
import {CreditCardInput} from 'react-native-credit-card-input';

function PayComponent(props) {
  const user = props.user;
  const submitPayment = () => {
    Alert.alert(IMLocalized('Payment complete.'));
  };

  const paymentProcesser = new PaymentsHelper();

  return (
    <ScrollView style={styles.screen}>
      <View style={ModalHeaderStyle.bar}>
        <Text>{user.unit ? user.unit : 'No property found.'}</Text>
        <Text>
          $
          {user.payment_due_amount
            ? user.payment_due_amount
            : 'No payment due amoutn found.'}{' '}
          Due by{' '}
          {user.payment_due_date ? user.payment_due_date : 'No due date found.'}
        </Text>
        <CreditCardInput />
        <TextButton
          style={[ModalHeaderStyle.rightButton, styles.rightButton]}
          onPress={submitPayment}>
          Pay Now
        </TextButton>
      </View>
    </ScrollView>
  );
}

PayComponent.propTypes = {
  user: PropTypes.object,
};

const styles = StyleSheet.create({
  PayComponentContainer: {
    margin: 6,
  },
});

export default PayComponent;
