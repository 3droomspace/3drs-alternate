import React, {Component} from 'react';
import {View} from 'react-native';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {AppIcon} from '/AppStyles';
import {logout, setUserData} from '@Containers/onboarding/redux';
import ListingAppConfig from '../ListingAppConfig';
import DynamicAppStyles from '/DynamicAppStyles';
import authManager from '@Containers/onboarding/utils/authManager';
import ManagerComponent from './ManagerComponent';

class ManagerDashboard extends Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('Manager Dashboard'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
    };
  };

  constructor(props) {
    super(props);

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onLogout() {
    authManager.logout(this.props.user);
    this.props.logout();
    this.props.navigation.navigate('LoadScreen', {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    });
  }

  onUpdateUser = newUser => {
    this.props.setUserData({
      user: newUser,
    });
  };

  render() {
    var menuItems = [
      {
        title: IMLocalized('My Listings'),
        tintColor: '#baa3f3',
        icon: AppIcon.images.list,
        onPress: () => this.props.navigation.navigate('MyListingModal'),
      },
      {
        title: IMLocalized('Calendar'),
        tintColor: '#df9292',
        icon: AppIcon.images.calendar,
        onPress: () => this.props.navigation.navigate('ManagerCalendar'),
      },
      // {
      //   title: IMLocalized('Appointments')
      // },
      // {
      //   title: IMLocalized('Maintenance'),
      //   tintColor: '#6b7be8',
      //   icon: AppIcon.images.renovation,
      // },
      // {
      //   title: IMLocalized('Analytics'),
      //   tintColor: '#a6a4b1',
      //   icon: AppIcon.images.chart,
      // },
      {
        title: IMLocalized('Contact Us'),
        icon: AppIcon.images.contactUs,
        tintColor: '#9ee19f',
        onPress: () =>
          this.props.navigation.navigate('Contact', {
            appStyles: DynamicAppStyles,
            form: ListingAppConfig.contactUsFields,
            screenTitle: IMLocalized('Contact us'),
          }),
      },
    ];

    return (
      <View>
        <ManagerComponent
          user={this.props.user}
          onUpdateUser={user => this.onUpdateUser(user)}
          onLogout={() => this.onLogout()}
          menuItems={menuItems}
          appStyles={DynamicAppStyles}
        />
      </View>
    );
  }
}

// const styles = StyleSheet.create({});

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
    isAdmin: auth.user && auth.user.isAdmin,
  };
};

export default connect(mapStateToProps, {
  logout,
  setUserData,
})(ManagerDashboard);
