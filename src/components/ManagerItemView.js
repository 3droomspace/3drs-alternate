import React from 'react';
import {Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native';

const ManagerItemView = props => {
  // const {appStyles} = props;
  const rightArrowIcon = require('/CoreAssets/right-arrow.png');

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.itemContainer}>
        <Image style={[styles.icon, props.iconStyle]} source={props.icon} />
        <Text style={styles.title}>{props.title}</Text>
      </View>
      <Image style={styles.itemNavigationIcon} source={rightArrowIcon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    width: '95%',
  },
  icon: {
    width: 24,
    height: 24,
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    height: '100%',
    marginLeft: 10,
  },
  title: {
    marginLeft: 15,
    // color: appStyles.colorSet.mainTextColor,
    fontSize: 14,
    marginTop: 3,
  },
  itemNavigationIcon: {
    height: 20,
    width: 20,
    marginRight: 10,
    tintColor: 'lightgray',
    // tintColor: appStyles.colorSet.grey6,
    // transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
});

export default ManagerItemView;
