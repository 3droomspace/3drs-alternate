import React, {useState} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {FontIcon} from '@Helpers/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

const MessageSearchBar = props => {
  const [input, handleInput] = useState('');

  return (
    <LinearGradient
      colors={['#009aca', '#069dc9', '#aeefaa']}
      locations={[0, 0.2, 1]}
      style={styles.barContainer}>
      <View style={styles.searchBarContainer}>
        <View style={styles.iconContainer}>
          <FontIcon name={'search'} size={16} color="#C0C0C0" />
        </View>
        <TextInput
          style={styles.searchBar}
          placeholder="Search"
          onChangeText={text => {
            handleInput(text);
          }}
        />
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  barContainer: {
    height: '15%',
    width: '100%',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingBottom: '3%',
  },
  searchBarContainer: {
    alignItems: 'center',
    fontSize: 30,
    height: 35,
    fontFamily: 'Avenir',
    flexDirection: 'row',
    borderRadius: 35,
    backgroundColor: 'white',
    width: '90%',
    marginTop: 60,
    shadowColor: 'black',
    shadowOffset: {
      height: 2,
    },
    shadowRadius: 10,
    shadowOpacity: 0.2,
    marginRight: '5%',
  },
  iconContainer: {
    marginRight: 5,
    marginLeft: 20,
  },
  searchBar: {
    backgroundColor: 'white',
    width: '80%',
    height: '100%',
    fontSize: 16,
    paddingLeft: 10,
    fontWeight: '500',
    fontFamily: 'Avenir',
  },
});

export default MessageSearchBar;
