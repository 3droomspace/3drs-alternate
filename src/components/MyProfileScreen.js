import React, {Component} from 'react';
import {
  BackHandler,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
import {AppIcon} from '/AppStyles';
import authManager from '@Containers/onboarding/utils/authManager';
import DynamicAppStyles from '/DynamicAppStyles';
import ListingAppConfig from '/ListingAppConfig';
import {IMUserProfileComponent} from '@Containers/profile';
import {logout, setUserData} from '@Containers/onboarding/redux';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import ChangePasswordModal from './ChangePasswordModal';
import Icon from 'react-native-vector-icons/Feather';
import CardListModal from '../components/PayModals/CardListModal';

class MyProfileScreen extends Component {
  static navigationOptions = ({screenProps, navigation}) => {
    const userType = navigation.state.params
      ? navigation.state.params.userType
      : '';
    const currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: userType + IMLocalized('Profile'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {
        color: currentTheme.fontColor,
        alignSelf: 'center',
        flex: 1,
        textAlign: 'center',
        marginLeft: -10,
      },
      headerLeft: (
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}>
          <Icon
            name={'chevron-left'}
            size={32}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.backButtonLabel}>{'Back'}</Text>
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.state = {
      changePwd: false,
      showCardListModal: false,
    };
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.props.navigation.setParams({
      userType:
        this.props.userType.charAt(0).toUpperCase() +
        this.props.userType.slice(1) +
        ' ',
    });
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onLogout() {
    authManager.logout(this.props.user);
    this.props.logout();
    this.props.navigation.navigate('LoadScreen', {
      appStyles: DynamicAppStyles,
      appConfig: ListingAppConfig,
    });
  }

  onChangePwd = () => {
    this.setState({
      changePwd: true,
    });
  };

  closeModal = () => {
    this.setState({
      changePwd: false,
    });
  };

  onUpdateUser = newUser => {
    this.props.setUserData({user: newUser});
  };

  onIDVerification = () => {
    this.props.navigation.navigate('IdVerification', {user: this.props.user});
  };

  render() {
    var menuItems = [
      {
        title: IMLocalized('Manager Dashboard'),
        tintColor: '#baa3f3',
        icon: AppIcon.images.checklist,
        onPress: () => this.props.navigation.navigate('ManagerDashboard'),
      },
      {
        title: IMLocalized('Account Details'),
        icon: AppIcon.images.accountDetail,
        tintColor: '#6b7be8',
        onPress: () =>
          this.props.navigation.navigate('AccountDetail', {
            appStyles: DynamicAppStyles,
            form: ListingAppConfig.editProfileFields,
            screenTitle: IMLocalized('Edit Profile'),
          }),
      },
      {
        title: IMLocalized('Settings'),
        icon: AppIcon.images.settings,
        tintColor: '#a6a4b1',
        onPress: () =>
          this.props.navigation.navigate('Settings', {
            appStyles: DynamicAppStyles,
            form: ListingAppConfig.userSettingsFields,
            screenTitle: IMLocalized('Settings'),
          }),
      },
      {
        title: IMLocalized('Pay'),
        icon: AppIcon.images.settings,
        tintColor: '#a6a4b1',
        onPress: () => {
          this.setState({
            showCardListModal: true,
          });
        },
      },
      {
        title: IMLocalized('Contact Us'),
        icon: AppIcon.images.contactUs,
        tintColor: '#9ee19f',
        onPress: () =>
          this.props.navigation.navigate('Contact', {
            appStyles: DynamicAppStyles,
            form: ListingAppConfig.contactUsFields,
            screenTitle: IMLocalized('Contact us'),
          }),
      },
    ];

    if (this.props.isAdmin) {
      menuItems.push({
        title: IMLocalized('Admin Dashboard'),
        tintColor: '#8aced8',
        icon: AppIcon.images.checklist,
        onPress: () => this.props.navigation.navigate('AdminDashboard'),
      });
    }

    return (
      <ScrollView>
        <IMUserProfileComponent
          user={this.props.user}
          onUpdateUser={user => this.onUpdateUser(user)}
          onLogout={() => this.onLogout()}
          onChangePwd={this.onChangePwd}
          onIDVerification={this.onIDVerification}
          menuItems={menuItems}
          appStyles={DynamicAppStyles}
          userType={this.props.userType}
        />

        {this.state.changePwd && (
          <ChangePasswordModal
            onCancel={this.closeModal}
            onDone={this.closeModal}
          />
        )}

        {this.state.showCardListModal && (
          <CardListModal
            isFromPay={true}
            user={this.props.user}
            onCloseModal={() => this.setState({showCardListModal: false})}
          />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonLabel: {
    fontSize: 18,
    marginLeft: -5,
    marginTop: -2,
  },
});

const mapStateToProps = ({app, auth}) => {
  return {
    user: auth.user,
    isAdmin: auth.user && auth.user.isAdmin,
    userType: app.userType,
  };
};

export default connect(mapStateToProps, {
  logout,
  setUserData,
})(MyProfileScreen);
