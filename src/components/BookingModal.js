import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Modal,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import TextButton from 'react-native-button';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';
import {extendMoment} from 'moment-range';
import NumericInput from 'react-native-numeric-input';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {firebaseBooking, firebaseAuth} from '@Firebase';
import {AppStyles, ModalHeaderStyle} from '/AppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';
import {Configuration} from '../Configuration';
import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';
import firebase from '@react-native-firebase/app';
import CardListModal from '../components/PayModals/CardListModal';

const fcmURL = 'https://fcm.googleapis.com/fcm/send';
const firebaseServerKey =
  'AAAAQfLnWt4:APA91bFXUv9AOZcviTxS2y5IxmTmDthEJsa2uZ9_UqBDIlDnwpP1qjapZwdsQqS0SCZbKszhPbD8eNSgRFGapNPbtV12T3kQPkvUHoeotjU2z6ddiQMPxbCmhLS7IIXbkIhM1uNcbz8f';

class Booking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedStartDate: null,
      selectedEndDate: null,
      pets: false,
      occupants: 1,
      message: '',
      bookedDates: [],
      loading: false,
      showCardListModal: false,
    };

    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date, type) {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }

  onPress = () => {
    this.setState({
      pets: !this.state.pets,
    });
  };

  onPost = () => {
    const {
      selectedStartDate,
      selectedEndDate,
      occupants,
      pets,
      message,
      bookedDates,
    } = this.state;
    const moment = extendMoment(Moment);
    const {userID, managerID} = this.props;
    const {id, price, address, filters} = this.props.data;
    const {firstName, lastName} = this.props.author;
    const {firstName1, lastName1} = this.props.tenant;

    console.log('--->', this.props.data);
    console.log('####', filters.Floor);
    console.log('####', filters[`Apt No`]);

    if (selectedStartDate === null || selectedEndDate === null) {
      Alert.alert('Select dates first');
      return;
    }

    this.setState({loading: true});
    return new Promise((resolve, reject) => {
      const start = Moment(selectedStartDate.toString());
      const end = Moment(selectedEndDate.toString());
      const totalDays =
        Math.floor(Moment.duration(end.diff(start)).asHours() / 24) + 1;
      const days = Math.floor(totalDays % 30);
      const months = Math.floor(days / 30);
      const years = Math.floor(months / 12);
      const money = price.replace('$', '');
      const totalMoney = '$' + Number(money) * (months + days / 30);
      const ownerName = firstName + ' ' + lastName;
      const ownerAddress = address.full_address;
      const tenantName =
        this.props.tenant.firstName + ' ' + this.props.tenant.lastName;
      const street = address.street.long_name;
      const city = address.city.long_name;
      const floor = filters.Floor;
      const apartment = filters[`Apt No`];

      // this.sendPushNotification();
      fetch(Configuration.serverUri, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          DAYS: days,
          MONTHS: months,
          YEARS: years,
          BEGIN_DATE: Moment(selectedStartDate).format('DD-MM-YYYY'),
          END_DATE: Moment(selectedEndDate).format('DD-MM-YYYY'),
          MONTHLY_RENT: money,
          TOTAL_RENT: totalMoney,
          INITIAL: 'start',
          MADE_DATE: Moment(new Date()).format('DD-MM-YYYY'),
          APARTMENT: apartment,
          FLOOR: floor,
          OWNER_NAME: ownerName,
          OWNER_ADDRESS: ownerAddress,
          TENANT_NAME: tenantName,
          TENANT_ADDRESS: '',
          BUILDING: street,
          BOROUGH: city,
        }),
      })
        .then(res => res.json())
        .then(response => {
          firebase
            .storage()
            .ref(response.data.name)
            .getDownloadURL()
            .then(
              function(url) {
                firebaseBooking.addBooking({
                  startDate: selectedStartDate.toString(),
                  endDate: selectedEndDate.toString(),
                  occupants,
                  pets,
                  message,
                  userID,
                  managerID,
                  listingID: id,
                  docusignPDF: url,
                });
                this.setState({
                  loading: false,
                  showCardListModal: true,
                });
              }.bind(this),
            );
        })
        .catch(err => {
          reject(err);
        });
    });
  };

  sendPushNotification = async () => {
    const pushToken = await firebaseAuth.fetchPushTokenIfPossible();
    const pushNotification = {
      to: pushToken,
      notification: {
        title: 'AAAA',
        body: 'Booking',
      },
      data: {
        title: 'BB',
        body: 'Booking-----',
      },
    };
    const fcmFetch = fetch(fcmURL, {
      method: 'post',
      headers: {
        Authorization: 'key=' + firebaseServerKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(pushNotification),
    });

    fcmFetch.then(c => c.json()).then(console.log);
  };

  componentDidMount() {
    let dates = [];
    const moment = extendMoment(Moment);
    const {bookings} = this.props;

    bookings.length > 0 &&
      bookings.map(booking => {
        const range = moment().range(booking.startDate, booking.endDate);
        let start = range.start;
        let end = range.end;
        while (end.diff(start, 'days') >= 0) {
          dates.push(start.format());
          start.add(1, 'days');
        }
      });
    this.setState({bookedDates: dates});
  }

  render() {
    const {selectedStartDate, selectedEndDate, occupants, loading} = this.state;
    const minDate = new Date(); // Today
    // const maxDate = new Date(2017, 6, 3);
    const startDate = selectedStartDate
      ? Moment(selectedStartDate).format('MMMM Do YYYY')
      : '';
    const endDate = selectedEndDate
      ? Moment(selectedEndDate).format('MMMM Do YYYY')
      : '';

    const radio_props = [
      {label: 'No', value: !this.state.pets},
      {label: 'Yes', value: this.state.pets},
    ];

    const {title, address} = this.props.data;

    return (
      <Modal
        visible={true}
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        <View style={ModalHeaderStyle.bar}>
          <Text style={ModalHeaderStyle.title}>Booking</Text>
          <TextButton
            style={[ModalHeaderStyle.rightButton, styles.rightButton]}
            onPress={this.props.onCancel}>
            Cancel
          </TextButton>
        </View>
        <ScrollView style={styles.body}>
          <Text style={styles.sectionTitle}>Book your stay at: </Text>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.address}>{address.full_address}</Text>
          <View style={styles.divider} />
          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Start Date</Text>
            <Text style={styles.sectionTitle}>End Date</Text>
          </View>
          <View style={styles.section}>
            <Text style={styles.selectDate}>{startDate}</Text>
            <Text style={styles.selectDate}>{endDate}</Text>
          </View>
          <View style={styles.divider} />
          <View>
            <View style={styles.container}>
              <CalendarPicker
                onDateChange={this.onDateChange}
                allowRangeSelection={true}
                minDate={minDate}
                disabledDates={this.state.bookedDates}
                todayBackgroundColor="#f2e6ff"
                selectedStartDate={selectedStartDate}
                selectedEndDate={selectedEndDate}
                selectedDayColor={
                  DynamicAppStyles.colorSet.mainThemeForegroundColor
                }
                selectedDayTextColor="#FFFFFF"
              />
            </View>
          </View>
          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Number of Occupants</Text>
            <Text style={styles.sectionTitle}>Do you have pets?</Text>
          </View>
          <View style={styles.section}>
            <View style={styles.selectDate}>
              <NumericInput
                type="up-down"
                minValue={1}
                rounded={true}
                totalWidth={100}
                initValue={occupants}
                onChange={value => this.setState({occupants: value})}
              />
            </View>
            <View style={styles.selectDate}>
              <RadioForm formHorizontal={true} animation={true}>
                {radio_props.map((obj, i) => (
                  <RadioButton labelHorizontal={false} key={i}>
                    <RadioButtonInput
                      onPress={this.onPress}
                      obj={obj}
                      index={i}
                      buttonInnerColor={
                        DynamicAppStyles.colorSet.mainThemeForegroundColor
                      }
                      buttonOuterColor={
                        DynamicAppStyles.colorSet.mainThemeForegroundColor
                      }
                      isSelected={obj.value}
                      buttonWrapStyle={{marginRight: 50}}
                    />
                    <RadioButtonLabel
                      onPress={this.onPress}
                      obj={obj}
                      index={i}
                      labelWrapStyle={{marginRight: 50}}
                    />
                  </RadioButton>
                ))}
              </RadioForm>
            </View>
          </View>
          <Text style={styles.sectionTitle}>Add a message: </Text>
          <TextInput
            multiline={true}
            numberOfLines={2}
            style={styles.input}
            onChangeText={text => this.setState({message: text})}
            value={this.state.description}
            placeholder="Start typing..."
            placeholderTextColor={AppStyles.color.grey}
            underlineColorAndroid="transparent"
          />
          <TextButton
            containerStyle={styles.addButtonContainer}
            onPress={this.onPost}
            style={styles.addButtonText}>
            Book Now
          </TextButton>
        </ScrollView>
        {loading && <TNActivityIndicator />}
        {this.state.showCardListModal && (
          <CardListModal
            user={this.props.tenant}
            price={this.props.data.price.replace('$', '')}
            onCloseModal={() => this.setState({showCardListModal: false})}
            onCloseBookModal={this.props.onCancel}
          />
        )}
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  rightButton: {
    marginRight: 10,
  },
  body: {
    width: '100%',
    // flex: 1,
    backgroundColor: AppStyles.color.white,
  },
  section: {
    flex: 2,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    width: '100%',
    backgroundColor: 'white',
    marginBottom: 10,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    padding: 10,
    paddingBottom: 10,
    textAlign: 'center',
  },
  sectionTitle: {
    textAlign: 'left',
    alignItems: 'center',
    color: AppStyles.color.title,
    fontSize: 19,
    padding: 10,
    paddingTop: 15,
    paddingBottom: 7,
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    width: '50%',
  },
  address: {
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 20,
    color: '#333',
  },
  selectDate: {
    width: '50%',
    fontSize: 16,
    color: '#333',
    fontStyle: 'italic',
    marginLeft: 10,
  },
  dateButton: {
    width: '50%',
  },
  input: {
    width: '100%',
    fontSize: 19,
    padding: 10,
    textAlignVertical: 'top',
    justifyContent: 'flex-start',
    paddingRight: 0,
    fontFamily: AppStyles.fontName.main,
    color: AppStyles.color.text,
  },
  divider: {
    backgroundColor: AppStyles.color.background,
    height: 10,
  },
  addButtonContainer: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    borderRadius: 5,
    padding: 15,
    margin: 10,
    marginVertical: 27,
  },
  addButtonText: {
    color: AppStyles.color.white,
    fontWeight: 'bold',
    fontSize: 15,
  },
});

export default Booking;
