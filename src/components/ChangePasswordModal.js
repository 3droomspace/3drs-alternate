import React from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {IMLocalized} from 'containers/localization/IMLocalization';
import {AppStyles, ModalHeaderStyle} from '/AppStyles';
import authManager from '@Containers/onboarding/utils/authManager';
import TextButton from 'react-native-button';

class ChangePasswordModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      loading: false,
    };
  }

  onDone = () => {
    this.props.onDone();
  };

  onCancel = () => {
    this.props.onCancel();
  };

  onPress = event => {
    this.props.onDone();
  };

  onChangeValue = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  onChangePassword = () => {
    const {oldPassword, newPassword, confirmPassword} = this.state;
    if (oldPassword === '' || newPassword === '' || confirmPassword === '') {
      Alert.alert('Please fill in the inputs correctly!');
      return;
    }
    if (newPassword !== confirmPassword) {
      Alert.alert('New password does not match!');
      return;
    }

    this.setState({
      loading: true,
    });

    authManager
      .updatePassword(oldPassword, newPassword)
      .then(result => {
        if (result) {
          Alert.alert('Password changed successfully!', '', [
            {text: 'OK', onPress: this.props.onDone},
          ]);
        } else {
          Alert.alert('Old password is incorrect!');
        }

        this.setState({
          loading: false,
        });
      })
      .catch(err => {
        console.error(err);
        this.setState({
          loading: false,
        });
      });
  };

  render() {
    const {oldPassword, newPassword, confirmPassword, loading} = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        {loading && (
          <View style={styles.loadingView}>
            <ActivityIndicator size="large" color={AppStyles.color.main} />
          </View>
        )}

        <View style={styles.body}>
          <View style={ModalHeaderStyle.bar}>
            <Text style={ModalHeaderStyle.title}>
              {IMLocalized('Change Password')}
            </Text>
            <TextButton
              style={[ModalHeaderStyle.rightButton, styles.rightButton]}
              onPress={this.onCancel}>
              Cancel
            </TextButton>
          </View>

          <View style={styles.row}>
            <Text style={styles.label}>{IMLocalized('Old Password')}</Text>
            <TextInput
              placeholder={IMLocalized('Old Password')}
              style={styles.textInput}
              secureTextEntry={true}
              value={oldPassword}
              onChangeText={text => this.onChangeValue('oldPassword', text)}
            />
          </View>

          <View style={styles.row}>
            <Text style={styles.label}>{IMLocalized('New Password')}</Text>
            <TextInput
              placeholder={IMLocalized('New Password')}
              style={styles.textInput}
              secureTextEntry={true}
              value={newPassword}
              onChangeText={text => this.onChangeValue('newPassword', text)}
            />
          </View>

          <View style={styles.row}>
            <Text style={styles.label}>{IMLocalized('Confirm Password')}</Text>
            <TextInput
              placeholder={IMLocalized('Confirm Password')}
              style={styles.textInput}
              secureTextEntry={true}
              value={confirmPassword}
              onChangeText={text => this.onChangeValue('confirmPassword', text)}
            />
          </View>

          <View style={styles.row}>
            <Text onPress={this.onChangePassword} style={styles.changePwd}>
              {IMLocalized('Change Password')}
            </Text>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    width: '100%',
    height: '100%',
  },
  loadingView: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    marginVertical: 20,
    marginHorizontal: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  label: {
    marginVertical: 10,
  },
  textInput: {
    width: '50%',
  },
  changePwd: {
    width: '90%',
    minHeight: 45,
    borderWidth: 1,
    color: AppStyles.color.text,
    fontSize: 15,
    paddingVertical: 10,
    borderColor: AppStyles.color.grey,
    borderRadius: 5,
    textAlign: 'center',
  },
  rightButton: {
    marginRight: 10,
  },
});

export default ChangePasswordModal;
