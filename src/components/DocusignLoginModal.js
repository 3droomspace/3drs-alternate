import React from 'react';
import {
  Modal,
  StyleSheet,
  ActivityIndicator,
  View,
  Text,
  Alert,
} from 'react-native';
import TextButton from 'react-native-button';
import {WebView} from 'react-native-webview';
import {encode as btoa} from 'base-64';

import {ModalHeaderStyle} from '/AppStyles';

const baseUri = 'https://account-d.docusign.com/oauth';
// const DOCUSIGN_INTEGRATION_KEY = '28d48e98-5385-4fd1-96e1-f2792ef947ef';
// const DOCUSIGN_SECRET_KEY = '50174b01-f3f0-4773-8637-288773c88f61';
const DOCUSIGN_INTEGRATION_KEY = 'f9083809-ec4e-4aee-b6b1-e81adcb6f276';
const DOCUSIGN_SECRET_KEY = '5a05ab9a-7f63-4470-960e-51ce2c0e2ff0';
const redirect_uri = 'http://example.com/callback/';
const CALLBACK = encodeURIComponent(redirect_uri);
const SCOPE = 'signature';

class DocusignLoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.authUrl = `${baseUri}/auth?response_type=code&client_id=${DOCUSIGN_INTEGRATION_KEY}&scope=${SCOPE}&redirect_uri=${CALLBACK}`;
    console.log(this.authUrl);
  }

  showURL2 = async event => {
    // console.log('showUrl', event.nativeEvent.url);
    if (event.nativeEvent.url) {
      if (event.nativeEvent.url.indexOf(redirect_uri) === 0) {
        const code = event.nativeEvent.url.split('code=')[1].split('&')[0];

        if (this.code) {
          return null;
        } else {
          this.code = code;
        }

        fetch(`${baseUri}/token`, {
          method: 'POST',
          headers: {
            Authorization:
              'Basic ' +
              btoa(`${DOCUSIGN_INTEGRATION_KEY}:${DOCUSIGN_SECRET_KEY}`),
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            grant_type: 'authorization_code',
            code: code,
          }),
        })
          .then(resp => resp.json())
          .then(resp => {
            this.props.gotAccessToken(resp);
          })
          .catch(err => {
            Alert.alert(err.toString());
          });
        /* eslint-enable */
      }
    }
  };

  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <ActivityIndicator
        color="#009688"
        size="large"
        style={styles.ActivityIndicatorStyle}
      />
    );
  }

  onCancel = () => {
    this.props.onCancel();
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        <View style={ModalHeaderStyle.bar}>
          <Text style={ModalHeaderStyle.title}>Login to Docusign</Text>
          <TextButton
            style={[ModalHeaderStyle.rightButton, styles.rightButton]}
            onPress={this.onCancel}>
            Cancel
          </TextButton>
        </View>

        <WebView
          style={styles.WebViewStyle}
          source={{uri: this.authUrl}}
          javaScriptEnabled={true}
          sharedCookiesEnabled={false}
          domStorageEnabled={true}
          cacheEnabled={false}
          onLoad={this.showURL2}
          onLoadStart={this.showURL2}
          incognito={true}
          renderLoading={this.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />
      </Modal>
    );
  }
}
export default DocusignLoginModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
