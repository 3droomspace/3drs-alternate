import React from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {setLocation} from '@Redux/app';

import AutoLocationInput from './Forms/LocationInput/CityInput';
import TextButton from 'react-native-button';
import {AppStyles, ModalHeaderStyle} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';
import Geocoder from 'react-native-geocoding';
import {Configuration} from '/Configuration';
import RangeSlider from 'rn-range-slider';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import Icon from 'react-native-vector-icons/Feather';

class LocationSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      location: {},
      value3Index: 0,
      setLocationIndex: false,
      isRenter: this.props.user.userType === 'renter' ? 1 : 0,
      price: 0,
      rangeLow: 1,
      rangeHigh: 40,
    };
  }

  setLocation = () => {
    Geocoder.init(Configuration.google_map_key);

    Geocoder.from(this.state.location.address)
      .then(json => {
        // let addrComponents = json.results[0].address_components;

        this.props.setLocation({
          address: this.state.location.address,
          geometry: json.results[0].geometry,
          addressComponents: json.results[0].address_components,
        });

        if (this.props.hideModal) {
          this.props.hideModal();
        }
      })
      .catch(error => {
        this.props.setLocation({
          address: this.state.location.address,
          location: {latitude: 0, longitude: 0},
          addressComponents: null,
        });

        if (this.props.hideModal) {
          this.props.hideModal();
        }
      });
  };

  onChangeLocation = loc => {
    this.setState({
      location: {
        latitude: loc.latitude,
        longitude: loc.longitude,
        address: loc.address,
      },
    });
  };

  nextSetLocationModal = () => {
    this.setState({setLocationIndex: true});
  };

  onPress = value => {
    this.setState({value3Index: value});
  };

  onPressDeposit = value => {
    this.setState({value3Index: value});
  };

  inputChange = e => {
    this.setState({inputValue: parseInt(e.target.value, 10)});
    // if (!this.state.isThirdTileActive) {
    //   this.showThirdTile(true);
    // }
  };

  onCancel = () => {
    this.props.hideModal();
  };

  render() {
    var radio_props = [
      {label: 'Shared', value: 0},
      {label: 'Private', value: 1},
    ];

    var radio_deposit = [
      {label: 'Shared', value: 0},
      {label: 'Private', value: 1},
    ];

    const lowPrice = this.state.rangeLow + 'K';
    const highPrice = this.state.rangeHigh + 'K';

    return !this.state.isRenter ? (
      <View>
        <Modal animationType="slide" transparent={false}>
          <View style={{...ModalHeaderStyle.body, ...styles.modalContainer}}>
            <TouchableOpacity style={styles.backButton} onPress={this.onCancel}>
              <Icon name={'chevron-left'} size={28} color={'#2169c0'} />
              <Text style={styles.backButtonLabel}>{'Cancel'}</Text>
            </TouchableOpacity>
            <View style={styles.locationInputWrapper}>
              <Text style={styles.labelText}>What is the City?</Text>

              <View style={styles.locationWrapper}>
                <AutoLocationInput
                  onChangeLocation={this.onChangeLocation}
                  initialValue={
                    this.props.location ? this.props.location.address : ''
                  }
                  showSearch={true}
                />
              </View>
            </View>
            <View>
              <Text style={styles.labelText}>What type of listing?</Text>
              <RadioForm
                style={{
                  width: '100%',
                  height: 80,
                  marginBottom: 0,
                  marginTop: 30,
                }}
                formHorizontal={true}
                animation={true}>
                {/* To create radio buttons, loop through your array of options */}
                {radio_props.map((obj, i) => (
                  <RadioButton labelHorizontal={true} key={i}>
                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                    <RadioButtonInput
                      obj={obj}
                      index={i}
                      isSelected={this.state.value3Index === i}
                      onPress={this.onPress}
                      borderWidth={1}
                      selectedButtonColor={'#4297f4'}
                      buttonInnerColor={'#4297f4'}
                      buttonOuterColor={
                        this.state.value3Index === i ? '#4297f4' : '#9b9b9b'
                      }
                      buttonSize={20}
                      buttonOuterSize={20}
                      buttonStyle={{}}
                      buttonWrapStyle={{marginLeft: 30}}
                    />
                    <RadioButtonLabel
                      obj={obj}
                      index={i}
                      labelHorizontal={true}
                      onPress={this.onPress}
                      labelStyle={{fontSize: 20, color: '#9b9b9b'}}
                      labelWrapStyle={{marginRight: 30}}
                    />
                  </RadioButton>
                ))}
              </RadioForm>
            </View>

            <TextButton
              disabled={!this.state.location.address}
              containerStyle={styles.continueButton}
              style={styles.continueButtonText}
              onPress={this.setLocation}>
              Continue
            </TextButton>
          </View>
        </Modal>

        {!this.state.setLocationIndex && (
          <Modal animationType="slide" transparent={false}>
            <View style={{...ModalHeaderStyle.body, ...styles.modalContainer}}>
              <View style={styles.locationInputWrapper}>
                <Text style={styles.labelText}>What is the rent amount?</Text>

                <View style={styles.locationWrapper}>
                  <Text
                    style={{
                      fontSize: 20,
                      borderBottomWidth: 2,
                      borderBottomColor: '#3bcdc5',
                      paddingBottom: 12,
                      paddingTop: 12,
                    }}>
                    $
                  </Text>
                  <TextInput
                    style={{
                      fontSize: 20,
                      borderBottomWidth: 2,
                      borderBottomColor: '#3bcdc5',
                      width: '90%',
                    }}
                    placeholder="0000"
                    keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={price => this.setState({price})}
                  />
                </View>
              </View>

              <View>
                <Text style={styles.labelText}>Is there a Deposit?</Text>
                <RadioForm
                  style={{
                    width: '100%',
                    height: 80,
                    marginBottom: 0,
                    marginTop: 30,
                  }}
                  formHorizontal={true}
                  animation={true}>
                  {/* To create radio buttons, loop through your array of options */}
                  {radio_deposit.map((obj, i) => (
                    <RadioButton labelHorizontal={true} key={i}>
                      {/*  You can set RadioButtonLabel before RadioButtonInput */}
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={this.state.value3Index === i}
                        onPress={this.onPressDeposit}
                        borderWidth={1}
                        selectedButtonColor={'#4297f4'}
                        buttonInnerColor={'#4297f4'}
                        buttonOuterColor={
                          this.state.value3Index === i ? '#4297f4' : '#9b9b9b'
                        }
                        buttonSize={20}
                        buttonOuterSize={20}
                        buttonStyle={{}}
                        buttonWrapStyle={{marginLeft: 30}}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        labelHorizontal={true}
                        onPress={this.onPressDeposit}
                        labelStyle={{fontSize: 20, color: '#9b9b9b'}}
                        labelWrapStyle={{marginRight: 30}}
                      />
                    </RadioButton>
                  ))}
                </RadioForm>
              </View>

              <TextButton
                // disabled={!this.state.location.address}
                containerStyle={styles.continueButton}
                style={styles.continueButtonText}
                onPress={this.nextSetLocationModal}>
                Continue
              </TextButton>
            </View>
          </Modal>
        )}
      </View>
    ) : (
      <Modal animationType="slide" transparent={false}>
        <ScrollView>
          <View style={{...ModalHeaderStyle.body, ...styles.modalContainer}}>
            <TouchableOpacity style={styles.backButton} onPress={this.onCancel}>
              <Icon name={'chevron-left'} size={28} color={'#2169c0'} />
              <Text style={styles.backButtonLabel}>{'Cancel'}</Text>
            </TouchableOpacity>
            <View style={styles.locationInputWrapper}>
              <Text style={styles.labelText}>Where do you want to live?</Text>

              <View style={styles.locationWrapper}>
                <AutoLocationInput
                  onChangeLocation={this.onChangeLocation}
                  initialValue={
                    this.props.location ? this.props.location.address : ''
                  }
                  showSearch={true}
                />
              </View>
            </View>
            <View>
              <Text style={styles.labelText}>What is your price range?</Text>
              <RangeSlider
                style={{width: '100%', height: 80}}
                gravity={'center'}
                min={1}
                max={40}
                step={1}
                thumbRadius={0}
                textFormat={'%dk'}
                selectionColor="#ae5de4"
                labelBackgroundColor="#ffffff"
                labelBorderColor="#ffffff"
                labelTextColor="#000000"
                blankColor="#d8d8d8"
                onValueChanged={(low, high, fromUser) => {
                  this.setState({rangeLow: low, rangeHigh: high});
                }}
              />
              <View style={styles.rangeLabelWrapper}>
                <Text style={styles.rangeLabel}>{lowPrice}</Text>
                <Text style={styles.rangeLabel}>{highPrice}</Text>
              </View>
            </View>

            <View>
              <Text style={styles.labelText}>What type of listing?</Text>
              <RadioForm
                style={{
                  width: '100%',
                  height: 80,
                  marginBottom: 0,
                  marginTop: 30,
                }}
                formHorizontal={true}
                animation={true}>
                {/* To create radio buttons, loop through your array of options */}
                {radio_props.map((obj, i) => (
                  <RadioButton labelHorizontal={true} key={i}>
                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                    <RadioButtonInput
                      obj={obj}
                      index={i}
                      isSelected={this.state.value3Index === i}
                      onPress={this.onPress}
                      borderWidth={1}
                      selectedButtonColor={'#4297f4'}
                      buttonInnerColor={'#4297f4'}
                      buttonOuterColor={
                        this.state.value3Index === i ? '#4297f4' : '#9b9b9b'
                      }
                      buttonSize={20}
                      buttonOuterSize={20}
                      buttonStyle={{}}
                      buttonWrapStyle={{marginLeft: 30}}
                    />
                    <RadioButtonLabel
                      obj={obj}
                      index={i}
                      labelHorizontal={true}
                      onPress={this.onPress}
                      labelStyle={{fontSize: 20, color: '#9b9b9b'}}
                      labelWrapStyle={{marginRight: 30}}
                    />
                  </RadioButton>
                ))}
              </RadioForm>
            </View>

            <TextButton
              disabled={!this.state.location.address}
              containerStyle={styles.continueButton}
              style={styles.continueButtonText}
              onPress={this.setLocation}>
              Continue
            </TextButton>
          </View>
        </ScrollView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    paddingHorizontal: 30,
  },
  labelText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 30,
    marginTop: 20,
  },
  locationWrapper: {
    marginTop: 15,
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  continueButton: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    borderRadius: 5,
    padding: 15,
    zIndex: -1,
  },
  continueButtonText: {
    color: AppStyles.color.white,
    fontWeight: 'bold',
    fontSize: 15,
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    left: -10,
    zIndex: 10000,
  },
  backButtonLabel: {
    fontSize: 16,
    marginLeft: -5,
    marginTop: -2.5,
    color: '#2169c0',
    fontFamily: AppStyles.fontName.main,
  },
  rangeLabelWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rangeLabel: {
    fontSize: 16,
    fontFamily: AppStyles.fontName.main,
    fontWeight: 'bold',
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.app.location,
});

const mapDispatchToProps = {
  setLocation,
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelect);
