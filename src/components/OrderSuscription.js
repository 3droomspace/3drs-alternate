import React from 'react';
import {
  View,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

const OrderSuscription = props => {
  return (
    <Modal style={styles.modal}>
      <View style={styles.screen}>
        <TouchableOpacity
          onPress={() => {
            props.onPrimeCancel();
          }}
          style={styles.backButton}>
          <Text style={styles.backButtonText}>Back</Text>
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>My Listings</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../assets/Images/Intro/add-comments.png')}
          />
        </View>
        <View style={styles.titleDescContainer}>
          <Text style={styles.titleDesc}>
            Looks like you want to add another listing.
          </Text>
        </View>
        <View style={styles.descriptionConatiner}>
          <Text style={styles.description}>
            To do that simply click the button below to pay the $200.00 fee to
            be a part of our Prime Subscription, which allows as many listings
            as you can possibly imagine.* We can’t wait to have you part of our
            community of Managers who list their spaces with us.
          </Text>
        </View>
        <View style={styles.orderButtonContainer}>
          <TouchableOpacity style={styles.orderButton}>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
              $200 Prime Suscription
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity>
          <Text style={styles.terms}>*Terms and Conditions apply.</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    // flex: 1,
  },
  titleContainer: {
    height: '15%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'black',
    shadowOffset: {
      height: 0,
      width: 1,
    },
    shadowRadius: 4,
    shadowOpacity: 0.5,
    backgroundColor: '#F8F8FF',
    paddingTop: 35,
  },
  screen: {
    alignItems: 'center',
    height: '100%',
    flex: 1,
    // paddingTop: 50
  },
  orderButton: {
    width: '85%',
    alignItems: 'center',
    height: '60%',
    justifyContent: 'center',
    borderRadius: 6,
    backgroundColor: '#3bcdc5',
  },
  orderButtonContainer: {
    width: '100%',
    alignItems: 'center',
    height: '11%',
    justifyContent: 'center',
    marginTop: 10,
  },
  imageContainer: {
    height: '30%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 50,
    marginTop: 10,
  },
  image: {
    height: 235,
    width: 250,
  },
  titleDescContainer: {
    width: '60%',
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionConatiner: {
    width: '70%',
    marginVertical: 20,
  },
  title: {
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    fontFamily: 'Avenir',
    color: 'grey',
    fontSize: 16.5,
    textAlign: 'center',
  },
  titleDesc: {
    fontFamily: 'Avenir',
    fontSize: 17,
    textAlign: 'center',
  },
  backButton: {
    position: 'absolute',
    zIndex: 1,
    left: 18,
    top: 45,
  },
  terms: {
    color: 'grey',
  },
  backButtonText: {
    color: 'black',
    fontSize: 16,
  },
});

export default OrderSuscription;
