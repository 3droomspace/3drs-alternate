import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  BackHandler,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import SavedButton from '@Components/SavedButton';
import StarRating from 'react-native-star-rating';
import {firebaseUser, firebaseReview, firebaseListing} from '@Firebase';
import ProfileImageCard from './ProfileImageCard';
import {
  AppStyles,
  AppIcon,
  TwoColumnListStyle,
  HeaderButtonStyle,
} from '/AppStyles';
import HeaderButton from './HeaderButton';
import {Configuration} from '/Configuration';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';
import ReviewModal from './ReviewModal';
import {timeFormat} from '@Helpers/timeFormat';

const defaultAvatar =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

class ListingProfileModal extends Component {
  static navigationOptions = ({screenProps, navigation}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('Profile'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
      headerRight: (
        <View style={HeaderButtonStyle.multi}>
          <HeaderButton
            customStyle={styles.headerIconContainer}
            iconStyle={styles.headerIcon}
            icon={AppIcon.images.review}
            onPress={() => {
              navigation.state.params.onPressReview();
            }}
          />
        </View>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.reviewsUnsubscribe = null;
    this.item = {id: props.navigation.getParam('userID')};

    this.state = {
      user: null,
      listings: [],
      data: this.item ? this.item : {},
      reviews: [],
      isListingDetailVisible: false,
      reviewModalVisible: false,
      selectedItem: null,
      loading: true,
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.userID = props.navigation.getParam('userID');
    this.listingItemActionSheet = React.createRef();
    this.listingsUnsubscribe = null;
  }

  onChangeCurrentUser(data) {
    if (data) {
      this.setState({
        user: data,
        loading: false,
      });
    } else {
      this.onGetUserError();
    }
  }

  async componentDidMount() {
    this.userUnsubscribe = firebaseUser.subscribeCurrentUser(
      this.userID,
      this.onChangeCurrentUser.bind(this),
    );

    this.props.navigation.setParams({
      onPressReview: this.onPressReview,
    });

    this.listingsUnsubscribe = firebaseListing.subscribeListings(
      {userId: this.userID},
      this.onListingsCollectionUpdate,
    );

    this.reviewsUnsubscribe = firebaseReview.subscribeReviews({
      field: 'userID',
    })(this.userID, this.onReviewsUpdate);

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  onGetUserError = () => {
    this.setState({loading: false}, () => {
      Alert.alert(
        '0ops! an error occured  while loading profile. This user profile may be incomplete.',
      );
      this.props.navigation.goBack();
    });
  };

  componentWillUnmount() {
    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }
    if (this.userUnsubscribe) {
      this.userUnsubscribe();
    }
    if (this.unsubscribe) {
      this.unsubscribe();
    }
    if (this.reviewsUnsubscribe) {
      this.reviewsUnsubscribe();
    }

    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onListingsCollectionUpdate = querySnapshot => {
    const listings = [];
    querySnapshot.forEach(doc => {
      const listing = doc.data();

      listings.push({...listing, id: doc.id});
    });

    this.setState({
      listings,
    });
  };

  onPressListingItem = item => {
    this.props.navigation.navigate('ListingProfileModalDetailsScreen', {item});

    this.unsubscribe = firebaseListing.subscribeListings(
      {docId: item.id},
      this.onDocUpdate,
    );
    this.reviewsUnsubscribe = firebaseReview.subscribeReviews(
      item.id,
      this.onReviewsUpdate,
    );
  };

  onPressReview = () => {
    this.setState({reviewModalVisible: true});
  };

  updateReviews = reviews => {
    this.setState({
      reviews: reviews,
    });
  };

  onDocUpdate = doc => {
    const listing = doc.data();

    this.setState(
      {
        data: {...listing, id: doc.id},
        loading: false,
      },
      () => {},
    );
  };

  onReviewCancel = () => {
    this.setState({reviewModalVisible: false});
  };

  onReviewsUpdate = (querySnapshot, usersRef) => {
    const data = [];
    const updateReviews = this.updateReviews;

    querySnapshot.forEach(doc => {
      const review = doc.data();

      usersRef
        .doc(review.authorID)
        .get()
        .then(function(userDoc) {
          data.push({...review, id: doc.id, name: userDoc.data().fullname});
          updateReviews(data);
        });
    });
  };

  // onListingDetailCancel = () => {
  //   this.setState({
  //     isListingDetailVisible: false,
  //   })
  // }

  onPressSavedIcon = item => {
    firebaseListing.saveUnsaveListing(item, this.props.user.id);
  };

  renderReviewItem = ({item}) => (
    <View style={styles.reviewItem}>
      <View style={styles.info}>
        <FastImage
          style={styles.userPhoto}
          resizeMode={FastImage.resizeMode.cover}
          source={
            item.profilePictureURL
              ? {uri: item.profilePictureURL}
              : {uri: defaultAvatar}
          }
        />
        <View style={styles.detail}>
          <Text style={styles.username}>
            {item.firstName && item.firstName} {item.lastName && item.lastName}
          </Text>
          <Text style={styles.reviewTime}>{timeFormat(item.createdAt)}</Text>
        </View>
        <StarRating
          containerStyle={styles.starRatingContainer}
          disabled={true}
          maxStars={5}
          starSize={22}
          starStyle={styles.starStyle}
          emptyStar={AppIcon.images.starNoFilled}
          fullStar={AppIcon.images.starFilled}
          halfStarColor={DynamicAppStyles.colorSet.mainThemeForegroundColor}
          rating={item.starCount}
        />
      </View>
      <Text style={styles.reviewContent}>{item.content}</Text>
    </View>
  );

  renderListingItem = ({item}) => {
    return (
      <TouchableOpacity onPress={() => this.onPressListingItem(item)}>
        <View style={TwoColumnListStyle.listingItemContainer}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto}
            source={{uri: item.photo}}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon}
            onPress={() => this.onPressSavedIcon(item)}
            item={item}
          />
          <Text numberOfLines={1} style={TwoColumnListStyle.listingName}>
            {item.title}
          </Text>
          <Text style={TwoColumnListStyle.listingPlace}>{item.place}</Text>
          <StarRating
            containerStyle={styles.starRatingContainer}
            maxStars={5}
            starSize={15}
            disabled={true}
            starStyle={styles.starStyle}
            emptyStar={AppIcon.images.starNoFilled}
            fullStar={AppIcon.images.starFilled}
            halfStarColor={DynamicAppStyles.colorSet.mainThemeForegroundColor}
            rating={item.starCount}
          />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {user} = this.state;

    if (this.state.loading) {
      return <ActivityIndicator size="small" color={AppStyles.color.main} />;
    }

    if (!this.state.loading && !this.state.user) {
      return null;
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <View style={styles.profileCardContainer}>
            <ProfileImageCard disabled={true} user={user} />
          </View>
          <View style={styles.profileItemContainer}>
            <View style={styles.detailContainer}>
              <Text style={styles.profileInfo}>{'Profile Info'}</Text>
              <View style={styles.profileInfoContainer}>
                <View style={styles.profileInfoTitleContainer}>
                  <Text style={styles.profileInfoTitle}>{'Rent Score :'}</Text>
                </View>
                <View style={styles.profileInfoValueContainer}>
                  <Text style={styles.profileInfoValue}>
                    {/* {user.phoneNumber ? user.phoneNumber : ""} */}
                    <StarRating
                      containerStyle={styles.starRatingContainer}
                      maxStars={5}
                      starSize={15}
                      disabled={true}
                      starStyle={styles.starStyle}
                      emptyStar={AppIcon.images.starNoFilled}
                      fullStar={AppIcon.images.starFilled}
                      halfStar={AppIcon.images.starHalfFilled}
                      halfStarColor={
                        DynamicAppStyles.colorSet.mainThemeForegroundColor
                      }
                      rating={this.state.user.starCount}
                    />
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.gridContainer}>
              <Text style={styles.myListings}>{'Listings'}</Text>
              <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                numColumns={2}
                data={this.state.listings}
                renderItem={item => this.renderListingItem(item)}
                keyExtractor={item => `${item.id}`}
              />
              {this.state.reviews.length > 0 && (
                <Text style={[styles.title, styles.reviewTitle]}>
                  {' '}
                  {IMLocalized('Reviews')}{' '}
                </Text>
              )}
              <FlatList
                data={this.state.reviews}
                renderItem={this.renderReviewItem}
                keyExtractor={item => `${item.id}`}
                initialNumToRender={5}
              />
              {this.state.reviewModalVisible && (
                <ReviewModal
                  field="userID"
                  data={this.state.data}
                  onCancel={this.onReviewCancel}
                  onDone={this.onReviewCancel}
                />
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

ListingProfileModal.propTypes = {
  user: PropTypes.object,
  onModal: PropTypes.func,
  isProfileModalVisible: PropTypes.bool,
  presentationStyle: PropTypes.string,
};

const itemIconSize = 26;
const itemNavigationIconSize = 23;

const reviewStyles = StyleSheet.create({
  headerIconContainer: {
    marginRight: 10,
  },
  headerIcon: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    height: 17,
    width: 17,
  },
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    color: AppStyles.color.title,
    fontSize: 25,
    padding: 10,
  },
  reviewTitle: {
    paddingTop: 0,
  },
  description: {
    fontFamily: AppStyles.fontName.bold,
    padding: 10,
    color: AppStyles.color.description,
  },
  photoItem: {
    backgroundColor: AppStyles.color.grey,
    height: 250,
    width: '100%',
  },
  paginationContainer: {
    flex: 1,
    position: 'absolute',
    alignSelf: 'center',
    paddingVertical: 8,
    marginTop: 220,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
  },
  mapView: {
    width: '100%',
    height: 200,
    // backgroundColor: AppStyles.color.grey
  },
  loadingMap: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  extra: {
    padding: 30,
    paddingTop: 10,
    paddingBottom: 0,
    marginBottom: 30,
  },
  extraRow: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  extraKey: {
    flex: 2,
    color: AppStyles.color.title,
    fontWeight: 'bold',
  },
  extraValue: {
    flex: 1,
    color: '#bcbfc7',
  },
  reviewItem: {
    padding: 10,
    marginLeft: 10,
  },
  info: {
    flexDirection: 'row',
  },
  userPhoto: {
    width: 44,
    height: 44,
    borderRadius: 22,
  },
  detail: {
    paddingLeft: 10,
    flex: 1,
  },
  username: {
    color: AppStyles.color.title,
    fontWeight: 'bold',
  },
  reviewTime: {
    color: '#bcbfc7',
    fontSize: 12,
  },
  starRatingContainer: {
    padding: 10,
  },
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
  reviewContent: {
    color: AppStyles.color.title,
    marginTop: 10,
  },
});

const styles = StyleSheet.create({
  ...reviewStyles,
  cardContainer: {
    flex: 1,
  },
  cardImageContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardImage: {
    height: 130,
    width: 130,
    borderRadius: 65,
  },
  gridContainer: {
    padding: Configuration.home.listing_item.offset,
    marginTop: 10,
  },
  cardNameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardName: {
    color: AppStyles.color.text,
    fontSize: 19,
  },
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  profileCardContainer: {
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileItemContainer: {
    marginTop: 6,
  },
  itemContainer: {
    flexDirection: 'row',
    height: 54,
    width: '85%',
    alignSelf: 'center',
    marginBottom: 10,
  },
  itemIconContainer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemIcon: {
    height: itemIconSize,
    width: itemIconSize,
  },
  itemTitleContainer: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  itemTitle: {
    color: AppStyles.color.text,
    fontSize: 17,
    paddingLeft: 20,
  },
  itemNavigationIconContainer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightButton: {
    marginRight: 10,
    color: AppStyles.color.main,
  },
  itemNavigationIcon: {
    height: itemNavigationIconSize,
    width: itemNavigationIconSize,
    tintColor: AppStyles.color.grey,
  },
  detailContainer: {
    backgroundColor: '#efeff4',
    padding: 20,
    marginTop: 25,
  },
  profileInfo: {
    padding: 5,
    color: '#333333',
    fontSize: 14,
  },
  myListings: {
    paddingTop: 5,
    paddingBottom: 20,
    fontWeight: '500',
    color: '#333333',
    fontSize: 17,
  },
  profileInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 5,
    width: '100%',
  },
  profileInfoTitleContainer: {
    flex: 1,
    // alignItems: "center",
    // backgroundColor: "green",
    justifyContent: 'center',
  },
  profileInfoTitle: {
    color: '#595959',
    fontSize: 12,
    padding: 5,
  },
  profileInfoValueContainer: {
    flex: 2,
    // alignItems: "center",
    // backgroundColor: "yellow",
    justifyContent: 'center',
  },
  profileInfoValue: {
    color: '#737373',
    fontSize: 12,
    padding: 5,
  },
  footerButtonContainer: {
    flex: 2,
    justifyContent: 'flex-start',
    marginTop: 8,
  },
  footerContainerStyle: {
    // borderColor: AppStyles.color.grey
  },
  blank: {
    flex: 0.5,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(ListingProfileModal);
