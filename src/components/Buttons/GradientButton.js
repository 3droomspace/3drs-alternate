import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

const colors = {
  blue: {
    start: {x: 0.0, y: 1.0},
    end: {x: 0.9, y: 0.2},
    colors: ['#1716eb', '#7b1bf2'],
    locations: [0.3, 1],
  },
  blueGreen: {
    start: {x: 0.0, y: 0.8},
    end: {x: 1.0, y: 0.2},
    colors: ['#00B6E3', '#00D5BE'],
  },
  green: {
    start: {x: 0.0, y: 1.0},
    end: {x: 1, y: 0.0},
    colors: ['#31C394', '#1EE6C0'],
    locations: [0.5, 1],
  },
};

export default class ApplyGradient extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={this.props.containerStyle}>
        <LinearGradient
          start={colors[this.props.color].start}
          end={colors[this.props.color].end}
          colors={colors[this.props.color].colors}
          locations={colors[this.props.color].locations}
          style={[styles.LinearGradient, this.props.style]}>
          <BtnText>{this.props.children}</BtnText>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  LinearGradient: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    // borderRadius: 13,
    textAlign: 'center',
    justifyContent: 'center',

    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: 'green',
    shadowOffset: {width: 0, height: 11},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
});

const BtnText = styled.Text`
  font-family: HelveticaNeue;
  align-self: center;
  font-size: 18px;
  font-weight: bold;
  color: white;
`;
