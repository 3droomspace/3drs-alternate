import React from 'react';
import {
  Modal,
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  AsyncStorage,
} from 'react-native';
import TextButton from 'react-native-button';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/FontAwesome';

import {AppStyles, ModalHeaderStyle} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

import CheckBox from './Forms/Checkbox/Checkbox';
import DocusignLoginModal from './DocusignLoginModal';
import {
  getOrRenewAccessToken,
  uploadDocumentToServer,
} from '../helpers/docusign';

export class RequestDocumentsModal extends React.Component {
  constructor(props) {
    super(props);

    console.log(this.props.renter);
    this.state = {
      requestID: false,
      documentFiles: [],
      loading: false,
      showDocusign: false,
    };
  }

  onCancel = () => {
    this.props.onCancel();
  };

  onToggleRequestID = () => {
    this.setState({
      requestID: !this.state.requestID,
    });
  };

  uploadDocument = async () => {
    // try {
    //   const doc = await DocumentPicker.pick({
    //     type: [DocumentPicker.types.pdf],
    //   });

    //   this.setState({
    //     documentFiles: [...this.state.documentFiles, doc],
    //   });
    // } catch (err) {
    //   if (DocumentPicker.isCancel(err)) {
    //   }
    // }
    this.setState({
      showDocusign: true,
      loading: false,
    });
  };

  removeDocument = index => {
    const newDocumentFiles = [...this.state.documentFiles];
    newDocumentFiles.splice(index, 1);
    this.setState({
      documentFiles: newDocumentFiles,
    });
  };

  uploadDocumentsToServer = () => {
    console.log('$$$$', this.props)
    uploadDocumentToServer(
      true,
      this.state.documentFiles,
      this.props.renter.email,
      this.props.renter.firstName + ' ' + this.props.renter.lastName,
      this.props.data,
    )
      .then(() => {
        Alert.alert('Docusign Email was sent!');

        this.setState({
          loading: false,
        });
        this.props.onCancel(this.state.requestID, true);
      })
      .catch(err => {
        console.error(err);
        Alert.alert(err.toString());

        this.setState({
          loading: false,
        });
        this.props.onCancel(this.state.requestID, true);
      });
  };

  submit = async () => {
    this.setState({
      loading: true,
    });

    this.uploadDocumentsToServer();

    // if (this.state.documentFiles && this.state.documentFiles.length) {
    //   getOrRenewAccessToken()
    //     .then(accessToken => {
    //       console.log('Docusign Access Token ===:', accessToken)
    //       if (accessToken) {
    //         this.uploadDocumentsToServer(accessToken);
    //       } else {
    //         this.setState({
    //           showDocusign: true,
    //         });
    //       }
    //     })
    //     .catch(err => {
    //       Alert.alert(err.toString());
    //       this.setState({
    //         loading: false,
    //       });
    //     });
    // } else {
    //   this.setState({
    //     loading: false,
    //   });
    //   this.props.onCancel(this.state.requestID, false);
    // }
  };

  closeDocusignModal = () => {
    this.setState({
      showDocusign: false,
      loading: false,
    });
  };

  gotAccessToken = response => {
    this.setState({
      showDocusign: false,
    });

    response = {
      ...response,
      updated: Date.now(),
    };

    AsyncStorage.setItem('docusignAuth', JSON.stringify(response)).then(() =>
      this.uploadDocumentsToServer(response.access_token),
    );
  };

  render() {
    const {documentFiles, loading, showDocusign} = this.state;

    const documents = documentFiles.map((doc, index) => {
      return (
        <View style={styles.docItem}>
          <View style={styles.docInfo}>
            <Text style={styles.docFileName}>{doc.name}</Text>
            <Text style={styles.docFileSize}>Size: {doc.size} KB</Text>
          </View>

          <TouchableOpacity
            style={styles.removeButton}
            onPress={() => this.removeDocument(index)}>
            <Icon name="trash" size={20} color="red" />
          </TouchableOpacity>
        </View>
      );
    });

    return (
      <Modal
        animationType="slide"
        transparent={false}
        onRequestClose={this.onCancel}>
        {loading && (
          <View style={styles.loadingView}>
            <ActivityIndicator size="large" color={AppStyles.color.main} />
          </View>
        )}

        {showDocusign && (
          <DocusignLoginModal
            onCancel={this.closeDocusignModal}
            gotAccessToken={this.gotAccessToken}
          />
        )}

        <View style={ModalHeaderStyle.bar}>
          <Text style={ModalHeaderStyle.title}>Request Documents</Text>
          <TextButton
            style={[ModalHeaderStyle.rightButton, styles.rightButton]}
            onPress={this.onCancel}>
            Cancel
          </TextButton>
        </View>
        <ScrollView style={styles.body}>
          <View style={styles.row}>
            <Text stlye={styles.label}>Request ID Document</Text>
            <CheckBox
              style={{height: 25}}
              onToggleSwitch={this.onToggleRequestID}
            />
          </View>
          {documentFiles.length > 0 && (
            <View style={styles.docsContainer}>
              <Text style={styles.docsContainerLabel}>Documents to sign:</Text>
              {documents}
            </View>
          )}
          <View style={styles.actionButtonsContainer}>
            <TouchableOpacity
              onPress={this.uploadDocument}
              style={styles.actionButton}>
              <Text style={styles.actionButtonText}>Add Documents to Sign</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.submit} style={styles.actionButton}>
              <Text style={styles.actionButtonText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Modal>
    );
  }
}

export default RequestDocumentsModal;

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: AppStyles.color.white,
    marginTop: 20,
  },
  rightButton: {
    marginRight: 10,
  },
  loadingView: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 5,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  actionButtonsContainer: {
    marginVertical: 50,
    paddingHorizontal: 50,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  actionButton: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    padding: 10,
    borderRadius: 5,
    marginVertical: 5,
  },
  actionButtonText: {
    color: AppStyles.color.white,
    textAlign: 'center',
    fontSize: 16,
  },
  docsContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },
  docItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  docInfo: {},
  docFileName: {
    fontSize: 18,
  },
  docFileSize: {
    marginTop: 5,
    fontSize: 12,
  },
  removeButton: {
    width: 27,
    height: 27,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5,
  },
  docsContainerLabel: {
    marginTop: 20,
    marginBottom: 20,
  },
});
