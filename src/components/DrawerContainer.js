import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity, Text} from 'react-native';
import {connect} from 'react-redux';
import MenuButton from '@Components/MenuButton';
import LinearGradient from 'react-native-linear-gradient';
import DynamicAppStyles from '/DynamicAppStyles';

class DrawerContainer extends React.Component {
  render() {
    const {navigation} = this.props;
    const user =
      this.props.navigation.state.params.user.user ||
      this.props.navigation.getParam('user');

    var userPhotoURI = user.profilePictureURL
      ? {uri: user.profilePictureURL}
      : DynamicAppStyles.iconSet.userAvatar;

    return (
      <LinearGradient
        colors={['#009aca', '#009aca', '#aeefaa', '#fbffc1']}
        locations={[0, 0.4, 1, 1]}
        style={{flex: 1}}>
        <View style={styles.content}>
          <View style={styles.container}>
            <View style={styles.avatargroup}>
              <Image style={styles.avatar} source={userPhotoURI} />
              <View>
                <Text style={styles.nameStyle}>
                  {user.firstName + ' ' + user.lastName}
                </Text>
                <Text style={styles.emailStyle}>{user.email}</Text>
              </View>
            </View>
            <MenuButton
              title="Profile"
              source="user-circle2"
              onPress={() => {
                if (this.props.navigation.state.isDrawerOpen) {
                  this.props.navigation.closeDrawer();
                }
                navigation.navigate('MyProfile');
              }}
            />
            {/* <MenuButton
              title="Messages"
              source="comment2"
              onPress={() => {
                navigation.navigate('Messages');
              }}
            /> */}
            {/* <MenuButton
              title="Bulletin Board"
              source='bell2'
              onPress={() => {
                navigation.navigate('error404');
              }}
            /> */}
            <MenuButton
              title="My Listings"
              source="map2"
              onPress={() => {
                navigation.navigate('MyListingModal');
              }}
            />
            <MenuButton
              title="My Wallet"
              source="wallet2"
              onPress={() => {
                // navigation.navigate('error404');
                navigation.navigate('MyWallet');
              }}
            />
            {/* <MenuButton
              title="Favorites"
              source="heart2"
              onPress={() => {
                navigation.navigate('Favorites');
              }}
            /> */}
            {user.userType === 'renter' && (
              <MenuButton
                title="Rental History"
                source="clock2"
                onPress={() => {
                  navigation.navigate('RentalHistory');
                }}
              />
            )}
            <MenuButton
              title="Contact Us"
              source="envelope2"
              onPress={() => {
                navigation.navigate('Contact');
              }}
            />

            {/* <MenuButton
              title="Link Bank"
              source="bank"
              onPress={() => {
                navigation.navigate('Payment');
              }}
            /> */}
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    // flex: 1,
    paddingTop: 70,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
    paddingHorizontal: 20,
    paddingLeft: 30,
  },
  avatargroup: {
    flexDirection: 'row',
    width: '80%',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 40,
    marginLeft: -40,
    borderRadius: 100,
    // justifyContent: 'space-between',
  },
  avatar: {
    width: DynamicAppStyles.WINDOW_WIDTH * 0.13,
    height: DynamicAppStyles.WINDOW_WIDTH * 0.13,
    resizeMode: 'cover',
    margin: 0,
    padding: 0,
    paddingRight: 30,
    borderWidth: 1,
    borderRadius: DynamicAppStyles.WINDOW_WIDTH,
  },
  nameStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ffffff',
    alignSelf: 'flex-start',
    textAlign: 'left',
    paddingLeft: 20,
    // marginRight: DynamicAppStyles.WINDOW_WIDTH * 0.3,
  },
  emailStyle: {
    fontSize: 12,
    // fontWeight: 'bold',
    color: '#ffffff',
    alignSelf: 'flex-start',
    textAlign: 'left',
    paddingLeft: 20,
    // marginRight: DynamicAppStyles.WINDOW_WIDTH * 0.3,
  },
});

const mapStateToProps = state => ({
  isAdmin: state.auth.user ? state.auth.user.isAdmin : false,
});

export default connect(mapStateToProps)(DrawerContainer);
