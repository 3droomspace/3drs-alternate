import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Button,
} from 'react-native';
import {GoogleAutoComplete} from 'react-native-google-autocomplete';
import LocationItem from './LocationItem';

class AutoComplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
      showSearch: this.props.showSearch,
    };
  }

  render() {
    if (this.state.showSearch) {
      return (
        <View style={styles.wrapper}>
          <View style={styles.container}>
            <GoogleAutoComplete
              GoogleAutoComplete
              apiKey="AIzaSyCyyfSMl88xGTwh9DXBIqXS67DUzLUOmZk"
              debounce={300}
              minLength={2}>
              {({
                handleTextChange,
                locationResults,
                fetchDetails,
                inputValue,
                isSearching,
                clearSearch,
              }) => (
                <React.Fragment>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Search for address..."
                      placeholderTextColor="gray"
                      onChangeText={handleTextChange}
                      value={inputValue}
                      borderWidth={0}
                    />
                    <Button title="X" onPress={clearSearch} />
                  </View>
                  {isSearching && (
                    <ActivityIndicator size="large" color="blue" />
                  )}
                  <ScrollView
                    keyboardShouldPersistTaps={true}
                    keyboardDismissMode="interactive">
                    {locationResults.map(el => (
                      <LocationItem
                        {...el}
                        key={el.id}
                        fetchDetails={fetchDetails}
                        onChangeLocation={this.props.onChangeLocation}
                        clearSearch={clearSearch}
                        inputValue={inputValue}
                        updateAddress={this.props.updateAddress}
                      />
                    ))}
                  </ScrollView>
                </React.Fragment>
              )}
            </GoogleAutoComplete>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    zIndex: 1,
  },
  container: {
    margin: 10,
    color: 'black',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  textInput: {
    height: 40,
    width: '90%',
    paddingHorizontal: 16,
    color: 'black',
  },
  inputWrapper: {
    flexDirection: 'row',
  },
});

export default AutoComplete;
