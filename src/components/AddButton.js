import React from 'react';
import {View, StyleSheet} from 'react-native';
import {FontIcon} from '@Helpers/FontAwesome';

const AddButton = () => {
  return (
    <View style={styles.screen}>
      <View style={styles.button}>
        <View>
          <FontIcon name={'plus'} size={35} color="white" />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    position: 'absolute',
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#3bcdc5',
    alignItems: 'center',
    justifyContent: 'center',
    width: 55,
    height: 55,
    borderRadius: 36,
    position: 'absolute',
    top: -32,
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOffset: {height: 5},
    shadowOpacity: 0.3,
    // borderWidth: 3,
    // borderColor: 'black',
    zIndex: 1,
  },
});

export default AddButton;
