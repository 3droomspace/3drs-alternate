import React from 'react';
import styled from 'styled-components/native';

export default function App(props) {
  const Circle = styled.View`
    width: 20px;
    height: 20px;
    border-radius: 30px;
    background-color: ${props.color};
    margin-right: 10px;
  `;

  return <Circle />;
}
