import React, {useMemo, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';

const ResponsiveImage = props => {
  const [containerWidth, setContainerWidth] = useState(0);
  const onViewLayoutChange = event => {
    const {width} = event.nativeEvent.layout;
    setContainerWidth(width);
  };

  const imageStyles = useMemo(() => {
    const ratio = containerWidth / props.srcWidth;
    return {
      width: containerWidth,
      height: containerWidth,
    };
  }, [containerWidth, props.srcWidth]);

  return (
    <View style={[styles.container, props.style]} onLayout={onViewLayoutChange}>
      <Image
        source={props.src}
        style={[imageStyles, {resizeMode: 'contain', alignSelf: 'center'}]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {width: '100%'},
});

export default ResponsiveImage;
