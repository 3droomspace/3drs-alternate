import React, {Component} from 'react';
import {View} from 'react-native';
import {Calendar} from 'react-native-calendars';

import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';

class ManagerCalendar extends Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('Calendar'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
    };
  };
  render() {
    return (
      <View>
        <Calendar />
      </View>
    );
  }
}

export default ManagerCalendar;
