import React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  BackHandler,
  Alert,
} from 'react-native';

import {connect} from 'react-redux';

import firebase from '@react-native-firebase/app';
import HeaderButton from '@Components/HeaderButton';
import '@react-native-firebase/firestore';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ActionSheet from 'react-native-actionsheet';

import PostModal from './PostModal';
import ListingAppConfig from '/ListingAppConfig';
import ServerConfiguration from '/ServerConfiguration';
import {AppStyles, TwoColumnListStyle, HeaderButtonStyle} from '/AppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';

class MyListingModal extends React.Component {
  static navigationOptions = ({screenProps, navigation}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('My Listings'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
      headerStyle: {marginTop: 11, borderBottomWidth: 0},
      headerRight: (
        <View style={HeaderButtonStyle.multi}>
          <HeaderButton
            customStyle={styles.composeButton}
            icon={DynamicAppStyles.iconSet.compose}
            iconStyle={{size: 150}}
            onPress={() => {
              navigation.state.params.onPressPost();
            }}
          />
        </View>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      listings: [],
      savedListings: [],
      selectedItem: null,
      postModalVisible: false,
      addModalVisible: false,
      categories: [],
    };

    this.listingItemActionSheet = React.createRef();

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.listingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.LISTINGS);

    this.savedListingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.SAVED_LISTINGS)
      .where('userID', '==', this.props.user.id);
    this.categoriesRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.CATEGORIES);
  }

  componentDidMount() {
    this.savedListingsUnsubscribe = this.savedListingsRef.onSnapshot(
      this.onSavedListingsCollectionUpdate,
    );

    this.props.navigation.setParams({
      onPressPost: this.onPressPost,
    });

    this.listingsUnsubscribe = this.listingsRef
      .where('authorID', '==', this.props.user.id)
      .where('isApproved', '==', true)
      .onSnapshot(this.onListingsCollectionUpdate);

    this.categoriesUnsubscribe = this.categoriesRef.onSnapshot(
      this.onCategoriesCollectionUpdate,
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }

    if (this.savedListingsUnsubscribe) {
      this.savedListingsUnsubscribe();
    }

    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onAddCancel = () => {
    this.setState({addModalVisible: false});
  };

  onPressPost = () => {
    const curLen = this.state.listings.filter(
      item => item.authorID === this.props.user.id,
    ).length;

    if (this.props.user.userType === 'renter' && curLen >= 1) {
      Alert.alert(
        'Information',
        'Renter can only post 1 listing. Wanna upgrade to Manager?',
        [
          {
            text: IMLocalized('Yes'),
            onPress: () =>
              this.props.navigation.navigate('AccountDetail', {
                appStyles: DynamicAppStyles,
                form: ListingAppConfig.editProfileFields,
                screenTitle: IMLocalized('Edit Profile'),
              }),
            style: 'destructive',
          },
          {text: IMLocalized('No')},
        ],
        {cancelable: false},
      );
    } else {
      this.setState({
        selectedItem: null,
        addModalVisible: true,
      });
    }
  };

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onSavedListingsCollectionUpdate = querySnapshot => {
    const savedListingdata = [];
    querySnapshot.forEach(doc => {
      const savedListing = doc.data();
      savedListingdata.push(savedListing.listingID);
    });

    this.setState({
      savedListings: savedListingdata,
    });
  };

  onCategoriesCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const category = doc.data();
      data.push({...category, id: doc.id});
    });
    this.setState({
      categories: data,
    });
  };

  onListingsCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const listing = doc.data();
      if (this.state.savedListings.findIndex(k => k === doc.id) >= 0) {
        listing.saved = true;
      } else {
        listing.saved = false;
      }
      data.push({...listing, id: doc.id});
    });

    this.setState({
      listings: data,
      loading: false,
    });
  };

  onPressListingItem = item => {
    this.props.navigation.navigate('MyListingDetailModal', {item});
  };

  onLongPressListingItem = item => {
    if (item.authorID === this.props.user.id) {
      this.setState({selectedItem: item}, () => {
        this.listingItemActionSheet.current.show();
      });
    }
  };

  onLisingItemActionDone = index => {
    if (index === 0) {
      this.setState({
        postModalVisible: true,
      });
    }

    if (index === 1) {
      Alert.alert(
        IMLocalized('Delete listing?'),
        IMLocalized('Are you sure you want to remove this listing?'),
        [
          {
            text: IMLocalized('Yes'),
            onPress: this.removeListing,
            style: 'destructive',
          },
          {text: IMLocalized('No')},
        ],
        {cancelable: false},
      );
    }
  };

  removeListing = () => {
    firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.LISTINGS)
      .doc(this.state.selectedItem.id)
      .delete()
      .then(function() {
        const realEstateSavedQuery = firebase
          .firestore()
          .collection(ServerConfiguration.database.collection.SAVED_LISTINGS)
          .where('listingID', '==', this.state.selectedItem.id);
        realEstateSavedQuery.get().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
            doc.ref.delete();
          });
        });
      })
      .catch(function(error) {
        console.log('Error deleting listing: ', error);
        Alert.alert(
          IMLocalized(
            'Oops! an error while deleting listing. Please try again later.',
          ),
        );
      });
  };

  onPostCancel = () => {
    this.setState({postModalVisible: false});
  };

  onPressSavedIcon = item => {
    if (item.saved) {
      firebase
        .firestore()
        .collection(ServerConfiguration.database.collection.SAVED_LISTINGS)
        .where('listingID', '==', item.id)
        .where('userID', '==', this.props.user.id)
        .get()
        .then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
            doc.ref.delete();
          });
        });
    } else {
      firebase
        .firestore()
        .collection(ServerConfiguration.database.collection.SAVED_LISTINGS)
        .add({
          userID: this.props.user.id,
          listingID: item.id,
        })
        .then(function(docRef) {})
        .catch(function(error) {
          console.error(error);
        });
    }
  };

  renderListingItem = ({item}) => {
    // const categoryName = this.state.categories.filter(
    //   c => c.id === item.categoryID,
    // ).name;

    return (
      <TouchableOpacity
        onPress={() => this.onPressListingItem(item)}
        onLongPress={() => this.onLongPressListingItem(item)}
        style={[
          TwoColumnListStyle.mainConatiner,
          {
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 3,
          },
        ]}>
        <View
          style={[
            TwoColumnListStyle.listingItemContainer1,
            {borderRadius: 20, overflow: 'hidden', alignSelf: 'center'},
          ]}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto1}
            source={{uri: item.photo}}
          />
          <View style={{paddingLeft: 10, paddingBottom: 10}}>
            <Text style={TwoColumnListStyle.dollar}>
              <Text style={{fontSize: 13, color: '#737373'}}>$ </Text>
              {item.price}
              <Text style={{fontSize: 11, color: '#777677'}}> per month.</Text>
            </Text>
            <Text
              style={{
                ...TwoColumnListStyle.listingName,
                maxHeight: 40,
                marginTop: 4,
              }}>
              {item.title}
            </Text>
            <Text style={TwoColumnListStyle.listingPlace1}>
              {`${
                item.address.street_number
                  ? item.address.street_number.long_name
                  : ''
              } ${item.address.street ? item.address.street.long_name : ''}, ${
                item.address.city ? item.address.city.long_name : ''
              }, ${item.address.county ? item.address.county.long_name : ''}, ${
                item.address.state ? item.address.state.short_name : ''
              }, ${
                item.address.country ? item.address.country.short_name : ''
              }`}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <View
                style={[TwoColumnListStyle.blueContainer, {marginRight: 10}]}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  {item.filters.Bedrooms === 'Any' ? 1 : item.filters.Bedrooms}{' '}
                  Bed,{' '}
                  {item.filters.Bathrooms === 'Any'
                    ? 1
                    : item.filters.Bathrooms}{' '}
                  Bath
                </Text>
              </View>
              <View style={TwoColumnListStyle.blueContainer}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  <Icon name="star" size={12} color="#324cf2" /> 4.6
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          data={this.state.listings}
          renderItem={this.renderListingItem}
          keyExtractor={item => `${item.id}`}
          contentContainerStyle={{paddingTop: 20}}
        />
        {this.state.postModalVisible && (
          <PostModal
            selectedItem={this.state.selectedItem}
            categories={this.state.categories}
            onCancel={this.onPostCancel}
          />
        )}
        {this.state.addModalVisible && (
          <PostModal onCancel={this.onAddCancel} />
        )}
        <ActionSheet
          ref={this.listingItemActionSheet}
          title={'Confirm'}
          options={['Edit Listing', 'Remove Listing', 'Cancel']}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={index => {
            this.onLisingItemActionDone(index);
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  rightButton: {
    marginRight: 10,
    color: AppStyles.color.main,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
  composeButton: {marginRight: 13},
});

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.app.location,
  startDate: state.app.startDate,
  endDate: state.app.endDate,
});

export default connect(mapStateToProps)(MyListingModal);
