import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {AppStyles, AppIcon} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';

export default class SavedButton extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {item, style} = this.props;
    return (
      <TouchableOpacity style={style} onPress={this.props.onPress}>
        {/* DynamicAppStyles.colorSet.mainThemeForegroundColor */}
        <Image
          style={{
            ...AppIcon.style,
            tintColor: item.saved ? 'red' : AppStyles.color.white,
          }}
          source={AppIcon.images.heartFilled}
        />
      </TouchableOpacity>
    );
  }
}
