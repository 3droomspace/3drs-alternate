/* eslint-disable no-undef */
import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ActivityIndicator,
  BackHandler,
  SafeAreaView,
  Dimensions,
  Modal,
} from 'react-native';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import ServerConfiguration from '/ServerConfiguration';
import Button from 'react-native-button';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import Moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import {firebaseListing} from '@Firebase';
import {AppStyles, TwoColumnListStyle} from '/AppStyles';
import SavedButton from '@Components/SavedButton';
import {Configuration} from '/Configuration';
import DynamicAppStyles from '/DynamicAppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Pdf from 'react-native-pdf';
import {
  getOrRenewAccessToken,
  uploadDocumentToServer,
} from '../../helpers/docusign';
import RequestDocumentsModal from '@Components/RequestDocumentsModal';
import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';

class ApprovedDocusign extends React.Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      booking: {},
      data: {},
      agreementPDF: '',
      doc_scale: false,
      showRequestDocumentsModal: false,
    };
    this.bookingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.BOOKINGS);
  }

  componentDidMount() {
    const {itemId, data} = this.props.navigation.state.params;
    this.bookingsRef
      .where('id', '==', itemId)
      .onSnapshot(this.onBookingsUpdate);
    this.bookingsRef
      .doc(itemId)
      .get()
      .then(response => {
        this.setState({agreementPDF: response._data.docusignPDF});
      });
  }

  onBookingsUpdate = querySnapshot => {
    const {itemId, data} = this.props.navigation.state.params;
    let booking = {};

    querySnapshot.forEach(doc => {
      booking = doc.data();
    });

    this.setState({booking, data: data});
  };

  onBack = () => {
    this.props.navigation.goBack();
  };

  fullDocument = () => {
    this.setState({doc_scale: true});
  };

  closeScale = () => {
    this.setState({doc_scale: false});
  };

  signClick = () => {
    getOrRenewAccessToken()
      .then(accessToken => {
        if (accessToken) {
          this.uploadIDToServer(accessToken);
        } else {
          this.setState({
            showDocusign: true,
          });
        }
      })
      .catch(err => {
        this.setState({
          loading: false,
        });
        Alert.alert(err.toString());
      });
  };

  requestDocuSign = () => {
    this.setState({
      showRequestDocumentsModal: true,
    });
  };

  closeRequestDocumentsModal = (requestID = false, requestSignature) => {
    this.setState({
      showRequestDocumentsModal: false,
    });

    if (requestID || requestSignature) {
      this.bookingsRef
        .doc(this.state.booking.id)
        .update({
          status: 'DocuSign Requested',
          requestID: requestID,
          docusignRequested: requestSignature,
        })
        .then(() => {});
    }
  };

  submit = () => {
    this.setState({
      loading: true,
    });
    // this.uploadDocumentsToServer();
    this.sentPDFDocusign();
  };

  sentPDFDocusign = () => {
    this.setState({
      loading: true,
    });
    const {renter, owner} = this.props.navigation.state.params;

    fetch(Configuration.GET_PDF_BASE64_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pdfURL: this.state.agreementPDF,
      }),
    })
      .then(res => res.json())
      .then(pdfResult => {
        var myHeaders = new Headers();
        myHeaders.append(
          'X-DocuSign-Authentication',
          '{ "Username": "thomas.work10@gmail.com",  "Password":"Thomas1994",  "IntegratorKey":"f9083809-ec4e-4aee-b6b1-e81adcb6f276" }',
          // `'{"Username": ${owner.email}, "Password":"Superstar@1991",  "IntegratorKey":"4c357852-62e8-4816-929c-08ee87d8fb36"}`
        );
        fetch(Configuration.DOCUSIGN_LOGIN_URL, {
          method: 'GET',
          headers: myHeaders,
        })
          .then(response => response.json())
          .then(result => {
            var baseUrl = result.loginAccounts[0].baseUrl;

            var myHeaders1 = new Headers();
            myHeaders1.append(
              'X-DocuSign-Authentication',
              '{ "Username":"thomas.work10@gmail.com",  "Password":"Thomas1994",  "IntegratorKey":"f9083809-ec4e-4aee-b6b1-e81adcb6f276" }',
            );
            myHeaders1.append('Accept', 'application/json');
            myHeaders1.append('Content-Type', 'application/json');

            var raw = JSON.stringify({
              documents: [
                {
                  documentBase64: pdfResult.data,
                  documentId: '1',
                  fileExtension: 'pdf',
                  name: 'NDA.pdf',
                },
              ],
              emailSubject: 'Please sign the NDA',
              recipients: {
                signers: [
                  {
                    email: renter.email,
                    name: renter.firstName + ' ' + renter.lastName,
                    recipientId: '1',
                    routingOrder: '1',
                    tabs: {
                      dateSignedTabs: [
                        {
                          anchorString: 'signer1date',
                          anchorYOffset: '-6',
                          fontSize: 'Size12',
                          name: 'Date Signed',
                          recipientId: '1',
                          tabLabel: 'date_signed',
                        },
                      ],
                      fullNameTabs: [
                        {
                          anchorString: 'signer1name',
                          anchorYOffset: '-6',
                          fontSize: 'Size12',
                          name: 'Full Name',
                          recipientId: '1',
                          tabLabel: 'Full Name',
                        },
                      ],
                      signHereTabs: [
                        {
                          anchorString: 'signer1sig',
                          anchorUnits: 'mms',
                          anchorXOffset: '0',
                          anchorYOffset: '0',
                          name: 'Please sign here',
                          optional: 'false',
                          recipientId: '1',
                          scaleValue: 1,
                          tabLabel: 'signer1sig',
                        },
                      ],
                    },
                  },
                ],
              },
              status: 'sent',
            });
            fetch(`${baseUrl}/envelopes`, {
              method: 'POST',
              headers: myHeaders1,
              body: raw,
            })
              .then(response => response.text())
              .then(result => {
                alert('Docusign email was sent!');
                this.setState({
                  loading: false,
                });
              });
          });
      });
  };

  uploadDocumentsToServer = () => {
    const {
      booking,
      data,
      agreementPDF,
      doc_scale,
      showRequestDocumentsModal,
    } = this.state;
    uploadDocumentToServer(
      true,
      this.state.agreementPDF,
      this.props.user.email,
      this.props.user.firstName + ' ' + this.props.user.lastName,
      data,
      booking,
    )
      .then(() => {
        Alert.alert('Docusign Email was sent!');

        this.setState({
          loading: false,
        });
        this.props.onCancel(this.state.requestID, true);
      })
      .catch(err => {
        console.error(err);
        Alert.alert(err.toString());

        this.setState({
          loading: false,
        });
        this.props.onCancel(this.state.requestID, true);
      });
  };

  render() {
    const {
      booking,
      data,
      agreementPDF,
      doc_scale,
      showRequestDocumentsModal,
      loading,
    } = this.state;
    const source = {
      uri: agreementPDF,
      cache: true,
    };
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1, paddingTop: 10, alignItems: 'center' }}>
          <View style={styles.titleView}>
            <TouchableOpacity style={styles.backBtn} onPress={this.onBack}>
              <Feather name="arrow-left" size={30} color="black" />
            </TouchableOpacity>
            <Text style={styles.title}>Agreement</Text>
          </View>
          
          <ScrollView style={{flex: 1}}>
            <View style={styles.mainView}>
              <Text style={styles.mentionTxt}>
                {/* {data.address ? data.address.full_address : ''} */}
                We provide a rental agreement based on{'\n'}New York City
                standards for full renter{'\n'}
                protections, optimized for renters and{'\n'}medium term rentals.
              </Text>
              <Text style={styles.mentionTxt}>
                {/* {Moment(booking.startDate).format('MMM.Do.YYYY')} -{' '}
                {Moment(booking.endDate).format('MMM.Do.YYYY')} */}
                Take a moment to get acquainted with our{'\n'}agreement:
              </Text>
              {agreementPDF ? (
                <TouchableOpacity onPress={this.fullDocument}>
                  <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                      console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page, numberOfPages) => {
                      console.log(`current page: ${page}`);
                    }}
                    onError={error => {
                      console.log(error);
                    }}
                    onPressLink={uri => {
                      console.log(`Link presse: ${uri}`);
                    }}
                    style={styles.pdf}
                    singlePage={true}
                    onPageSingleTap={() => alert('1')}
                  />
                </TouchableOpacity>
              ) : null}
              {/* <TouchableOpacity style={styles.signBtn} onPress={this.submit}>
                <Text style={styles.signTxt}>
                  I HAVE READ & UNDERSTAND THIS{'\n'}AGREEMENT
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                // onPress={this.signClick}
                onPress={this.requestDocuSign}>
                <Text style={styles.anotheragreement}>
                  I Want to Upload Another Agreement
                </Text>
              </TouchableOpacity> */}
            </View>
          </ScrollView>
        </View>
        
        <Modal
          animationType="slide"
          transparent={true}
          visible={doc_scale}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <Pdf
            source={source}
            style={{
              flex: 1,
            }}
            singlePage={false}
            onPageSingleTap={() => alert('1')}
          />
          <TouchableOpacity style={styles.closeBtn} onPress={this.closeScale}>
            <MaterialIcons name="close" size={30} color="black" />
          </TouchableOpacity>
        </Modal>
        {showRequestDocumentsModal && (
          <RequestDocumentsModal
            onCancel={this.closeRequestDocumentsModal}
            data={data}
            renter={this.props.user}
            booking={booking}
          />
        )}
        {loading && <TNActivityIndicator />}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  titleView: {
    width: '100%',
    alignItems: 'center',
  },
  backBtn: {
    marginRight: 10,
    position: 'absolute',
    left: 10,
    top: 5,
  },
  title: {
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    color: AppStyles.color.title,
    fontSize: 35,
    textAlign: 'center',
  },
  closeBtn: {
    marginRight: 10,
    position: 'absolute',
    left: 10,
    top: 60,
  },
  mainView: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    marginTop: 20,
  },
  addressTxt: {
    fontFamily: AppStyles.fontName.bold,
    color: AppStyles.color.title,
    fontSize: 15,
    textAlign: 'center',
    marginTop: 15,
  },
  mentionTxt: {
    fontFamily: AppStyles.fontName.bold,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
  pdf: {
    width: Dimensions.get('window').width - 40,
    height: Dimensions.get('window').width ,
  },
  signBtn: {
    width: Dimensions.get('window').width - 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#44D7B6',
    shadowColor: '#24A148',
    shadowRadius: 5,
    shadowOffset: {height: 5},
    shadowOpacity: 0.4,
    borderRadius: 6,
    marginTop: 30,
  },
  signTxt: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  anotheragreement: {
    fontSize: 18,
    color: '#24A148',
    textAlign: 'center',
    marginTop: 25,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(ApprovedDocusign);
