import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import commonStyles from './styles';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {apiListPaymentMethods} from '../../services/3drsAPI';
import {getCustomerData} from '../../firebase/stripe_customers';
import {showAlert} from './utils';

export default function CardListScreen(props) {
  const user = props.navigation.getParam('user');
  const prevRoute = props.navigation.getParam('prevScreen');
  const [loading, setLoading] = useState(false);
  const [pmList, setPmList] = useState([]);

  useEffect(() => {
    async function fetchData() {
      setLoading(true);

      try {
        // Retrieve stripe customer data;
        const sUserRes = await getCustomerData(user.id);
        const sCustomerId = sUserRes.data.customer_id;

        // Get the list of payment methods
        const pMListReqObj = {
          customerId: sCustomerId,
        };
        const pMListRes = await apiListPaymentMethods(pMListReqObj);
        if (!pMListRes.data.data) {
          showAlert(
            '',
            "It doesn't work with the older users\nPlease signup again.",
          );
          // props.navigation.goBack();
          props.navigation(prevRoute);
        } else {
          console.log('fetch data ===>>>', pMListRes.data.data);
          setPmList(pMListRes.data.data);
        }
      } catch (error) {
        console.log('fetch error ===>>>', error);
      }
      setLoading(false);
    }
    fetchData();
  }, [prevRoute, props, props.navigation, user.id]);

  const onMakePayment = selIdex => {
    props.navigation.navigate('PaymentScreen', {
      user,
      paymentMethod: pmList[selIdex],
    });
  };

  return (
    <View style={commonStyles.container}>
      <LinearGradient
        style={commonStyles.header}
        colors={['#009aca', '#069dc9', '#aeefaa']}
        locations={[0.1, 0.2, 1]}
      />

      {loading ? (
        <View style={styles.indicator}>
          <ActivityIndicator animating size="large" color={'black'} />
        </View>
      ) : (
        <View style={commonStyles.body}>
          <View style={[commonStyles.row, {justifyContent: 'space-between'}]}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('MyProfile', {user})}>
              <MaterialIcon name={'arrow-back'} size={25} />
            </TouchableOpacity>

            <Text style={commonStyles.title1}>Cards List</Text>
            <View style={commonStyles.placeHolder} />
          </View>

          {pmList.map((item, index) => (
            <Card
              name={item.billing_details.name || ''}
              lastNumber={item.card.last4}
              key={'card key' + index}
              onPress={() => onMakePayment(index)}
            />
          ))}

          <TouchableOpacity
            style={commonStyles.btn}
            onPress={() => {
              props.navigation.navigate('AddCardScreen', {
                user,
              });
            }}>
            <Text style={commonStyles.btnText}>Add new card</Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
}

function Card({name, lastNumber, onPress}) {
  return (
    <TouchableOpacity style={commonStyles.inputCard} onPress={onPress}>
      <View style={styles.cardIcon}>
        <FontAwesomeIcon name={'credit-card-alt'} size={25} />
      </View>

      <View>
        {/* <Text>{name}</Text> */}
        <Text>{`XXXX ${lastNumber}`}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardIcon: {
    width: 50,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
