import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {BackHandler, Alert} from 'react-native';
import {connect} from 'react-redux';
import TextButton from 'react-native-button';
import {firebaseUser} from '@Firebase';
import {setUserData} from '@Containers/onboarding/redux';
import PayComponent from '@Components/PayComponent';

class PayScreen extends Component {
  static navigationOptions = ({screenProps, navigation}) => {
    let appStyles = navigation.state.params.appStyles;
    let screenTitle = navigation.state.params.screenTitle;
    let currentTheme = appStyles.navThemeConstants[screenProps.theme];
    const {params = {}} = navigation.state;

    return {
      headerTitle: screenTitle,

      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.appStyles = props.navigation.getParam('appStyles') || props.appStyles;
    this.form = props.navigation.getParam('form') || props.form;
    this.onComplete =
      props.navigation.getParam('onComplete') || props.onComplete;

    this.state = {
      form: props.form,
      alteredFormDict: {},
    };
    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onFormSubmit: this.onFormSubmit,
    });
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onFormSubmit = () => {};

  onFormChange = alteredFormDict => {
    this.setState({alteredFormDict});
  };

  testUser = {
    unit: '1202 manhannttan',
    payment_due_amount: '1200',
    payment_due_date: '9/9/2021',
  };

  render() {
    return <PayComponent user={this.testUser} />;
  }
}

PayScreen.propTypes = {
  user: PropTypes.object,
  setUserData: PropTypes.func,
};

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, {setUserData})(PayScreen);
