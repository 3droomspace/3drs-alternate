import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import commonStyles from './styles';
import stripe from 'tipsi-stripe';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {apiCreateCard} from '../../services/3drsAPI';
import {getCustomerData} from '../../firebase/stripe_customers';
import {showAlert} from './utils';

export default function PaymentScreen(props) {
  const user = props.navigation.getParam('user');
  const [loading, setLoading] = useState(false);
  // const [cardNumber, setCardNumber] = useState('4000 0025 0000 3155');
  const [cardNumber, setCardNumber] = useState('4242 4242 4242 4242');
  const [cardOwner, setCardOwner] = useState('Harry');
  const [expDate, setExpDate] = useState('12/21');
  const [cvc, setCvc] = useState('123');

  const onChangeNumber = text => {
    let formattedText = text.split(' ').join('');
    if (formattedText.length > 0) {
      formattedText = formattedText.match(new RegExp('.{1,4}', 'g')).join(' ');
    }
    setCardNumber(formattedText);
    return formattedText;
  };

  const onChangeDate = text => {
    let formatted = text;
    if (formatted.length === 2) {
      if (expDate.indexOf('/') === -1) {
        formatted = text + '/';
      }
    }
    setExpDate(formatted);
  };

  const onLinkCard = async () => {
    if (
      cardNumber.length === 19 &&
      expDate.length === 5 &&
      cvc.length === 3 &&
      cardOwner
    ) {
      setLoading(true);
      try {
        const sCardNumber = cardNumber.split(' ').join('');
        const sExpMonth = Number(expDate.substring(0, 2));
        const sExpYear = Number(expDate.substring(3, 5));

        // Retrieve stripe customer data;
        const stripeCustomerData = await getCustomerData(user.id);
        const stripeCustomerId = stripeCustomerData.data.customer_id;
        console.log('stripeCustomerData ===>>>', stripeCustomerId);

        // Save a card
        const params = {
          object: 'card',
          // mandatory
          number: sCardNumber,
          expMonth: sExpMonth,
          expYear: sExpYear,
          cvc: cvc,
          // optional
          name: cardOwner,
          currency: 'usd',
          addressLine1: '',
          addressLine2: '',
          addressCity: '',
          addressState: '',
          addressCountry: '',
          addressZip: '',
        };
        const tokenForCard = await stripe.createTokenWithCard(params);
        console.log('tokenForCard ===>>>', tokenForCard.tokenId);
        const cardReqObj = {
          customerId: stripeCustomerId,
          token: tokenForCard.tokenId,
        };
        const newCard = await apiCreateCard(cardReqObj);
        console.log('newCard ===>>>', newCard.data);

        setLoading(false);
        props.navigation.replace('CardListScreen', {
          user,
        });
      } catch (error) {
        showAlert('', error.toString());
        setLoading(false);
      }
    } else {
      showAlert('', 'Please type the correct info');
    }
  };

  return (
    <View style={commonStyles.container}>
      <LinearGradient
        style={commonStyles.header}
        colors={['#009aca', '#069dc9', '#aeefaa']}
        locations={[0.1, 0.2, 1]}
      />

      <View style={commonStyles.body}>
        <View style={[commonStyles.row, {justifyContent: 'space-between'}]}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <MaterialIcon name={'arrow-back'} size={25} />
          </TouchableOpacity>

          <Text style={commonStyles.title1}>Please enter card detail</Text>
          <View style={commonStyles.placeHolder} />
        </View>

        <Card
          title={'CARD NUMBER'}
          placeHolder={'1234 1234 1234 1234'}
          value={cardNumber}
          onChangeText={onChangeNumber}
          maxLength={19}
          keyboardType={'numeric'}
        />

        <Card
          title={'CARD OWNER'}
          placeHolder={''}
          value={cardOwner}
          onChangeText={setCardOwner}
        />

        <Card
          title={'EXPIRE DATE'}
          placeHolder={'00/00'}
          value={expDate}
          onChangeText={onChangeDate}
          maxLength={5}
          keyboardType={'numeric'}
        />

        <Card
          title={'CVC'}
          placeHolder={'000'}
          value={cvc}
          onChangeText={setCvc}
          maxLength={3}
          keyboardType={'numeric'}
        />

        <TouchableOpacity
          style={commonStyles.btn}
          onPress={!loading ? onLinkCard : undefined}>
          {loading ? (
            <ActivityIndicator animating size="small" color={'white'} />
          ) : (
            <Text style={commonStyles.btnText}>ADD CARD</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

function Card({
  title,
  placeHolder,
  onChangeText,
  value,
  maxLength,
  keyboardType,
}) {
  return (
    <View style={commonStyles.inputCard}>
      <Text style={commonStyles.inputCardTitle}>{title}</Text>
      <TextInput
        placeholder={placeHolder}
        placeholderTextColor="#3a3a3a"
        style={commonStyles.inputCardText}
        value={value}
        onChangeText={onChangeText}
        maxLength={maxLength}
        keyboardType={keyboardType}
      />
    </View>
  );
}
