import React, {Component} from 'react';
import {View, Text, TextInput, StatusBar} from 'react-native';
import {connect} from 'react-redux';

import {IMConversationListView} from '@Containers/chat';
import DynamicAppStyles from '/DynamicAppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';

import MessageSearchBar from '@Components/MessageSearchBar';

class ConversationsScreen extends Component {
  static navigationOptions = ({}) => {
    return {
      header: null,
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({
      openDrawer: this.openDrawer,
    });
  }

  onEmptyStatePress() {
    this.props.navigation.navigate('Categories');
  }

  render() {
    const emptyStateConfig = {
      title: IMLocalized('No Messages'),
      description: IMLocalized(
        'You can contact vendors by messaging them on the listings page. Your conversations with them will show up here.',
      ),
      buttonName: IMLocalized('Browse Listings'),
      onPress: () => {
        this.onEmptyStatePress();
      },
    };

    return (
      <View style={{flex: 1}}>
        <IMConversationListView
          navigation={this.props.navigation}
          appStyles={DynamicAppStyles}
          emptyStateConfig={emptyStateConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps)(ConversationsScreen);
