import React from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  BackHandler,
  Text,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {connect} from 'react-redux';
import styled from 'styled-components/native';

import {HeaderButtonStyle} from '/AppStyles';
import HeaderButton from '@Components/HeaderButton';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';
import ListingAppConfig from '/ListingAppConfig';
import SearchListingResults from '@Components/SearchListingResults';
import LocationSelect from '@Components/LocationSelect';
import PostModal from '@Components/PostModal';
import OrderSuscription from '@Components/OrderSuscription';
import {firebaseListing} from '@Firebase';
import DateRangeSelect from '@Components/DateRangeSelect';
import Icon from 'react-native-vector-icons/Feather';

class HomeScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title: IMLocalized('Listings'),
    headerTitleStyle: {
      flex: 1,
      fontFamily: 'CircularStd-Book',
      fontSize: 30,
      color: '#383838',
      fontWeight: '100',
      textAlign: 'center',
    },

    headerLeft: (
      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.goBack()}>
        <Icon
          name={'chevron-left'}
          size={42}
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Text style={styles.backButtonLabel}>{'Back'}</Text>
      </TouchableOpacity>
    ),
    headerStyle: {marginTop: 11, borderBottomWidth: 0},
    headerTintColor: 'rgba(0, 0, 0, .9)',
    headerRight: (
      <View style={HeaderButtonStyle.multi}>
        {/* <HeaderButton
          customStyle={styles.composeButton}
          icon={DynamicAppStyles.iconSet.compose}
          iconStyle={{ size: 150 }}
          onPress={() => {
            navigation.state.params.onPressPost();
            // console.log(navigation.state.params);
          }}
        /> */}
        <HeaderButton
          customStyle={styles.mapButton}
          icon={DynamicAppStyles.iconSet.map}
          onPress={() => {
            navigation.navigate('Map', {});
          }}
        />
      </View>
    ),
  });

  constructor(props) {
    super(props);

    this.listingItemActionSheet = React.createRef();

    this.state = {
      categories: [],
      listings: [],
      selectedItem: null,
      hasListing: false,
      primeModalVisible: false,
      postModalVisible: false,
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.onPrimeModal = this.onPrimeModal.bind(this);
  }

  loadItems = () => {
    this.listingsUnsubscribe = firebaseListing.subscribeListings(
      {orderBy: [['createdAt', 'desc']]},
      this.onListingsCollectionUpdate,
    );
  };

  onListingsCollectionUpdate = querySnapshot => {
    const data = [];

    querySnapshot.forEach(doc => {
      const listing = doc.data();
      data.push({...listing, id: doc.id});
    });

    this.setState({
      listings: data,
    });
  };

  componentWillUnmount() {
    this.listingsUnsubscribe();
  }

  componentDidMount() {
    this.categoriesUnsubscribe = firebaseListing.subscribeListingCategories(
      this.onCategoriesCollectionUpdate,
    );

    this.loadItems();

    this.props.navigation.setParams({
      onPressPost: this.onPressPost,
      menuIcon: this.props.user.profilePictureURL,
      onModal: this.onModal,
    });

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  onCategoriesCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const category = doc.data();
      data.push({...category, id: doc.id});
    });
    this.setState({
      categories: data,
      loading: false,
    });
  };

  onModal = (modalVisible, callback) => {
    this.setState({[modalVisible]: !this.state[modalVisible]}, () => {
      callback;
    });
  };

  onPressPost = () => {
    const curLen = this.state.listings.filter(
      item => item.authorID === this.props.user.id,
    ).length;
    if (this.props.user.userType === 'renter' && curLen >= 1) {
      Alert.alert(
        'Information',
        'Renter can only post 1 listing. Wanna upgrade to Manager?',
        [
          {
            text: IMLocalized('Yes'),
            onPress: () =>
              this.props.navigation.navigate('AccountDetail', {
                appStyles: DynamicAppStyles,
                form: ListingAppConfig.editProfileFields,
                screenTitle: IMLocalized('Edit Profile'),
              }),
            style: 'destructive',
          },
          {text: IMLocalized('No')},
        ],
        {cancelable: false},
      );
    } else {
      this.setState({
        selectedItem: null,
        postModalVisible: true,
      });
    }
  };
  // onPressPost = () => {
  //   this.props.navigation.navigate('MyProfile')
  // };

  // onPostCancel = () => {
  //   this.setState({ postModalVisible: false });
  // };

  onPrimeModal = () => {
    this.setState({primeModalVisible: true});
  };

  onPrimeCancel = () => {
    this.props.navigation.navigate('MyProfile');
    this.setState({primeModalVisible: false});
  };

  onPrimeModal = () => {
    this.setState({primeModalVisible: true});
  };

  onPrimeCancel = () => {
    this.props.navigation.navigate('MyProfile');
    this.setState({primeModalVisible: false});
  };

  showModal = () => {
    this.setState({
      postModalVisible: true,
    });
  };

  openItem = item => {
    this.props.navigation.navigate('Detail', {
      item,
      customLeft: true,
      routeName: 'Home',
    });
  };

  selectItem = item => {
    this.setState({
      selectedItem: item,
    });
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.state.postModalVisible && (
          <PostModal onCancel={this.onPostCancel} />
        )}
        <SearchListingResults openItem={this.openItem} />
        {this.state.primeModalVisible && (
          <OrderSuscription onPrimeCancel={this.onPrimeCancel} />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    paddingTop: 30,
  },
  userPhoto: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginLeft: 10,
    marginRight: 10,
  },
  mapButton: {
    marginRight: 13,
    marginLeft: 7,
  },
  composeButton: {marginRight: 13},
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonLabel: {
    fontSize: 20,
    marginLeft: -5,
  },
});

export const Input = styled.TextInput`
  font-family: CircularStd-Book;
  background: #e5e4ea;
  border-radius: 8px;
  height: 50px;
  padding-left: 45px;
  font-size: 17px;
  margin-left: 20px;
  margin-bottom: 10px;
`;

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.app.location,
  startDate: state.app.startDate,
  endDate: state.app.endDate,
});

export default connect(mapStateToProps)(HomeScreen);
