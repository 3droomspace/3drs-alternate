import React, {useEffect, useState, useCallback} from 'react';
import {View, AsyncStorage} from 'react-native';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {setUserData} from '../redux';

import authManager from '@Containers/onboarding/utils/authManager';
import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';

const LoadScreen = props => {
  const {navigation} = props;
  const [isLoading, setIsLoading] = useState(true);

  const appStyles =
    navigation.state.params.appStyles || props.navigation.getParam('appStyles');
  const appConfig =
    navigation.state.params.appConfig || props.navigation.getParam('appConfig');

  useEffect(() => {
    tryToLoginFirst();
  }, [tryToLoginFirst]);

  const tryToLoginFirst = useCallback(async () => {
    setIsLoading(true);
    authManager
      .retrievePersistedAuthUser()
      .then(response => {
        setIsLoading(false);

        if (response) {
          const user = response.res;
          props.setUserData(user);
          props.navigation.navigate('MainStack', {user: user});
        } else {
          props.navigation.navigate('LoginStack', {appStyles, appConfig});
        }
      })
      .catch(err => {
        console.log(err);
        props.navigation.navigate('LoginStack', {appStyles, appConfig});
      });
  }, [appConfig, appStyles, props]);

  if (isLoading === true) {
    return <TNActivityIndicator appStyles={appStyles} />;
  }

  return <View />;
};

LoadScreen.propTypes = {
  user: PropTypes.object,
  navigation: PropTypes.object,
};

LoadScreen.navigationOptions = {
  header: null,
};

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, {
  setUserData,
})(LoadScreen);
