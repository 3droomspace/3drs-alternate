import React, {useEffect, useState} from 'react';
import {
  View,
  StatusBar,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';

import AppIntroSlider from 'react-native-app-intro-slider';
import styled from 'styled-components/native';

import {connect} from 'react-redux';
import {setUserType} from '@Redux/app';

import ResponsiveImage from '@Components/Utils/ResponsiveImage';
import GradientButton from '@Components/Buttons/GradientButton';
import TermsOfUseView from '../components/TermsOfUseView';

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
    display: 'flex',
  },
  titleHighlight: {
    color: '#33c090',
  },
  image: {
    width: '100%',
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    textAlign: 'center',
  },
  title: {
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
  },
  paginationContainer: {
    position: 'absolute',
    bottom: 0,
    left: 16,
    right: 16,
  },
  paginationDots: {
    height: 16,
    margin: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginHorizontal: 24,
    justifyContent: 'space-between',
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 16,
  },
  switcherText: {
    color: '#1cb278',
    fontSize: 15,
  },
  button: {
    flex: 1,
    paddingVertical: 20,
    marginHorizontal: 8,
    borderRadius: 24,
    backgroundColor: '#1cb278',
  },
  buttonText: {
    color: 'white',
    fontWeight: '600',
    textAlign: 'center',
  },
  tos: {
    marginTop: 10,
    marginBottom: -20,
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
  },
});

const renterData = [
  {
    title: (
      <Text>
        How you and your grandkids will
        <Text style={styles.titleHighlight}> find </Text>a place to live
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide1.png'),
    // text: ( ),
  },
  {
    title: (
      <Text>
        Finding a roommate should be this
        <Text style={styles.titleHighlight}> easy </Text>.
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide2.png'),
    // text: ( ),
  },
  {
    title: (
      <Text>
        A<Text style={styles.titleHighlight}> new way </Text>
        to find a place to live.
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide3.png'),
    // text: ( ),
  },
];

const managerData = [
  {
    title: (
      <Text>
        Easy
        <Text style={styles.titleHighlight}> Listing Process </Text>
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide4.png'),
    text:
      'Access the latest technology to help you list your units more efficiently.',
  },
  {
    title: (
      <Text>
        Verified
        <Text style={styles.titleHighlight}> Renter Marketplace. </Text>
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide5.png'),
    text:
      'Your listings are automatically added to a rental marketplace of users with verified rental history.',
  },
  {
    title: (
      <Text>
        Optimized
        <Text style={styles.titleHighlight}> Management Tools </Text>
      </Text>
    ),
    image: require('../../../../assets/Images/Intro/slide6.png'),
    text:
      'Increase your revenue with tools to help streamline property management and maintenance.',
  },
];

let slider;
const WalkthroughScreen = props => {
  const {navigation} = props;
  const appConfig =
    navigation.state.params.appConfig || navigation.getParam('appConfig');
  const appStyles =
    navigation.state.params.appStyles || navigation.getParam('appStyles');
  const [isRenter, setIsRenter] = useState(true);

  // var currentSlide = 0;
  let [currentSlide, setCurrentSlide] = useState(0);
  useEffect(() => {
    StatusBar.setHidden(true);
    let timer = setInterval(() => {
      slider.goToSlide((currentSlide + 1) % 3);
      setCurrentSlide(currentSlide + 1);
    }, 4000);
    return () => {
      clearInterval(timer);
    };
  }, [currentSlide]);

  const onLogin = () => {
    navigation.navigate('Login', {appStyles, appConfig});
  };

  const onSignup = () => {
    navigation.navigate('Signup', {appStyles, appConfig});
  };

  const _onSlideChange = (index, lastIndex) => {
    setCurrentSlide(index);
  };

  const _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <View
          style={{
            height: appStyles.WINDOW_HEIGHT - 170,
            display: 'flex',
            overflow: 'hidden',
          }}>
          <TitleTextBlack>{item.title}</TitleTextBlack>
          <View
            style={{
              display: 'flex',
              flex: 1,
              overflow: 'hidden',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={item.image}
              style={{
                width: '100%',
                resizeMode: 'contain',
                height: '100%',
              }}
              resizeMode={'contain'}
            />
          </View>
          {item.text && (
            <Text
              style={{
                paddingLeft: 40,
                paddingRight: 40,
                color: '#9b9b9b',
                paddingTop: 10,
                fontSize: 15,
              }}>
              {item.text}
            </Text>
          )}
        </View>
      </View>
    );
  };

  const _keyExtractor = item => item.title;

  const _renderPagination = activeIndex => {
    return (
      <View style={styles.paginationContainer}>
        <SafeAreaView>
          <View style={styles.buttonContainer}>
            <GradientButton
              style={{
                width: 160,
              }}
              color="blueGreen"
              onPress={onLogin}>
              LOGIN
            </GradientButton>
            <GradientButton
              style={{
                width: 160,
                paddingHorizontal: 10,
              }}
              color="green"
              onPress={onSignup}>
              SIGN UP
            </GradientButton>
          </View>
          <View style={styles.switchContainer}>
            <TouchableOpacity
              onPress={() => {
                currentSlide = 0;
                props.setUserType(isRenter ? 'manager' : 'renter');
                setIsRenter(!isRenter);
              }}>
              <Text style={styles.switcherText}>
                {isRenter ? 'Are you a Manager?' : 'Are you a Renter?'}
              </Text>
            </TouchableOpacity>
          </View>

          <TermsOfUseView
            tosLink={appConfig.tosLink}
            ppLink={appConfig.ppLink}
            style={styles.tos}
          />

          <View style={styles.paginationDots}>
            {renterData.map((_, i) => (
              <TouchableOpacity
                key={i}
                style={[
                  styles.dot,
                  i === activeIndex
                    ? {
                        backgroundColor: 'rgba(0, 0, 0, .5)',
                      }
                    : {
                        backgroundColor: 'rgba(0, 0, 0, .2)',
                      },
                ]}
                onPress={() => slider?.goToSlide(i, true)}
              />
            ))}
          </View>
        </SafeAreaView>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      <StatusBar translucent backgroundColor="transparent" />
      <AppIntroSlider
        keyExtractor={_keyExtractor}
        renderItem={_renderItem}
        renderPagination={_renderPagination}
        onSlideChange={_onSlideChange}
        data={isRenter ? renterData : managerData}
        ref={ref => (slider = ref)}
      />
    </View>
  );
};

WalkthroughScreen.propTypes = {
  navigation: PropTypes.object,
};

WalkthroughScreen.navigationOptions = {
  header: null,
};

const mapStateToProps = state => ({userType: state.app.userType});

const mapDispatchToProps = {
  setUserType,
};

export default connect(mapStateToProps, mapDispatchToProps)(WalkthroughScreen);

const TitleTextBlack = styled.Text`
  font-family: HelveticaNeue;
  font-size: 34px;
  font-weight: bold;
  color: #000000;
  margin: 20% 30px 0 20px;
`;
