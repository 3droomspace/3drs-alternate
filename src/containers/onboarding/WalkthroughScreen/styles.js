import {DynamicStyleSheet} from 'react-native-dark-mode';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    title: {
      fontSize: 25,
      fontWeight: 'bold',
      textAlign: 'center',
      paddingTop: 35,
      paddingBottom: 35,
      color: '#3BCDC5',
    },
    text: {
      fontSize: 18,
      textAlign: 'center',
      color: 'black',
      paddingLeft: 20,
      paddingRight: 20,
    },
    image: {
      width: 100,
      height: 100,
      marginBottom: 60,
      tintColor: 'white',
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    button: {
      fontSize: 18,
      color: '#33C090',
      marginTop: 10,
    },
  });
};

export default dynamicStyles;
