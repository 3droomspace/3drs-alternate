import * as CONSTANTS from './constants';
import {AsyncStorage} from 'react-native';

const initialState = {
  user: {},
  users: [],
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.UPDATE_USER:
      AsyncStorage.setItem('user', JSON.stringify(action.data.user));
      return {
        ...state,
        user: action.data.user,
      };

    case CONSTANTS.SET_USERS:
      return {
        ...state,
        users: [...action.data],
      };

    case CONSTANTS.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};
