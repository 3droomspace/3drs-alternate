import * as CONSTANTS from './constants';

export const setUsers = data => ({
  type: CONSTANTS.SET_USERS,
  data,
});

export const setUserData = data => ({
  type: CONSTANTS.UPDATE_USER,
  data,
});

export const logout = () => ({
  type: CONSTANTS.LOG_OUT,
});
