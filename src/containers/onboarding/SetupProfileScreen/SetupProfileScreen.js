import React, {useState} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {setUserData} from '../redux';
import GradientButton from '@Components/Buttons/GradientButton';
import {setUserType} from '@Redux/app';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import DynamicAppStyles from '/DynamicAppStyles';

const SetupProfileScreen = props => {
  const [loading, setLoading] = useState(false);
  const user =
    props.navigation.state.params.user || props.navigation.getParam('user');
  const appStyles =
    props.navigation.state.params.appStyles ||
    props.navigation.getParam('appStyles');
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const isRenter = user.user.userType == 'renter' ? 1 : 0;

  var userPhotoURI = user.profilePictureURL
    ? user.profilePictureURL
    : DynamicAppStyles.iconSet.userAvatar;

  const save = () => {
    props.navigation.navigate('MainStack', {user: user});
  };

  const skip = () => {
    props.navigation.navigate('MainStack', {user: user});
  };
  var birthday = '00/00/0000';
  var phoneNumber = user.user.phoneNumber;

  return isRenter ? (
    <View style={styles.container}>
      <KeyboardAwareScrollView>
        <Text style={styles.title}>{IMLocalized('Setup Profile')}</Text>
        <View style={styles.avatargroup}>
          <Image style={styles.avatar} source={userPhotoURI} />
          <TouchableOpacity onPress={() => {}}>
            <Text style={styles.editAvatarStyle}>
              {IMLocalized('Edit Profile')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>{'Name'}</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>
              {user.user.firstName + ' ' + user.user.lastName}
            </Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Gender</Text>
          <View style={styles.rightSideStyle}>
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'<'} </Text>
            </TouchableOpacity>
            <Text style={styles.rightTextStyle}>{'Prefer not to say'}</Text>
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'>'} </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Birthdate</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{birthday}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Email</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Edit'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Phone Number</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{phoneNumber}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Password</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Change'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Interested In</Text>
          <View style={styles.rightSideStyle}>
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'<'} </Text>
            </TouchableOpacity>
            <Text style={styles.rightTextStyle}>{'Private'}</Text>
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'>'} </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}> {'Government ID'} </Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Upload'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Emergency Contact</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Update'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>

        <GradientButton
          style={styles.savebutton}
          color="blueGreen"
          onPress={() => save()}>
          {IMLocalized('SAVE')}
        </GradientButton>

        <TouchableOpacity onPress={() => skip()}>
          <Text style={styles.skipButton}>{IMLocalized('Skip')}</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>

      {/* {loading && <TNActivityIndicator appStyles={appStyles} />} */}
    </View>
  ) : (
    <View style={styles.container}>
      <KeyboardAwareScrollView>
        <Text style={styles.title}>{IMLocalized('Setup Profile')}</Text>
        <View style={styles.avatargroup}>
          <Image style={styles.avatar} source={userPhotoURI} />
          <TouchableOpacity onPress={() => {}}>
            <Text style={styles.editAvatarStyle}>
              {IMLocalized('Edit Profile')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>{'Company Name'}</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{''}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>{'Manager First Name'}</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{user.user.firstName}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>{'Manager Last Name'}</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{user.user.lastName}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Email</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Edit'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Phone Number</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <Text style={styles.rightTextStyle}>{phoneNumber}</Text>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Password</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Change'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>{'EIN'}</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Upload'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>
        <View style={styles.seperateStyle} />

        <View style={styles.lineStyle}>
          <Text style={styles.leftSideStyle}>Emergency Contact</Text>
          <View style={styles.rightSideStyle}>
            <Text />
            <TouchableOpacity onPress={() => {}}>
              <Text style={styles.clickable}> {'Update'} </Text>
            </TouchableOpacity>
            <Text />
          </View>
        </View>

        <GradientButton
          style={styles.savebutton}
          color="blueGreen"
          onPress={() => save()}>
          {IMLocalized('SAVE')}
        </GradientButton>

        <TouchableOpacity onPress={() => skip()}>
          <Text style={styles.skipButton}>{IMLocalized('Skip')}</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>

      {/* {loading && <TNActivityIndicator appStyles={appStyles} />} */}
    </View>
  );
};

export default connect(
  state => ({
    userType: state.app.userType,
  }),
  {
    setUserData,
    setUserType,
  },
)(SetupProfileScreen);
