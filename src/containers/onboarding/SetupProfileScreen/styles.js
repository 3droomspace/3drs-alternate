import {I18nManager} from 'react-native';
import {DynamicStyleSheet} from 'react-native-dark-mode';
import DynamicAppStyles from '/DynamicAppStyles';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      // alignItems: 'center',
      backgroundColor: DynamicAppStyles.colorSet.mainThemeBackgroundColor,
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#000',
      marginTop: 25,
      alignSelf: 'stretch',
      textAlign: 'center',
    },
    savebutton: {
      width: '80%',
      paddingHorizontal: 10,
      alignSelf: 'center',
      marginTop: 20,
      marginBottom: 10,
      color: '#3bcdc5',
    },
    avatargroup: {
      flexDirection: 'row',
      width: '80%',
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'space-between',
    },
    avatar: {
      width: DynamicAppStyles.WINDOW_WIDTH * 0.15,
      height: DynamicAppStyles.WINDOW_WIDTH * 0.15,
      resizeMode: 'contain',
      margin: 0,
      padding: 0,
      borderRadius: 50,
    },
    editAvatarStyle: {
      fontSize: 17,
      fontWeight: 'bold',
      color: '#50e3c2',
      alignSelf: 'center',
      textAlign: 'center',
      marginRight: DynamicAppStyles.WINDOW_WIDTH * 0.3,
    },
    skipButton: {
      fontSize: 17,
      fontWeight: 'bold',
      color: '#50e3c2',
      alignSelf: 'center',
      textAlign: 'center',
      marginBottom: 20,
    },
    lineStyle: {
      width: '80%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignSelf: 'center',
      alignItems: 'center',
      paddingTop: 20,
      paddingBottom: 10,
    },
    leftSideStyle: {
      paddingLeft: 10,
      fontSize: 17,
    },
    rightSideStyle: {
      width: '50%',
      flexDirection: 'row',
      alignSelf: 'center',
      alignItems: 'center',
      textAlign: 'center',
      justifyContent: 'space-between',
      paddingRight: 10,
    },
    rightTextStyle: {
      fontSize: 15,
      color: '#9b9b9b',
    },
    seperateStyle: {
      borderTopWidth: 1,
      borderColor: '#9b9b9b',
      width: '80%',
      opacity: 0.5,
      alignSelf: 'center',
    },
    clickable: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#50e3c2',
    },
  });
};

export default dynamicStyles;
