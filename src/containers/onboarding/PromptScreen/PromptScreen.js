import React, {useState} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {setUserData} from '../redux';
import GradientButton from '@Components/Buttons/GradientButton';
import {setUserType} from '@Redux/app';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import DynamicAppStyles from '/DynamicAppStyles';

const PromptScreen = props => {
  const [loading, setLoading] = useState(false);
  const user =
    props.navigation.state.params.user || props.navigation.getParam('user');
  const appStyles =
    props.navigation.state.params.appStyles ||
    props.navigation.getParam('appStyles');
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const isRenter = user.user.userType === 'renter' ? 1 : 0;

  const goSetupProfile = () => {
    props.navigation.navigate('SetupProfile', {user, appStyles});
  };

  return (
    <View style={styles.container}>
      <Image
        source={
          isRenter
            ? DynamicAppStyles.iconSet.profileSetup
            : DynamicAppStyles.iconSet.managerProfileSetup
        }
        style={styles.setupImageStyle}
      />
      <Text style={styles.title}>
        {IMLocalized(
          isRenter
            ? "Welcome, let's get your profile setup..."
            : 'Welcome, let’s get your manager’s profile setup...',
        )}
      </Text>
      <GradientButton
        style={styles.goButton}
        color="green"
        onPress={() => goSetupProfile()}>
        {IMLocalized("LET'S GO")}
      </GradientButton>

      {/* {loading && <TNActivityIndicator appStyles={appStyles} />} */}
    </View>
  );
};

export default connect(
  state => ({
    userType: state.app.userType,
  }),
  {
    setUserData,
    setUserType,
  },
)(PromptScreen);
