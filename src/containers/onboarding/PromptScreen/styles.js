import {I18nManager} from 'react-native';
import {DynamicStyleSheet} from 'react-native-dark-mode';
import DynamicAppStyles from '/DynamicAppStyles';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      // flex: 1,
      // alignItems: 'center',
      backgroundColor: DynamicAppStyles.colorSet.mainThemeBackgroundColor,
    },
    title: {
      fontSize: DynamicAppStyles.fontSet.title,
      fontWeight: 'bold',
      color: '#000',
      marginTop: 25,
      marginBottom: 20,
      alignSelf: 'stretch',
      textAlign: 'center',
      marginLeft: 30,
    },
    goButton: {
      width: '80%',
      paddingHorizontal: 10,
      marginTop: 50,
      alignSelf: 'center',
      marginBottom: 20,
    },
    setupImageStyle: {
      alignSelf: 'center',
      width: '80%',
      height: '40%',
      resizeMode: 'contain',
      marginTop: DynamicAppStyles.WINDOW_HEIGHT * 0.15,
    },
  });
};

export default dynamicStyles;
