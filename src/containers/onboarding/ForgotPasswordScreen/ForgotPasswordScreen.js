import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useDarkMode} from 'react-native-dark-mode';
import firebase from '@react-native-firebase/app';
import InputWithIcon from '../../../components/Forms/InputWithIcon/InputWithIcon';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import GradientButton from '@Components/Buttons/GradientButton';
import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';

const BACKGROUND_IMG = require('../../../../assets/Images/Intro/reset-password.png');
function BackButton({onBack, appStyles}) {
  return (
    <TouchableOpacity onPress={onBack}>
      <Image
        style={appStyles.styleSet.backArrowStyle}
        source={appStyles.iconSet.backArrow}
      />
    </TouchableOpacity>
  );
}

const ForgotPasswordScreen = props => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);

  // appStyles && dark mode
  const isDarkMode = useDarkMode();
  const appStyles =
    props.navigation.state.params.appStyles ||
    props.navigation.getParam('appStyles');

  const forgotPassword = async () => {
    // validte email
    if (email === '') {
      Alert.alert('Please type in your email');
      return;
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
      Alert.alert('Please type in correct email');
      return;
    }

    // send reset password link
    try {
      setLoading(true);
      await firebase.auth().sendPasswordResetEmail(email);
      Alert.alert('Please check your email');
      setLoading(false);
    } catch (error) {
      setLoading(false);
      let errMsg = 'Failed';
      if (error.code === 'auth/user-not-found') {
        errMsg = 'There is no user record corresponding to this email';
      }
      Alert.alert(errMsg);
    }
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.body}>
          <Text style={styles.title}>{'Forgot Password?'}</Text>
          <ImageBackground style={styles.imageBg} source={BACKGROUND_IMG}>
            <Text style={styles.description}>
              {
                'No worries, we got you. Just enter your\nemail below and we will send you\npassword reset instructions'
              }
            </Text>
          </ImageBackground>
          <InputWithIcon
            iconName={'email-outline'}
            iconType={'MaterialCommunityIcons'}
            activeColor={appStyles.colorSet.mainThemeForegroundColor}
            blurColor={
              isDarkMode
                ? appStyles.colorSet.grey3.dark
                : appStyles.colorSet.grey3.light
            }
            autoCompleteType={'email'}
            placeholderTextColor="#aaaaaa"
            placeholder={IMLocalized('Email')}
            onChangeText={text => {
              setEmail(text);
            }}
            value={email}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
          />
          <GradientButton
            containerStyle={styles.gradientBtn}
            color="green"
            onPress={forgotPassword}>
            {IMLocalized('RESET PASSWORD')}
          </GradientButton>
        </View>

        <View style={styles.backBtnWrapper}>
          <BackButton
            onBack={() => props.navigation.goBack()}
            appStyles={appStyles}
          />
        </View>
      </KeyboardAwareScrollView>

      {loading && <TNActivityIndicator appStyles={appStyles} />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollContainer: {
    alignItems: 'center',
    height: hp('100%'),
    width: wp('100%'),
  },
  imageBg: {
    width: wp('100%'),
    height: hp('30%'),
    resizeMode: 'contain',
    alignItems: 'center',
    marginTop: 16,
  },
  gradientBtn: {
    width: wp('80%'),
    marginTop: 24,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 18,
    color: '#0d0d0d',
    textAlign: 'center',
  },
  backBtnWrapper: {
    position: 'absolute',
    left: 0,
  },
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
});

export default ForgotPasswordScreen;
