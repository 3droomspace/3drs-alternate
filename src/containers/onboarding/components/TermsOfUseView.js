import React from 'react';
import {Text, Linking, View} from 'react-native';
import {IMLocalized} from '@Containers/localization/IMLocalization';

const TermsOfUseView = props => {
  const {tosLink, ppLink, style} = props;
  return (
    <View style={style}>
      <View style={{display: 'flex', flexDirection: 'row'}}>
        <Text
          style={{color: 'gray', fontSize: 12}}
          onPress={() => Linking.openURL(tosLink)}>
          {IMLocalized('Terms')}
        </Text>
        <Text style={{fontSize: 12, marginHorizontal: 5, color: 'gray'}}>
          &
        </Text>
        <Text
          style={{color: 'gray', fontSize: 12}}
          onPress={() => Linking.openURL(ppLink)}>
          {IMLocalized('Conditions')}
        </Text>
      </View>
    </View>
  );
};

export default TermsOfUseView;
