import React, {useState} from 'react';
import {
  Text,
  View,
  Alert,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import Button from 'react-native-button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDynamicStyleSheet, useDarkMode} from 'react-native-dark-mode';

import dynamicStyles from './styles';
import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';
import TNProfilePictureSelector from '@Components/truly-native/TNProfilePictureSelector/TNProfilePictureSelector';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import InputWithIcon from '../../../components/Forms/InputWithIcon/InputWithIcon';
import {setUserData} from '../redux';
import authManager from '../utils/authManager';
import sendGridManager from '../../../sendGrid';
import {localizedErrorMessage} from '../utils/ErrorCode';
import GradientButton from '@Components/Buttons/GradientButton';

import {setUserType} from '@Redux/app';

const SignupScreen = props => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmpassword, setconfirmpassword] = useState('');
  const [phone, setPhone] = useState('');

  const [profilePictureURL, setProfilePictureURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isRenter, setIsRenter] = useState(true);

  const WIDTH = Dimensions.get('window').width;

  const appConfig =
    props.navigation.state.params.appConfig ||
    props.navigation.getParam('appConfig');
  const appStyles =
    props.navigation.state.params.appStyles ||
    props.navigation.getParam('appStyles');
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const isDarkMode = useDarkMode();

  const onRegister = () => {
    setLoading(true);
    if (password !== confirmpassword) {
      Alert.alert("Password can't confirm!");
      setLoading(false);
      return;
    } else if (password.length < 6) {
      Alert.alert('Password should be more than 6 letters!');
      setLoading(false);
      return;
    }
    const userDetails = {
      firstName,
      lastName,
      email,
      password,
      phone,
      photoURI: profilePictureURL,
      appIdentifier: appConfig.appIdentifier,
      userType: props.userType,
    };
    authManager
      .createAccountWithEmailAndPassword(userDetails, appConfig.appIdentifier)
      .then(response => {
        const user = response;
        if (user.user) {
          sendGridManager.signupConfirmation(user.user.email);
          authManager.sendEmailVerification(user.user);
          props.setUserData(response);
          // props.navigation.navigate('Prompt', {user, appStyles});
          props.navigation.navigate('MainStack', {user: user});
        } else {
          Alert.alert(
            '',
            localizedErrorMessage(response.error),
            [{text: IMLocalized('OK')}],
            {
              cancelable: false,
            },
          );
        }
        setLoading(false);
      });
  };

  const renderSignupWithEmail = () => {
    return (
      <>
        <InputWithIcon
          // iconName={'account-outline'}
          // iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          placeholder={IMLocalized('First Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setFirstName(text)}
          value={firstName}
          underlineColorAndroid="transparent"
        />
        <InputWithIcon
          // iconName={'account-outline'}
          // iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          placeholder={IMLocalized('Last Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setLastName(text)}
          value={lastName}
          underlineColorAndroid="transparent"
        />
        <InputWithIcon
          iconName={'email-outline'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          autoCompleteType={'email'}
          keyboardType={'email-address'}
          placeholder={IMLocalized('E-mail Address')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <InputWithIcon
          iconName={'phone'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          keyboardType={'phone-pad'}
          placeholder={IMLocalized('Phone Number')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setPhone(text)}
          value={phone}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <InputWithIcon
          iconName={'lock-outline'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          autoCompleteType={'password'}
          placeholder={IMLocalized('Password')}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          onChangeText={text => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <InputWithIcon
          iconName={'lock-outline'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          placeholder={IMLocalized('Confirm Password')}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          onChangeText={text => setconfirmpassword(text)}
          value={confirmpassword}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <GradientButton
          style={{
            width: '80%',
            paddingHorizontal: 10,
            marginTop: 50,
            alignSelf: 'center',
            marginBottom: 20,
          }}
          color="green"
          onPress={() => onRegister()}>
          SIGN UP
        </GradientButton>
      </>
    );
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={{flex: 1, width: '100%'}}
        keyboardShouldPersistTaps="always">
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle}
            source={appStyles.iconSet.backArrow}
          />
        </TouchableOpacity>

        <Image
          style={{
            width: WIDTH * 0.8,
            height: WIDTH * 0.8,
            alignSelf: 'center',
            resizeMode: 'contain',
            marginTop: -40,
            marginBottom: -40,
          }}
          source={
            require('../../../../assets/Images/Intro/bg-10.png')
            // isRenter
            //   ? require('../../../../assets/Images/Intro/bg-10.png')
            //   : require('../../../../assets/Images/Intro/bg-11.png')
          }
        />

        <View style={{position: 'absolute', marginTop: 80}}>
          <Text style={{textAlign: 'center', padding: 30}}>
            <Text style={styles.title_2}>{IMLocalized('Simple ')}</Text>
            <Text style={styles.title_1}>
              {isRenter
                ? IMLocalized('Verification')
                : IMLocalized('Verification for Managers.')}
            </Text>
          </Text>
        </View>

        {/* <TNProfilePictureSelector
          setProfilePictureURL={setProfilePictureURL}
          appStyles={appStyles}
        /> */}
        {renderSignupWithEmail()}
        {/* {appConfig.isSMSAuthEnabled && (
          <>
            <Text style={styles.orTextStyle}>{IMLocalized('OR')}</Text>
            <Button
              containerStyle={styles.PhoneNumberContainer}
              onPress={() =>
                props.navigation.navigate('Sms', {
                  isSigningUp: true,
                  appStyles,
                  appConfig,
                })
              }>
              {IMLocalized('Sign up with phone number')}
            </Button>
          </>
        )} */}
        {/* <TermsOfUseView
          tosLink={appConfig.tosLink}
          ppLink={appConfig.ppLink}
          style={styles.tos}
        /> */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 16,
            marginBottom: 25,
          }}>
          <TouchableOpacity
            onPress={() => {
              props.setUserType(isRenter ? 'manager' : 'renter');
              setIsRenter(!isRenter);
            }}>
            <Text style={{color: '#1cb278', fontSize: 15}}>
              {isRenter ? 'Are you in Management?' : 'Not in Management?'}
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
      {loading && <TNActivityIndicator appStyles={appStyles} />}
    </View>
  );
};

export default connect(
  state => ({
    userType: state.app.userType,
  }),
  {
    setUserData,
    setUserType,
  },
)(SignupScreen);
