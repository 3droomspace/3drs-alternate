import {I18nManager} from 'react-native';
import {DynamicStyleSheet} from 'react-native-dark-mode';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    orTextStyle: {
      color: appStyles.colorSet.mainTextColor,
      marginTop: 40,
      marginBottom: 10,
      alignSelf: 'center',
    },
    logoStyle: {
      width: 128,
      height: 140,
      marginTop: 20,
      alignSelf: 'center',
    },
    title: {
      width: '70%',
      fontSize: appStyles.fontSet.title,
      fontWeight: 'bold',
      color: '#000',
      marginTop: 25,
      marginBottom: 20,
      alignSelf: 'center',
      textAlign: 'center',
      // marginLeft: 30,
    },
    loginContainer: {
      width: '70%',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: 5,
      padding: 15,
      marginTop: 30,
      alignSelf: 'center',
    },
    loginText: {
      color: '#ffffff',
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      paddingLeft: 10,
      width: '90%',
      color: appStyles.colorSet.mainTextColor,
      alignItems: 'center',
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    GridContainer: {
      width: '80%',
      alignSelf: 'center',
      alignItems: 'flex-end',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 20,
      borderBottomWidth: 1,
    },
    googleContainer: {
      width: '70%',
      backgroundColor: '#4C81F4',
      borderRadius: 25,
      padding: 10,
      marginTop: 15,
      alignSelf: 'center',
    },
    facebookContainer: {
      width: '70%',
      backgroundColor: '#4267B2',
      borderRadius: 25,
      padding: 10,
      marginTop: 30,
      alignSelf: 'center',
    },
    facebookText: {
      color: '#ffffff',
    },
    phoneNumberContainer: {
      marginTop: 20,
    },
  });
};

export default dynamicStyles;
