import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Alert,
  TouchableOpacity,
  Image,
  StyleSheet,
  AsyncStorage,
} from 'react-native';
import {Icon} from 'native-base';
import Button from 'react-native-button';
import {connect} from 'react-redux';
import {useDynamicStyleSheet, useDarkMode} from 'react-native-dark-mode';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as keychain from 'react-native-keychain';

import TNActivityIndicator from '@Components/truly-native/TNActivityIndicator';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import InputWithIcon from '../../../components/Forms/InputWithIcon/InputWithIcon';
import dynamicStyles from './styles';
import {setUserData} from '../redux';
import authManager from '../utils/authManager';
import {localizedErrorMessage} from '../utils/ErrorCode';
import GradientButton from '@Components/Buttons/GradientButton';
import CheckBox from '@react-native-community/checkbox';
import TouchId from '../../TouchFaceId/FingerPrint';

import {setUserType} from '@Redux/app';

const LoginScreen = props => {
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [biometryType, setBiometryType] = useState(null);
  const [isRenter, setIsRenter] = useState(true);

  const [rememberMe, setRememberMe] = useState(true);

  useEffect(() => {
    keychain.getGenericPassword().then(credentials => {
      if (credentials.username || credentials.password) {
        setEmail(credentials.username);
        setPassword(credentials.password);
      }
    });
  }, []);

  const appStyles =
    props.navigation.state.params.appStyles ||
    props.navigation.getParam('appStyles');
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const local_styles = StyleSheet.create({
    addContainer: {
      // display: 'flex',
      flex: 1,
      flexDirection: 'row',
      width: '80%',
      alignItems: 'center',
      alignSelf: 'center',
      paddingTop: 20,
      justifyContent: 'space-between',
      // marginLeft: "auto",
    },
    rememberText: {
      fontSize: 15,
      paddingLeft: 5,
    },
    forgotPassword: {
      fontSize: 15,
      color: '#1cb278',
      alignItems: 'center',
    },
    switchContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: 16,
    },
    switcherText: {
      color: '#1cb278',
      fontSize: 15,
    },
  });
  const appConfig =
    props.navigation.state.params.appConfig ||
    props.navigation.getParam('appConfig');

  const isDarkMode = useDarkMode();

  const onPressLogin = async () => {
    setLoading(true);
    authManager
      .loginWithEmailAndPassword(email, password)
      .then(async response => {
        if (response.user) {
          setLoading(false);

          if (
            (response.user.user.userType === 'renter' && isRenter) ||
            (response.user.user.userType !== 'renter' && !isRenter)
          ) {
            const user = response.user;
            props.setUserData(user);

            if (rememberMe) {
              await keychain.setGenericPassword(email, password);
            }
            // props.navigation.navigate('Prompt', {user, appStyles});
            props.navigation.navigate('MainStack', {user: user});
          } else {
            authManager.logout(response.user);
            Alert.alert(
              '',
              isRenter ? 'You are not a Renter' : 'You are not a Manager',
            );
          }
        } else {
          setLoading(false);
          Alert.alert(
            '',
            localizedErrorMessage(response.error),
            [{text: IMLocalized('OK')}],
            {
              cancelable: false,
            },
          );
        }
      });
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={{flex: 1, width: '100%'}}
        keyboardShouldPersistTaps="always">
        <TouchableOpacity
          style={{alignSelf: 'flex-start'}}
          onPress={() => props.navigation.goBack()}>
          <Image
            style={appStyles.styleSet.backArrowStyle}
            source={appStyles.iconSet.backArrow}
          />
        </TouchableOpacity>
        <Image source={appStyles.iconSet.logo} style={styles.logoStyle} />

        <Text style={styles.title}>
          {isRenter ? 'Rental Bliss.' : 'Management Bliss.'}
        </Text>
        <InputWithIcon
          iconName={'email-outline'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          autoCompleteType={'email'}
          keyboardType={'email-address'}
          placeholder={IMLocalized('E-mail')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <InputWithIcon
          iconName={'lock-outline'}
          iconType={'MaterialCommunityIcons'}
          activeColor={appStyles.colorSet.mainThemeForegroundColor}
          blurColor={
            isDarkMode
              ? appStyles.colorSet.grey3.dark
              : appStyles.colorSet.grey3.light
          }
          autoCompleteType={'password'}
          placeholderTextColor="#aaaaaa"
          secureTextEntry
          placeholder={IMLocalized('Password')}
          onChangeText={text => setPassword(text)}
          value={password}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
        />
        <View style={local_styles.addContainer}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CheckBox
              value={rememberMe}
              onValueChange={setRememberMe}
              onAnimationDidStop={() => {}}
              lineWidth={2}
              hideBox={false}
              boxType="circle"
              tintColor="#929AAF"
              onCheckColor="white"
              onFillColor="#1cb278"
              onTintColor="white"
              animationDuration={0.5}
              disabled={false}
              onAnimationType="bounce"
              offAnimationType="stroke"
              // style={}
            />
            <Text style={local_styles.rememberText}>Remember me</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('ForgotPassword', {
                appStyles,
                appConfig,
              });
            }}>
            <Text style={local_styles.forgotPassword}>Forgot password</Text>
          </TouchableOpacity>
        </View>
        <GradientButton
          style={{
            width: '80%',
            paddingHorizontal: 10,
            marginTop: 50,
            alignSelf: 'center',
            marginBottom: 20,
          }}
          color="green"
          onPress={() => onPressLogin()}>
          {IMLocalized('LOG IN')}
        </GradientButton>
        <View style={local_styles.switchContainer}>
          <TouchableOpacity
            onPress={() => {
              props.setUserType(isRenter ? 'manager' : 'renter');
              setIsRenter(!isRenter);
            }}>
            <Text style={local_styles.switcherText}>
              {isRenter ? 'Are you a Manager?' : 'Are you a Renter?'}
            </Text>
          </TouchableOpacity>
        </View>

        {/* <TouchId user={email} password={password} /> */}
        {/* <Text style={styles.orTextStyle}> {IMLocalized('OR')}</Text>
        <Button
          containerStyle={styles.facebookContainer}
          style={styles.facebookText}
          onPress={() => onFBButtonPress()}>
          {IMLocalized('Login With Facebook')}
        </Button>
        <Button
          containerStyle={styles.googleContainer}
          style={styles.facebookText}
          onPress={() => googleSignin()}>
          {IMLocalized('Login With Google')}
        </Button> */}

        {loading && <TNActivityIndicator appStyles={appStyles} />}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default connect(
  state => ({
    userType: state.app.userType,
  }),
  {
    setUserData,
    setUserType,
  },
)(LoginScreen);
