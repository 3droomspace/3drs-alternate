import React from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Text,
  View,
  Alert,
  BackHandler,
  ActivityIndicator,
} from 'react-native';

import {connect} from 'react-redux';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

import FastImage from 'react-native-fast-image';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import MapView, {Marker} from 'react-native-maps';
import StarRating from 'react-native-star-rating';
import Moment from 'moment';
import Video from 'react-native-video';

import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import HeaderButton from '@Components/HeaderButton';
import ReviewModal from '@Components/ReviewModal';
import DynamicAppStyles from '/DynamicAppStyles';
import {timeFormat} from '@Helpers/timeFormat';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import BookingModal from '@Components/BookingModal';
import ServerConfiguration from '/ServerConfiguration';
import sendGrid from '../../sendGrid';
import Icon from 'react-native-vector-icons/Feather';

import {AppStyles, AppIcon, HeaderButtonStyle} from '/AppStyles';
import {firebaseUser, firebaseReview, firebaseListing} from '@Firebase';

const defaultAvatar =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

const {width: viewportWidth} = Dimensions.get('window');
const LATITUDEDELTA = 0.0422;
const LONGITUDEDELTA = 0.0221;
const db = firebase.firestore();

class DetailsScreen extends React.Component {
  static navigationOptions = ({screenProps, navigation}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    const options = {
      headerLeft: (
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}>
          <Icon
            name={'chevron-left'}
            size={42}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.backButtonLabel}>{'Back'}</Text>
        </TouchableOpacity>
      ),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
      headerRight: (
        <View style={HeaderButtonStyle.multi}>
          {(navigation.state.params.isAdmin ||
            navigation.state.params.isUser) && (
            <HeaderButton
              customStyle={styles.headerIconContainer}
              iconStyle={[styles.headerIcon, {tintColor: '#e2362d'}]}
              icon={AppIcon.images.delete}
              onPress={() => {
                navigation.state.params.onDelete();
              }}
            />
          )}
          {!navigation.state.params.isUser && navigation.state.params.author && (
            <HeaderButton
              customStyle={styles.headerIconContainer}
              iconStyle={styles.headerIcon}
              icon={AppIcon.images.communication}
              onPress={() => {
                navigation.state.params.onPersonalMessage();
              }}
            />
          )}
          {navigation.state.params.isAlreadyBooked && (
            <HeaderButton
              customStyle={styles.headerIconContainer}
              iconStyle={styles.headerIcon}
              icon={AppIcon.images.review}
              onPress={() => {
                navigation.state.params.onPressReview();
              }}
            />
          )}
          <HeaderButton
            customStyle={styles.headerIconContainer}
            icon={
              navigation.state.params.saved
                ? AppIcon.images.heartFilled
                : AppIcon.images.heart
            }
            onPress={() => {
              navigation.state.params.onPressSave();
            }}
            iconStyle={[styles.headerIcon, {tintColor: 'red'}]}
          />
        </View>
      ),
    };
    return options;
  };
  videoPlayer;

  constructor(props) {
    super(props);

    const {navigation} = props;
    this.item = navigation.getParam('item');
    this.unsubscribe = null;
    this.reviewsUnsubscribe = null;
    this.savedListingUnsubscribe = null;

    this.state = {
      activeSlide: 0,
      data: this.item ? this.item : {},
      photo: this.item.photo,
      reviews: [],
      saved: false,
      user: {},
      reviewModalVisible: false,
      bookingModalVisible: false,
      isProfileModalVisible: false,
      didFinishAnimation: false,
      bookings: [],
      acceptBookings: [],
      users: {},
      duration: 0,
      currentTime: 0,
      isLoading: true,
      paused: false,
      screenType: 'content',
      isBooked: false,
    };

    this.bookingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.BOOKINGS);
    this.usersRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.USERS);

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onDelete: this.onDelete,
      onPressReview: this.onPressReview,
      onPressSave: this.onPressSave,
      saved: this.state.saved,
      onPersonalMessage: this.onPersonalMessage,
      onProfileModal: this.onProfileModal,
      isAdmin: this.props.isAdmin,
      author: this.state.data.author,
      isUser: this.props.user.id === this.state.data.authorID,
    });

    setTimeout(() => {
      this.setState({didFinishAnimation: true});
    }, 500);

    this.upDateUserInfo();
    this.unsubscribe = firebaseListing.subscribeListings(
      {docId: this.item.id},
      this.onDocUpdate,
    );

    this.bookingsUnsubscribe = this.bookingsRef
      .where('listingID', '==', this.item.id)
      .onSnapshot(this.onBookingsUpdate);

    this.usersUnsubscribe = this.usersRef.onSnapshot(this.onUsersUpdate);

    this.reviewsUnsubscribe = firebaseReview.subscribeReviews({
      field: 'listingID',
    })(this.item.id, this.onReviewsUpdate);
    this.savedListingsUnsubscribe = firebaseListing.subscribeSavedListings(
      this.props.user.id,
      this.onSavedListingsCollectionUpdate,
      this.item.id,
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.bookingsUnsubscribe();
    this.reviewsUnsubscribe();
    this.savedListingsUnsubscribe();
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    const customLeft = this.props.navigation.getParam('customLeft');
    const routeName = this.props.navigation.getParam('routeName');

    if (customLeft) {
      this.props.navigation.navigate(routeName);
    } else {
      this.props.navigation.goBack();
    }

    return true;
  };

  upDateUserInfo = async () => {
    const res = await firebaseUser.getUserData(this.state.data.authorID);
    if (res.success) {
      this.setState({
        author: res.data,
      });
    }
  };

  onPersonalMessage = () => {
    const viewer = this.props.user;
    const viewerID = viewer.id || viewer.userID;
    const vendorID = this.state.data.authorID || this.state.data.authorID;
    let channel = {
      id: viewerID < vendorID ? viewerID + vendorID : vendorID + viewerID,
      participants: [this.state.data.author],
    };
    this.props.navigation.navigate('PersonalChat', {
      channel,
      appStyles: DynamicAppStyles,
    });
  };

  onContactLandlord = () => {
    const viewer = this.props.user;
    const viewerID = viewer.id || viewer.userID;
    const landlordID = this.state.data.authorID || this.state.data.authorID;

    const channelID = viewerID + landlordID;

    const channelInfo = {
      channelID: viewerID + landlordID,
      creatorID: viewerID,
      creator_id: viewerID,
      id: viewerID + landlordID,
      lastMessage: '',
      lastMessageDate: new Date(),
      renterName: viewer.firstName + viewer.lastName,
      propertyName: this.state.data.title,
    };

    const uniqueViewerId = channelID + viewerID;
    const uniqueLandlordId = channelID + landlordID;

    const createChannel = db
      .collection('channels')
      .doc(channelID)
      .set(channelInfo);

    const channelParticipation = db
      .collection('channel_participation')
      .doc(uniqueViewerId)
      .set({
        channel: channelID,
        user: viewerID,
      });
    const channel_participation = db
      .collection('channel_participation')
      .doc(uniqueLandlordId)
      .set({
        channel: channelID,
        user: landlordID,
      });

    const channel = {
      id: channelID,
      participants: [viewerID, landlordID, this.state.author.profilePictureURL],
      name: this.state.author.firstName + ' ' + this.state.author.lastName,
      propertyName: this.state.data.title,
    };
    this.props.navigation.navigate('PersonalChat', {
      channel,
      appStyles: DynamicAppStyles,
    });
  };

  onUserId = id => {
    const viewer = this.props.user;
    const viewerID = viewer.id || viewer.userID;
    const landlordID = this.state.data.authorID || this.state.data.authorID;
    if (id === viewerID) {
      return viewer.firstName + viewer.lastName;
    } else {
      return this.state.data.title;
    }
  };

  onDocUpdate = doc => {
    const listing = doc.data();

    this.setState({
      data: {...listing, id: doc.id},
      loading: false,
    });
  };

  updateReviews = reviews => {
    this.setState({
      reviews: reviews,
    });
  };

  onReviewsUpdate = (querySnapshot, usersRef) => {
    const data = [];
    const updateReviews = this.updateReviews;

    querySnapshot.forEach(doc => {
      const review = doc.data();
      data.push(review);
    });
    updateReviews(data);
  };

  onBookingsUpdate = querySnapshot => {
    const data = [];
    const acceptData = [];
    querySnapshot.forEach(doc => {
      const booking = doc.data();
      // if (this.props.user.id !== this.state.data.authorID) {
      //   if (
      //     booking.userID !== this.props.user.id &&
      //     booking.status === 'Requested'
      //   )
      //     return;
      // }
      if (booking.userID === this.props.user.id) {
        this.props.navigation.setParams({
          isAlreadyBooked: true,
        });
      }
      data.push(booking);

      if (booking.status === 'Accepted') {
        acceptData.push(booking);
      } else {
        data.push(booking);
      }
    });
    this.setState({
      bookings: data,
      acceptBookings: acceptData,
    });
  };

  onUsersUpdate = querySnapshot => {
    const data = {};

    querySnapshot.forEach(doc => {
      const user = doc.data();
      data[user.id] = user.firstName + ' ' + user.lastName;
    });

    this.setState({
      users: data,
    });
  };

  onSavedListingsCollectionUpdate = querySnapshot => {
    const savedListingdata = [];
    if (querySnapshot) {
      querySnapshot.forEach(doc => {
        const savedListing = doc.data();
        savedListingdata.push(savedListing);
      });

      this.setState({
        saved: savedListingdata.length > 0,
      });
    }

    this.props.navigation.setParams({
      saved: this.state.saved,
    });
  };

  onPressReview = () => {
    this.setState({reviewModalVisible: true});
  };

  onDelete = () => {
    Alert.alert(
      IMLocalized('Delete listing?'),
      IMLocalized('Are you sure you want to remove this listing?'),
      [
        {
          text: IMLocalized('Yes'),
          onPress: this.removeListing,
          style: 'destructive',
        },
        {text: IMLocalized('No')},
      ],
      {cancelable: false},
    );
  };

  removeListing = () => {
    const customLeft = this.props.navigation.getParam('customLeft');
    const routeName = this.props.navigation.getParam('routeName');

    firebaseListing.removeListing(this.state.data.id, ({success}) => {
      if (success) {
        Alert.alert(IMLocalized('The listing was successfully deleted.'));

        //confirmation email
        sendGrid.deleteListingConfirmation(this.props.user.email);

        if (customLeft) {
          this.props.navigation.navigate(routeName);
        } else {
          this.props.navigation.goBack();
        }
        return;
      }
      Alert.alert(IMLocalized('There was an error deleting listing!'));
    });
  };

  onReviewCancel = () => {
    this.setState({reviewModalVisible: false});
  };

  onPressBook = () => {
    this.setState({
      bookingModalVisible: true,
    });
  };

  onBookingCancel = isPaymentSuccess => {
    this.setState({
      bookingModalVisible: false,
      isBooked: isPaymentSuccess,
    });
  };

  onProfileModal = isVisible => {
    this.setState({[isVisible]: !this.state[isVisible]});
  };

  onPressSave = () => {
    const item = {...this.state.data, saved: this.state.saved};

    firebaseListing.saveUnsaveListing(item, this.props.user.id);
  };

  onVideoLoad = data => {
    this.setState({duration: data.duration, isLoading: false});
  };

  onVideoError = error => {
    Alert.alert('Something went wrong playing the video', error);
  };

  renderItem = ({item}) => {
    if (!item) {
      return null;
    }
    if (item.indexOf('mp4') > -1) {
      return (
        <View style={styles.videoItem}>
          <Video
            source={{uri: item}} // Can be a URL or a local file.
            resizeMode={'cover'}
            paused={this.state.paused}
            onLoad={this.onVideoLoad}
            onError={this.onVideoError}
            controls={true}
            repeat={true}
            style={styles.video}
            volume={0.0}
          />
        </View>
      );
    } else {
      return (
        <TouchableOpacity>
          {item.startsWith('https://') ? (
            <FastImage
              style={styles.photoItem}
              resizeMode={FastImage.resizeMode.cover}
              source={{uri: item}}
            />
          ) : (
            <FastImage
              style={styles.photoItem}
              resizeMode={FastImage.resizeMode.cover}
              source={{uri: 'https//:'}}
            />
          )}
        </TouchableOpacity>
      );
    }
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          width: 10,
          height: '100%',
        }}
      />
    );
  };

  renderBookingItem = ({item}) => {
    if (
      this.item.authorID !== this.props.user.id &&
      item.userID !== this.props.user.id
    ) {
      return null;
    }
    const createdAt = item.createdAt
      ? new Date(item.createdAt.seconds * 1000)
      : new Date();
    const startDate = new Date(item.startDate);
    const endDate = new Date(item.endDate);

    return (
      <View style={[styles.bookingItem, styles.extra]}>
        {this.item.authorID === this.props.user.id && (
          <View style={styles.extraRow}>
            <Text style={styles.extraKey}>Name</Text>
            <Text style={styles.extraValue}>
              {this.state.users[item.userID]}
            </Text>
          </View>
        )}
        <View style={styles.extraRow}>
          <Text style={styles.extraKey}>Booked At</Text>
          <Text style={styles.extraValue}>
            {Moment(createdAt).format('MMMM Do YYYY HH:mm')}
          </Text>
        </View>
        <View style={styles.extraRow}>
          <Text style={styles.extraKey}>Start Date</Text>
          <Text style={styles.extraValue}>
            {Moment(startDate).format('MMMM Do YYYY')}
          </Text>
        </View>
        <View style={styles.extraRow}>
          <Text style={styles.extraKey}>End Date</Text>
          <Text style={styles.extraValue}>
            {Moment(endDate).format('MMMM Do YYYY')}
          </Text>
        </View>
        <View style={styles.extraRow}>
          <Text style={styles.extraKey}>Status</Text>
          <Text style={styles.extraValue}>{item.status}</Text>
        </View>
        {/* {this.item.authorID === this.props.user.id && ( */}
        <TouchableOpacity onPress={() => this.showBookingDetails(item)}>
          <View style={styles.bookingDetailsButtonContainer}>
            <Text style={styles.bookingDetailsButton}>View details</Text>
          </View>
        </TouchableOpacity>
        {/* )} */}
      </View>
    );
  };

  showBookingDetails = item => {
    const viewer = this.props.user;
    const viewerID = viewer.id || viewer.userID;
    if (item.status === 'Accepted') {
      const renter =
        item.userID == viewerID ? this.state.manager : this.state.renter;
      const owner =
        item.userID == viewerID ? this.state.renter : this.state.manager;
      this.props.navigation.navigate('ApprovedDocusign', {
        renterId: item.userID,
        itemId: item.id,
        data: this.state.data,
        renter: renter,
        owner: owner,
        // managerId: this.state.data.authorID,
      });
    } else {
      this.props.navigation.navigate('BookingDetails', {
        itemId: item.id,
        renterId: item.userID,
        managerId: this.state.data.authorID,
        title: this.state.data.title,
        selfId: viewerID,
        data: this.state.data,
      });
    }
  };

  renderReviewItem = ({item}) => (
    <View style={styles.reviewItem}>
      <View style={styles.info}>
        <FastImage
          style={styles.userPhoto}
          resizeMode={FastImage.resizeMode.cover}
          source={
            item.profilePictureURL
              ? {uri: item.profilePictureURL}
              : {uri: defaultAvatar}
          }
        />
        <View style={styles.detail}>
          <Text style={styles.username}>
            {item.firstName && item.firstName} {item.lastName && item.lastName}
          </Text>
          <Text style={styles.reviewTime}>{timeFormat(item.createdAt)}</Text>
        </View>
        <StarRating
          containerStyle={styles.starRatingContainer}
          disabled={true}
          maxStars={5}
          starSize={22}
          starStyle={styles.starStyle}
          emptyStar={AppIcon.images.starNoFilled}
          fullStar={AppIcon.images.starFilled}
          halfStarColor={DynamicAppStyles.colorSet.mainThemeForegroundColor}
          rating={item.starCount}
        />
      </View>
      <Text style={styles.reviewContent}>{item.content}</Text>
    </View>
  );

  render() {
    var extraInfoArr = null;
    const {activeSlide, data, isBooked} = this.state;

    if (!data.filters) {
      return <View />;
    }
    // console.log('=====>>',this.state.author);
    const filters = data.filters;
    extraInfoArr = Object.keys(filters).map(function(key) {
      if (filters[key] != 'Any' && filters[key] != 'All') {
        return (
          <View style={styles.extraRow}>
            <Text style={styles.extraKey}>{key}</Text>
            <Text style={styles.extraValue}>{filters[key]}</Text>
          </View>
        );
      }
    });

    return (
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 75}}>
        <View style={styles.carousel}>
          <Carousel
            ref={c => {
              this._slider1Ref = c;
            }}
            data={data.photos}
            renderItem={this.renderItem}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            // hasParallaxImages={true}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
            firstItem={0}
            loop={false}
            // loopClonesPerSide={2}
            autoplay={false}
            autoplayDelay={500}
            autoplayInterval={3000}
            onSnapToItem={index => this.setState({activeSlide: index})}
          />
          <Pagination
            dotsLength={data.photos && data.photos.length}
            activeDotIndex={activeSlide}
            containerStyle={styles.paginationContainer}
            dotColor={'rgba(255, 255, 255, 0.92)'}
            dotStyle={styles.paginationDot}
            inactiveDotColor="white"
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={this._slider1Ref}
            tappableDots={!!this._slider1Ref}
          />
          {!this.props.navigation.state.params.isUser && (
            <View style={[styles.heartContainer, {left: '62.5%'}]}>
              <HeaderButton
                customStyle={styles.headerIconContainer}
                iconStyle={styles.headerIcon}
                icon={AppIcon.images.accountDetail}
                onPress={() => {
                  this.props.navigation.navigate('ListingProfileModal', {
                    userID: this.props.navigation.state.params.item.authorID,
                  });
                }}
              />
            </View>
          )}
          <View style={styles.heartContainer}>
            <HeaderButton
              customStyle={styles.headerIconContainer}
              icon={
                this.props.navigation.state.params.saved
                  ? AppIcon.images.heartFilled
                  : AppIcon.images.heart
              }
              onPress={() => {
                this.props.navigation.state.params.onPressSave();
              }}
              iconStyle={styles.heartIcon}
            />
          </View>
        </View>
        <View style={styles.headerContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{data.title} </Text>
          </View>
          <View>
            <Text style={styles.listingType}>
              {data.filters['Listing Type']}
            </Text>
          </View>
        </View>
        <View style={styles.priceContainer}>
          <Text style={styles.price}>${data.price}</Text>
        </View>
        <View style={styles.iconSpecContainer}>
          <View style={styles.specContainer}>
            <View style={styles.iconContainer}>
              <FontAwesome name="bed" size={25} color="#13d1d8" />
            </View>
            <Text style={styles.iconDescription}>
              {data.filters.Bedrooms}
              {data.filters.Bedrooms === '1' ? ' Bedroom' : ' Bedrooms'}
            </Text>
          </View>
          <View>
            <View style={styles.iconContainer}>
              <FontAwesome5 name="bath" size={25} color="#13d1d8" />
            </View>
            <Text style={styles.iconDescription}>
              {data.filters.Bathrooms}
              {data.filters.Bathrooms === '1' ? ' Bathroom' : ' Bathrooms'}
            </Text>
          </View>
          <View>
            <View style={styles.iconContainer}>
              <FontAwesome name="car" size={25} color="#13d1d8" />
            </View>
            <Text style={styles.iconDescription}>Garage</Text>
          </View>
        </View>
        <View style={styles.detailsContainer}>
          <Text style={styles.description}> {data.description} </Text>
        </View>

        {this.item.authorID !== this.props.user.id && (
          <>
            <View style={styles.landlordInfoContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome name="user" size={30} color="orange" />
              </View>
              <View style={styles.landlord}>
                <Text> Meet the landlord: *Landlord Name*</Text>
              </View>
            </View>

            <TouchableOpacity onPress={this.onContactLandlord}>
              <View style={styles.messageButtonContainer}>
                <Text style={styles.messageButton}>Contact Manager</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity disabled={isBooked} onPress={this.onPressBook}>
              <View style={styles.bookingButtonContainer}>
                <Text style={styles.bookingButton}>
                  {!isBooked ? 'Book Now' : 'Booked'}
                </Text>
              </View>
            </TouchableOpacity>
          </>
        )}
        {this.state.bookingModalVisible && (
          <BookingModal
            onCancel={this.onBookingCancel}
            data={data}
            bookings={this.state.bookings}
            userID={this.props.user.id}
            author={this.state.author}
            tenant={this.props.user}
            managerID={this.state.data.authorID}
          />
        )}
        <Text style={styles.title}> {IMLocalized('Location')} </Text>
        {data.address && (
          <View style={styles.extra}>
            <View style={styles.extraRow}>
              <Text style={styles.extraKey}>Street</Text>
              <Text style={styles.extraValue}>
                {(data.address.street_number
                  ? data.address.street_number.long_name + ' '
                  : '') +
                  (data.address.street ? data.address.street.long_name : '')}
              </Text>
            </View>
            <View style={styles.extraRow}>
              <Text style={styles.extraKey}>City</Text>
              <Text style={styles.extraValue}>
                {data.address.city ? data.address.city.long_name : ''}
              </Text>
            </View>
            <View style={styles.extraRow}>
              <Text style={styles.extraKey}>State</Text>
              <Text style={styles.extraValue}>
                {data.address.state ? data.address.state.long_name : ''}
              </Text>
            </View>
            <View style={styles.extraRow}>
              <Text style={styles.extraKey}>Postal Code</Text>
              <Text style={styles.extraValue}>
                {data.address.postal_code
                  ? data.address.postal_code.long_name
                  : ''}
              </Text>
            </View>
            <View style={styles.extraRow}>
              <Text style={styles.extraKey}>Country</Text>
              <Text style={styles.extraValue}>
                {data.address.country ? data.address.country.long_name : ''}
              </Text>
            </View>
          </View>
        )}
        {data && this.state.didFinishAnimation ? (
          <MapView
            style={styles.mapView}
            initialRegion={{
              latitude: data.address.location.coordinates.latitude,
              longitude: data.address.location.coordinates.longitude,
              latitudeDelta: LATITUDEDELTA,
              longitudeDelta: LONGITUDEDELTA,
            }}>
            <Marker
              coordinate={{
                latitude: data.address.location.coordinates.latitude,
                longitude: data.address.location.coordinates.longitude,
              }}
            />
          </MapView>
        ) : (
          <View style={[styles.mapView, styles.loadingMap]}>
            <ActivityIndicator size="small" color={AppStyles.color.main} />
          </View>
        )}
        <Text style={styles.title}> {IMLocalized('Extra info')} </Text>
        {extraInfoArr && <View style={styles.extra}>{extraInfoArr}</View>}
        {this.state.reviews.length > 0 && (
          <Text style={[styles.title, styles.reviewTitle]}>
            {' '}
            {IMLocalized('Reviews')}{' '}
          </Text>
        )}
        <FlatList
          data={this.state.reviews}
          renderItem={this.renderReviewItem}
          keyExtractor={item => `${item.id}`}
          initialNumToRender={5}
        />

        {this.state.bookings.length > 0 && (
          <Text style={styles.title}>
            {' '}
            {IMLocalized('Booking Information')}{' '}
          </Text>
        )}
        <FlatList
          data={this.state.bookings}
          renderItem={this.renderBookingItem}
          keyExtractor={item => `${item.id}`}
          initialNumToRender={5}
        />
        {this.state.acceptBookings.length > 0 && (
          <Text style={styles.title}> My Bookings</Text>
        )}
        <FlatList
          data={this.state.acceptBookings}
          renderItem={this.renderBookingItem}
          keyExtractor={item => `${item.id}`}
          initialNumToRender={5}
        />
        {this.state.reviewModalVisible && (
          <ReviewModal
            field="listingID"
            data={data}
            onCancel={this.onReviewCancel}
            onDone={this.onReviewCancel}
          />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headerIconContainer: {
    marginRight: 10,
  },
  headerIcon: {
    tintColor: '#13d1d8',
    height: 25,
    width: 25,
    marginLeft: 10,
  },
  heartIcon: {
    tintColor: 'red',
    height: 25,
    width: 25,
    marginLeft: 10,
  },
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
    color: AppStyles.color.title,
    fontSize: 25,
  },
  reviewTitle: {},
  description: {
    fontFamily: AppStyles.fontName.bold,
    marginHorizontal: '5%',
    color: '#9b9b9b',
  },
  photoItem: {
    backgroundColor: AppStyles.color.grey,
    height: 250,
    width: '100%',
  },
  videoItem: {
    height: 250,
    width: '100%',
  },
  video: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  paginationContainer: {
    flex: 1,
    position: 'absolute',
    alignSelf: 'center',
    paddingVertical: 8,
    marginTop: 220,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
  },
  mapView: {
    width: '100%',
    height: 200,
    // backgroundColor: AppStyles.color.grey
  },
  loadingMap: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  extra: {
    padding: 30,
    paddingTop: 10,
    paddingBottom: 0,
    marginBottom: 30,
  },
  extraRow: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  extraKey: {
    flex: 1,
    color: AppStyles.color.title,
    fontWeight: 'bold',
  },
  extraValue: {
    flex: 2,
    color: '#666',
  },
  reviewItem: {
    // padding: 10,
    // marginLeft: 10
  },
  info: {
    flexDirection: 'row',
  },
  userPhoto: {
    width: 44,
    height: 44,
    borderRadius: 22,
  },
  detail: {
    // paddingLeft: 10,
    flex: 1,
  },
  username: {
    color: AppStyles.color.title,
    fontWeight: 'bold',
  },
  reviewTime: {
    color: '#bcbfc7',
    fontSize: 12,
  },
  starRatingContainer: {
    padding: 10,
  },
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
  reviewContent: {
    color: AppStyles.color.title,
    marginTop: 10,
  },
  bookingButtonContainer: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    marginHorizontal: 100,
    marginVertical: 10,
    borderRadius: 10,
    // padding: 10,
    // borderWidth: 0.3,
    // borderColor: '#333'
  },
  bookingButton: {
    textAlign: 'center',
    color: 'white',
    fontSize: 25,
  },
  bookingDetailsButtonContainer: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    marginVertical: 10,
    borderRadius: 5,
    paddingVertical: 5,
  },
  bookingDetailsButton: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
  },
  iconSpecContainer: {
    flexDirection: 'row',
    // borderColor: 'black',
    // borderWidth: 1,
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  detailsContainer: {
    justifyContent: 'center',
    // borderColor: 'black',
    // borderWidth: 1,
    paddingVertical: 15,
    // marginHorizontal: '3%'
  },
  landlordInfoContainer: {
    flexDirection: 'row',
    padding: 20,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingRight: 5,
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: '5%',
    marginRight: 10,
  },
  listingType: {
    marginTop: 10,
    width: 110,
    borderRadius: 3,
    backgroundColor: '#13d1d8',
    color: 'white',
    textAlign: 'center',
    fontFamily: 'HelveticaNeue',
    fontSize: 12,
    fontWeight: 'bold',
  },
  priceContainer: {
    marginHorizontal: '5%',
    height: hp('4%'),
    // borderColor: 'black',
    // borderWidth: 1,
    justifyContent: 'center',
  },
  price: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    color: 'grey',
  },
  iconContainer: {
    alignItems: 'center',
    marginVertical: '3%',
  },
  landlord: {
    justifyContent: 'center',
  },
  iconDescription: {
    color: 'grey',
  },
  heartContainer: {
    // borderColor: 'black',
    // borderWidth: 1,
    marginLeft: 35,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'absolute',
    left: '75%',
    top: '90%',
  },
  messageButton: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  messageButtonContainer: {
    backgroundColor: 'orange',
    marginHorizontal: 100,
    borderRadius: 10,
    height: 30,
    justifyContent: 'center',
    marginBottom: 10,
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonLabel: {
    fontSize: 20,
    marginLeft: -5,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
  isAdmin: state.auth.user ? state.auth.user.isAdmin : false,
});

export default connect(mapStateToProps)(DetailsScreen);
