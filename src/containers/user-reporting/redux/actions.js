import * as CONSTANTS from './constants';

export const setBannedUserIDs = data => ({
  type: CONSTANTS.SET_BANNED_USER_IDS,
  data,
});
