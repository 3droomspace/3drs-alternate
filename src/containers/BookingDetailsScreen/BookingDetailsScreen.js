import React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Alert,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';

import {connect} from 'react-redux';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import FastImage from 'react-native-fast-image';
import Moment from 'moment';
import DocumentPicker from 'react-native-document-picker';
import {Configuration} from '/Configuration';
import {createChannel} from '@Firebase/channel';

import RequestDocumentsModal from '@Components/RequestDocumentsModal';

import ServerConfiguration from '/ServerConfiguration';
import {AppStyles} from '/AppStyles';
import DynamicAppStyles from '/DynamicAppStyles';
import DocusignLoginModal from '../../components/DocusignLoginModal';
import {
  getOrRenewAccessToken,
  uploadDocumentToServer,
} from '../../helpers/docusign';

const db = firebase.firestore();

export class BookingDetailsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      booking: {},
      renter: {},
      manager: {},
      showRequestDocumentsModal: false,
      IDDoc: null,
      loading: false,
      showDocusign: false,
      agreementPDF: '',
    };

    this.bookingsRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.BOOKINGS);
    this.usersRef = firebase
      .firestore()
      .collection(ServerConfiguration.database.collection.USERS);

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    const {itemId, renterId, managerId} = this.props.navigation.state.params;
    this.bookingsRef
      .where('id', '==', itemId)
      .onSnapshot(this.onBookingsUpdate);

    this.bookingsRef
      .doc(itemId)
      .get()
      .then(response => {
        this.setState({agreementPDF: response._data.docusignPDF});
      });

    this.usersRef
      .where('id', '==', renterId)
      .onSnapshot(querySnapshot => this.onUsersUpdate(querySnapshot, 'renter'));

    this.usersRef
      .where('id', '==', managerId)
      .onSnapshot(querySnapshot =>
        this.onUsersUpdate(querySnapshot, 'manager'),
      );
  }

  onBookingsUpdate = querySnapshot => {
    let booking = {};

    querySnapshot.forEach(doc => {
      booking = doc.data();
    });

    this.setState({booking});
  };

  onUsersUpdate = (querySnapshot, userType) => {
    let user = {};

    querySnapshot.forEach(doc => {
      user = doc.data();
    });

    console.log(userType, user);
    if (userType === 'renter') {
      this.setState({
        renter: user,
      });
    } else {
      this.setState({
        manager: user,
      });
    }
  };

  requestDocuSign = () => {
    // this.setState({
    //   showRequestDocumentsModal: true,
    // });

    const {renterId, selfId, itemId, data} = this.props.navigation.state.params;
    const renter = renterId == selfId ? this.state.manager : this.state.renter;
    const owner = renterId == selfId ? this.state.renter : this.state.manager;
    this.props.navigation.navigate('ApprovedDocusign', {
      renterId: renterId,
      itemId: itemId,
      data: data,
      renter: renter,
      owner: owner,
      // managerId: this.state.data.authorID,
    });
  };

  signRentalAgreement = () => {
    const {renterId, itemId, data} = this.props.navigation.state.params;
    this.props.navigation.navigate('ApprovedDocusign', {
      renterId: renterId,
      itemId: itemId,
      data: data,
      // managerId: this.state.data.authorID,
    });
  };

  uploadID = async () => {
    try {
      const doc = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });

      this.setState({
        IDDoc: doc,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
      }
    }
  };

  uploadIDToServer = access_token => {
    const {renterId, selfId} = this.props.navigation.state.params;
    const renter = renterId == selfId ? this.state.manager : this.state.renter;

    this.setState({
      loading: true,
    });

    fetch(Configuration.GET_PDF_BASE64_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pdfURL: this.state.agreementPDF,
      }),
    })
      .then(res => res.json())
      .then(pdfResult => {
        var myHeaders = new Headers();
        myHeaders.append('Authorization', 'Bearer ' + access_token);
        fetch(Configuration.DOCUSIGN_USERINFO_URL, {
          method: 'GET',
          headers: myHeaders,
        })
          .then(response => response.json())
          .then(result => {
            var baseUrl = result.accounts[0].base_uri;
            var accountId = result.accounts[0].account_id;
            var myHeaders1 = new Headers();
            myHeaders1.append('Authorization', 'Bearer ' + access_token);
            myHeaders1.append('Content-Type', 'application/json');
            var raw = JSON.stringify({
              documents: [
                {
                  documentBase64: pdfResult.data,
                  documentId: '1',
                  fileExtension: 'pdf',
                  name: 'NDA.pdf',
                },
              ],
              emailSubject: 'Please sign the NDA',
              recipients: {
                signers: [
                  {
                    email: renter.email,
                    name: renter.firstName + ' ' + renter.lastName,
                    recipientId: '1',
                    routingOrder: '1',
                    tabs: {
                      dateSignedTabs: [
                        {
                          anchorString: 'signer1date',
                          anchorYOffset: '-6',
                          fontSize: 'Size12',
                          name: 'Date Signed',
                          recipientId: '1',
                          tabLabel: 'date_signed',
                        },
                      ],
                      fullNameTabs: [
                        {
                          anchorString: 'signer1name',
                          anchorYOffset: '-6',
                          fontSize: 'Size12',
                          name: 'Full Name',
                          recipientId: '1',
                          tabLabel: 'Full Name',
                        },
                      ],
                      signHereTabs: [
                        {
                          anchorString: 'signer1sig',
                          anchorUnits: 'mms',
                          anchorXOffset: '0',
                          anchorYOffset: '0',
                          name: 'Please sign here',
                          optional: 'false',
                          recipientId: '1',
                          scaleValue: 1,
                          tabLabel: 'signer1sig',
                        },
                      ],
                    },
                  },
                ],
              },
              status: 'sent',
            });
            fetch(
              baseUrl + '/restapi/v2/accounts/' + accountId + '/envelopes',
              {
                method: 'POST',
                headers: myHeaders1,
                body: raw,
              },
            )
              .then(response => response.json())
              .then(result => {
                this.bookingsRef
                  .doc(this.state.booking.id)
                  .update({
                    status: 'Accepted',
                  })
                  .then(() => {});

                createChannel(this.props.user, [this.state.renter], null).then(
                  () => {
                    this.setState({ loading: false });
                    Alert.alert(
                      '3DRS',
                      'Request has been accepted and you can now chat with ' +
                        this.state.renter.firstName +
                        '!',
                      [{text: 'Chat', onPress: this.goToChat}, {text: 'Close'}],
                    );
                  },
                );
              });
          });
      });

    // uploadDocumentToServer(
    //   access_token,
    //   false,
    //   [this.state.IDDoc],
    //   this.state.manager.email,
    //   this.state.manager.firstName + ' ' + this.state.manager.lastName,
    // )
    //   .then(() => {
    //     Alert.alert('ID was sent to the manager via email!');

    //     this.finalizeSigning();
    //   })
    //   .catch(err => {
    //     console.error(err);
    //     Alert.alert(err.toString());

    //     this.setState({
    //       loading: false,
    //     });
    //   });
  };

  // Role: Renter
  completeSigning = async () => {
    this.setState({
      loading: true,
    });

    if (this.state.IDDoc) {
      getOrRenewAccessToken()
        .then(accessToken => {
          if (accessToken) {
            this.uploadIDToServer(accessToken);
          } else {
            this.setState({
              showDocusign: true,
            });
          }
        })
        .catch(err => {
          this.setState({
            loading: false,
          });
          Alert.alert(err.toString());
        });
    } else {
      this.setState({
        loading: false,
      });
      this.finalizeSigning();
    }
  };

  // Role: Renter
  finalizeSigning = () => {
    this.bookingsRef
      .doc(this.state.booking.id)
      .update({
        status: 'DocuSign Signed',
      })
      .then(() => {
        this.setState({
          loading: false,
        });
      });
  };

  // Role: Renter
  cancelBooking = () => {
    this.bookingsRef
      .doc(this.state.booking.id)
      .update({
        status: 'Cancelled by Renter',
      })
      .then(() => {});
  };

  // Role: Manager
  closeRequestDocumentsModal = (requestID = false, requestSignature) => {
    this.setState({
      showRequestDocumentsModal: false,
    });

    if (requestID || requestSignature) {
      this.bookingsRef
        .doc(this.state.booking.id)
        .update({
          status: 'DocuSign Requested',
          requestID: requestID,
          docusignRequested: requestSignature,
        })
        .then(() => {});
    }
  };

  // Role: Manager
  acceptRequest = () => {
    this.setState({showDocusign: true});
  };

  closeDocusignModal = () => {
    this.setState({
      showDocusign: false,
      loading: false,
    });
  };

  gotAccessToken = response => {
    this.setState({
      showDocusign: false,
    });
    response = {
      ...response,
      updated: Date.now(),
    };

    AsyncStorage.setItem('docusignAuth', JSON.stringify(response)).then(() =>
      this.uploadIDToServer(response.access_token),
    );
  };

  goToChat = () => {
    const {title, selfId} = this.props.navigation.state.params;
    const {id, userID, firstName, lastName} = this.props.user;
    const user = renterId == selfId ? this.state.manager : this.state.renter;
    // Need the following
    // id from owner
    // const ownerId = selfId;
    const ownerId = user.userID;
    // id from renter
    const renterId = userID;
    // renter name
    const renterName = firstName + ' ' + lastName;
    // property name
    const propertyName = title;

    const channelID = ownerId + renterId;

    // this.props.navigation.navigate('Messages');
    const channelInfo = {
      channelID: channelID,
      creatorID: ownerId,
      creator_id: ownerId,
      id: channelID,
      lastMessageDate: new Date(),
      renterName: renterName,
      propertyName: propertyName,
    };
    const uniqueID = channelID + renterId;
    const unique = channelID + ownerId;

    const createChannel = db
      .collection('channels')
      .doc(channelID)
      .set(channelInfo);
    const channelParticipation = db
      .collection('channel_participation')
      .doc(uniqueID)
      .set({
        channel: channelID,
        user: renterId,
      });
    const channel_participation = db
      .collection('channel_participation')
      .doc(unique)
      .set({
        channel: channelID,
        user: ownerId,
      });

    const channel = {
      id: channelID,
      participants: [renterId, ownerId],
    };
    this.props.navigation.navigate('PersonalChat', {
      channel,
      appStyles: DynamicAppStyles,
    });
  };

  render() {
    const {
      booking,
      showRequestDocumentsModal,
      loading,
      showDocusign,
    } = this.state;
    const {renterId, selfId} = this.props.navigation.state.params;

    const user = renterId == selfId ? this.state.manager : this.state.renter;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.containerStyle}>
        {loading && (
          <View style={styles.loadingView}>
            <ActivityIndicator size="large" color={AppStyles.color.main} />
          </View>
        )}

        {showDocusign && (
          <DocusignLoginModal
            onCancel={this.closeDocusignModal}
            gotAccessToken={this.gotAccessToken}
          />
        )}

        <FastImage
          style={styles.userAvatar}
          resizeMode={FastImage.resizeMode.cover}
          source={{uri: user.profilePictureURL}}
        />

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Name</Text>
          <Text style={styles.infoRowValue}>
            {user.firstName + ' ' + user.lastName}
          </Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Start Date</Text>
          <Text style={styles.infoRowValue}>
            {Moment(booking.startDate).format('MMMM Do YYYY')}
          </Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>End Date</Text>
          <Text style={styles.infoRowValue}>
            {Moment(booking.endDate).format('MMMM Do YYYY')}
          </Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Number of Occupants</Text>
          <Text style={styles.infoRowValue}>{booking.occupants}</Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Have pets</Text>
          <Text style={styles.infoRowValue}>{booking.pets ? 'Yes' : 'No'}</Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Message</Text>
          <Text style={styles.infoRowValue}>{booking.message}</Text>
        </View>

        <View style={styles.infoRow}>
          <Text style={styles.infoRowLabel}>Status</Text>
          <Text style={styles.infoRowValue}>{booking.status}</Text>
        </View>

        {(booking.status === 'DocuSign Requested' ||
          booking.status === 'DocuSign Signed') && (
          <>
            <View style={styles.infoRow}>
              <Text style={styles.infoRowLabel}>Request ID</Text>
              <Text style={styles.infoRowValue}>
                {booking.requestID ? 'Yes' : 'No'}
              </Text>
            </View>
            <View style={styles.infoRow}>
              <Text style={styles.infoRowLabel}>Documents Requested</Text>
              <Text style={styles.infoRowValue}>
                {booking.docusignRequested ? 'Yes' : 'No'}
              </Text>
            </View>
          </>
        )}

        {/* {(booking.status === 'DocuSign Signed' ||
          booking.status === 'Accepted') &&
          booking.IDDocumentUrl != null && (
            <>
              <View style={styles.infoRow}>
                <Text style={styles.infoRowLabel}>ID Url</Text>
                <Text
                  style={{
                    ...styles.infoRowValue,
                    color: DynamicAppStyles.colorSet.mainThemeForegroundColor,
                  }}
                  onPress={() => Linking.openURL(booking.IDDocumentUrl)}>
                  {booking.IDDocumentUrl}
                </Text>
              </View>
            </>
          )} */}

        {/* {booking.status === 'Accepted' &&
          booking.documentUrls &&
          booking.documentUrls.length > 0 && (
            <View style={styles.infoRow}>
              <Text style={styles.infoRowLabel}>Signed Documents</Text>
              <View style={styles.infoRowValue}>
                {booking.documentUrls.map(url => {
                  return (
                    <Text
                      style={styles.documentUrl}
                      onPress={() => Linking.openURL(url)}>
                      {url}
                    </Text>
                  );
                })}
              </View>
            </View>
          )} */}

        <View style={styles.actionButtonsContainer}>
          {booking.status === 'Requested' && renterId != selfId && (
            <TouchableOpacity
              onPress={this.requestDocuSign}
              style={styles.actionButton}>
              <Text style={styles.actionButtonText}>Review Documents</Text>
            </TouchableOpacity>
          )}

          {booking.status === 'DocuSign Signed' && renterId != selfId && (
            <TouchableOpacity
              onPress={this.acceptRequest}
              style={styles.actionButton}>
              <Text style={styles.actionButtonText}>
                Finalize Signing and Accept
              </Text>
            </TouchableOpacity>
          )}

          {booking.status === 'Requested' && renterId != selfId && (
            <TouchableOpacity
              onPress={this.acceptRequest}
              style={styles.actionButton}>
              <Text style={styles.actionButtonText}>Accept</Text>
            </TouchableOpacity>
          )}

          {booking.status === 'DocuSign Requested' && renterId != selfId && (
            <TouchableOpacity
              onPress={this.requestDocuSign}
              style={styles.actionButton}>
              <Text style={styles.actionButtonText}>Re-request Documents</Text>
            </TouchableOpacity>
          )}

          {booking.status === 'DocuSign Requested' &&
            renterId == selfId &&
            booking.requestID && (
              <TouchableOpacity
                onPress={this.uploadID}
                style={styles.actionButton}>
                <Text style={styles.actionButtonText}>Upload ID</Text>
              </TouchableOpacity>
            )}

          {booking.status === 'DocuSign Requested' &&
            renterId == selfId &&
            booking.docusignRequested && (
              <TouchableOpacity
                disabled={booking.requestID && !this.state.IDDoc}
                onPress={this.completeSigning}
                style={
                  !(booking.requestID && !this.state.IDDoc)
                    ? styles.actionButton
                    : {
                        ...styles.actionButton,
                        backgroundColor: 'lightgrey',
                      }
                }>
                <Text style={styles.actionButtonText}>Complete Signing</Text>
              </TouchableOpacity>
            )}

          {booking.status !== 'Cancelled by Renter' && renterId == selfId && (
            <TouchableOpacity
              onPress={this.cancelBooking}
              style={{
                ...styles.actionButton,
                backgroundColor: 'red',
              }}>
              <Text style={styles.actionButtonText}>Cancel Booking</Text>
            </TouchableOpacity>
          )}

          {booking.status === 'Accepted' && (
            <TouchableOpacity
              style={styles.actionButton}
              onPress={this.goToChat}>
              <Text style={styles.actionButtonText}>Chat</Text>
            </TouchableOpacity>
          )}
        </View>

        {showRequestDocumentsModal && (
          <RequestDocumentsModal
            onCancel={this.closeRequestDocumentsModal}
            renter={user}
          />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    marginBottom: 50,
  },
  loadingView: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerStyle: {
    padding: 10,
    paddingBottom: 25,
  },
  userInformation: {
    width: '100%',
    padding: 5,
    paddingTop: 20,
  },
  userAvatar: {
    backgroundColor: AppStyles.color.grey,
    height: 200,
    width: 200,
    borderRadius: 100,
    alignSelf: 'center',
    marginBottom: 20,
  },
  infoRow: {
    display: 'flex',
    flexDirection: 'row',
    marginVertical: 5,
    paddingHorizontal: 20,
  },
  infoRowLabel: {
    flex: 1,
    color: AppStyles.color.title,
    fontWeight: 'bold',
  },
  infoRowValue: {
    flex: 3,
    color: '#666',
  },
  actionButtonsContainer: {
    marginVertical: 30,
    paddingHorizontal: 50,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  actionButton: {
    backgroundColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    marginVertical: 5,
    borderRadius: 5,
    paddingVertical: 5,
  },
  actionButtonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
  },
  documentUrl: {
    color: DynamicAppStyles.colorSet.mainThemeForegroundColor,
    marginBottom: 10,
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(BookingDetailsScreen);
