import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {Content, Container} from 'native-base';
import DocumentPicker from 'react-native-document-picker';
import email from 'react-native-email';
import GradientButton from '@Components/Buttons/GradientButton';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import Mailer from 'react-native-mail';
var RNGRP = require('react-native-get-real-path');

const ContactUsForm = props => {
  const [form, manageForm] = useState({
    'First Name': props.user.firstName,
    'Last Name': props.user.lastName,
    Email: props.user.email,
  });

  const [file, setFile] = useState(undefined);

  const onFormChange = (input, title) => {
    manageForm({...form, [title]: input});
  };

  const openDocumentFile = async () => {
    try {
      const result = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      setFile(result);
      // console.log('path ===>>>', result);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  };
  const createTwoButtonAlert = () =>
    Alert.alert(
      'Message sent successfully',
      "We'll review your request shortly",
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );

  const handleEmail = async () => {
    if (form['First Name']) {
      let path = '';
      let attachments;
      if (file) {
        if (Platform.OS === 'android') {
          path = await RNGRP.getRealPathFromURI(file.uri);
        } else {
          path = file.uri;
        }
        console.log('path ===>>>', path);
        if (path != null) {
          attachments = [
            {
              path: path,
              type: file.type,
              name: file.name,
            },
          ];
        }
      }
      Mailer.mail(
        {
          recipients: ['info@3droomspace.com'],
          ccRecipients: ['info@3droomspace.com'],
          bccRecipients: ['info@3droomspace.com'],
          subject: `Support ${form['First Name'] + ' ' + form['Last Name']}`,
          body: form.Comments,
          customChooserTitle: `Support ${form['First Name'] +
            ' ' +
            form['Last Name']}`, // Android only (defaults to "Send Mail")
          attachments: attachments,
        },
        (error, event) => {
          console.log('error ===>>>', error);
          Alert.alert(
            error,
            event,
            [
              {
                text: 'Ok',
                onPress: () => console.log('OK: Email Error Response'),
              },
              {
                text: 'Cancel',
                onPress: () => console.log('CANCEL: Email Error Response'),
              },
            ],
            {cancelable: true},
          );
        },
      );
    }
  };

  const shadowOpt = {
    width: 160,
    height: 170,
    color: '#000',
    border: 2,
    radius: 3,
    opacity: 0.2,
    x: 0,
    y: 3,
    style: {marginVertical: 5},
  };

  return (
    <View style={styles.form}>
      {/* <View style={styles.backContainer}>
        <TouchableOpacity onPress={props.modalOff}>
          <Text style={{ color: 'black' }}>Back</Text>
        </TouchableOpacity>
      </View> */}
      <View style={styles.inputContainer}>
        <View>
          <View style={styles.title}>
            <Text style={styles.titleText}>Contact</Text>
            <Text
              style={{
                fontSize: 40,
                fontWeight: 'bold',
                color: '#3bcdc5',
              }}>
              {' '}
              3DRS
            </Text>
          </View>

          <TextInput
            value={props.user.firstName}
            style={styles.textInput}
            placeholder={'First Name'}
            placeholderTextColor="grey"
            onChangeText={text => {
              let title = 'First Name';
              onFormChange(text, title);
            }}
          />
          <TextInput
            value={props.user.lastName}
            style={styles.textInput}
            placeholder={'Last Name'}
            placeholderTextColor="grey"
            onChangeText={text => {
              let title = 'Last Name';
              onFormChange(text, title);
            }}
          />
          <TextInput
            value={props.user.email}
            style={styles.textInput}
            placeholder={'Email'}
            placeholderTextColor="grey"
            onChangeText={text => {
              let title = 'Email';
              onFormChange(text, title);
            }}
          />
          <View style={styles.attatchment}>
            <Text>Attatchment</Text>
            <TouchableOpacity
              onPress={() => {
                openDocumentFile();
              }}>
              <Text style={{color: '#40E0D0'}}>Upload</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.commentContainer}>
            <TextInput
              style={styles.comments}
              placeholder={'Comments or Questions?'}
              onChangeText={text => {
                let title = 'Comments';
                onFormChange(text, title);
              }}
            />
          </View>
          <GradientButton color="green" onPress={() => handleEmail()}>
            {IMLocalized('CONTACT US')}
          </GradientButton>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = ({app, auth}) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, null)(ContactUsForm);

const styles = StyleSheet.create({
  form: {
    // justifyContent: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    // height: '70%',
    // flex: 1,
    // display: 'flex',
  },
  titleText: {
    fontSize: 40,
    fontWeight: 'bold',
    fontFamily: 'HelveticaNeue-Bold',
  },
  textInput: {
    marginTop: '3%',
    // marginBottom: 10,
    borderBottomColor: '#d9d8d7',
    // paddingBottom: 5,
    borderBottomWidth: 1,
    paddingBottom: 20,
  },
  inputContainer: {
    // marginTop: '-10%',
    marginTop: 20,
    width: '80%',
    height: '85%',
  },
  attatchment: {
    marginTop: '3%',
    marginBottom: 30,
    paddingBottom: 10,
    paddingTop: 15,
    paddingLeft: 5,
    color: 'grey',
    borderBottomColor: '#d9d8d7',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 25,
  },
  commentContainer: {
    height: '30%',
    borderColor: '#d9d8d7',
    borderWidth: 2,
    marginBottom: '10%',
    padding: 10,
    // opacity: 0.1,
  },
  comments: {
    marginTop: '6%',
    height: '95%',
    borderRadius: 10,
    backgroundColor: 'white',
    color: 'black',
  },
  title: {
    flexDirection: 'row',
    marginBottom: '2%',
  },
  backContainer: {
    marginRight: '70%',
    marginBottom: 20,
  },
  // commentContainer: {
  //   // borderColor: 'black',
  //   // borderWidth: 1,

  //   marginVertical: 10,
  //   marginBottom: 55,
  // },
  commentContact: {
    height: '100%',
  },
  contactButton: {
    backgroundColor: 'blue',
  },
});
