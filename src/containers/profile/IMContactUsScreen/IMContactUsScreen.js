import React, {Component} from 'react';
import {BackHandler, Linking, Modal} from 'react-native';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import ContactUsForm from '../components/IMContactUs/IMContactUsForm';

class IMContactUsScreen extends Component {
  // static navigationOptions = ({screenProps, navigation}) => {
  //   // let appStyles = navigation.state.params.appStyles;
  //   let screenTitle =
  //     navigation.state.params.screenTitle || IMLocalized('Contact Us');
  //   let currentTheme = appStyles.navThemeConstants[screenProps.theme];
  //   return {
  //     headerTitle: screenTitle,
  //     headerStyle: {
  //       backgroundColor: currentTheme.backgroundColor,
  //     },
  //     headerTintColor: currentTheme.fontColor,
  //   };
  // };

  constructor(props) {
    super(props);

    this.appStyles = props.navigation.getParam('appStyles') || props.appStyles;
    this.form = props.navigation.getParam('form') || props.form;
    this.phone = props.navigation.getParam('phone') || props.phone;
    this.initialValuesDict = {};

    this.state = {
      alteredFormDict: {},
      modalVisible: true,
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.formSent = this.formSent.bind(this);
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onFormButtonPress = _buttonField => {
    Linking.openURL(`tel:${this.phone}`);
  };

  formSent() {
    this.setState({modalVisible: false});
    this.onBackButtonPressAndroid();
  }

  render() {
    return (
      // <Modal visible={this.state.modalVisible} animationType="fade">
      <ContactUsForm
        modalVisible={this.state.modalVisible}
        modalOff={this.formSent}
        form={this.form}
      />
      // </Modal>
    );
  }
}

export default IMContactUsScreen;
