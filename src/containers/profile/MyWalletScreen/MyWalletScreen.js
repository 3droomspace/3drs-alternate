import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {AppIcon} from '/AppStyles';

const {width: screenWidth} = Dimensions.get('window');

function PaymentCard({title}) {
  return (
    <TouchableOpacity style={styles.paymentCard}>
      <Image source={AppIcon.images.wishlistFilled} style={styles.tranIcon} />
      <SizedBox height={8} />
      <Text style={styles.cardTitle}>{title}</Text>
    </TouchableOpacity>
  );
}

function SubscriptionCard({title, description, hasViewAll}) {
  return (
    <TouchableOpacity style={styles.subscriptionCard} disabled={hasViewAll}>
      <Image
        source={require('../../../../assets/Images/Intro/video-buffer.png')}
        style={styles.sImage}
      />
      <SizedBox height={8} />

      {hasViewAll ? (
        <TouchableOpacity style={styles.sViewAllBtn}>
          <Text style={styles.sViewAllText}>{'View all'}</Text>
        </TouchableOpacity>
      ) : (
        <>
          <Text style={styles.cardTitle}>{title}</Text>
          <SizedBox height={2} />
          <Text style={styles.cardDescription}>{description}</Text>
        </>
      )}
    </TouchableOpacity>
  );
}

function TransactionCard({cardType, title, date, stOne, stTwo}) {
  return (
    <TouchableOpacity style={styles.transactionCard}>
      <View style={[styles.tranIconWrapper, styles.tranContentWrapper]}>
        <Image source={AppIcon.images.wishlistFilled} style={styles.tranIcon} />
      </View>
      <View style={[styles.tranInfoWrapper, styles.tranContentWrapper]}>
        <Text style={styles.cardTitle}>{title}</Text>
        <Text style={styles.cardDescription}>{date}</Text>
      </View>
      <View style={[styles.tranStatusWrapper, styles.tranContentWrapper]}>
        <Text style={[styles.cardTitle, styles.tranStOne]}>{stOne}</Text>
        <Text style={[styles.cardDescription, styles.tranStTwo]}>{stTwo}</Text>
      </View>
    </TouchableOpacity>
  );
}

function SizedBox({width, height}) {
  return <View style={{width: width || null, height: height || null}} />;
}

const TRANSACTIONS = [
  {
    title: 'Transfer money to Nadal...',
    date: 'July 4, 2020 - 11:17',
    stOne: '- $ 20.00',
    stTwo: 'Success',
  },
  {
    title: 'Transfer money to Nadal...',
    date: 'July 4, 2020 - 11:17',
    stOne: '- $ 40.00',
    stTwo: 'Success',
  },
  {
    title: 'Transfer money to Nadal...',
    date: 'July 4, 2020 - 11:17',
    stOne: '+4',
    stTwo: 'Success',
  },
  {
    title: 'Transfer money to Nadal...',
    date: 'July 4, 2020 - 11:17',
    stOne: '+1',
    stTwo: 'Success',
  },
];

export default function MyWalletScreen() {
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <SizedBox height={24} />
        <View style={styles.row}>
          <PaymentCard title={'Make a Payment'} />
          <SizedBox width={16} />
          <PaymentCard title={'Payment Methods'} />
        </View>
        <SizedBox height={24} />

        <Text style={styles.headerTitle}>Subscriptions</Text>
        <SizedBox height={24} />
        <View style={styles.row}>
          <SubscriptionCard
            title={'Feather Apartments'}
            description={'$ 2450 per month'}
          />
          <SizedBox width={16} />
          <SubscriptionCard hasViewAll />
        </View>
        <SizedBox height={24} />

        <View style={[styles.row, {justifyContent: 'space-between'}]}>
          <Text style={styles.headerTitle}>Transaction History</Text>
          <TouchableOpacity>
            <Text style={[styles.headerTitle, {color: '#0f62fe'}]}>
              View All
            </Text>
          </TouchableOpacity>
        </View>
        <SizedBox height={24} />

        {TRANSACTIONS.map(e => (
          <TransactionCard
            title={e.title}
            date={e.date}
            stOne={e.stOne}
            stTwo={e.stTwo}
          />
        ))}
      </ScrollView>
    </View>
  );
}

const HEADER_TITLE_FONT_SIZE = 18;
const CARD_TITLE_FONT_SIZE = 15;
const CARD_DESCRIPTION_FONT_SIZE = 12;
const SIMAGE_WIDTH = (screenWidth - 12 * 2 - 16 - 12 * 4) / 2;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 12,
    backgroundColor: '#f4f4f4',
  },
  row: {
    flexDirection: 'row',
  },
  paymentCard: {
    flex: 1,
    height: 230,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardTitle: {
    fontFamily: 'Avenir',
    fontSize: CARD_TITLE_FONT_SIZE,
  },
  cardDescription: {
    fontFamily: 'Avenir',
    fontSize: CARD_DESCRIPTION_FONT_SIZE,
    color: '#6f6f6f',
  },
  subscriptionCard: {
    flex: 1,
    height: 180,
    backgroundColor: 'white',
    padding: 12,
  },
  sImage: {
    alignSelf: 'center',
    width: SIMAGE_WIDTH,
    height: SIMAGE_WIDTH / 1.77,
  },
  transactionCard: {
    width: '100%',
    height: 100.5,
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 20,
  },
  tranContentWrapper: {
    justifyContent: 'space-between',
  },
  tranIconWrapper: {
    width: 50,
    marginRight: 8,
  },
  tranIcon: {
    width: 50,
    height: 50,
  },
  tranInfoWrapper: {
    flex: 1,
  },
  tranStatusWrapper: {
    width: 80,
    marginRight: 8,
    alignItems: 'flex-end',
  },
  tranStOne: {
    fontFamily: 'Helvetica',
  },
  tranStTwo: {
    fontFamily: 'Helvetica',
    color: '#63c47b',
  },
  headerTitle: {
    fontFamily: 'Avenir',
    fontSize: HEADER_TITLE_FONT_SIZE,
  },
  sViewAllBtn: {
    width: '90%',
    height: 40,
    borderWidth: 1,
    borderColor: '#44d7b6',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sViewAllText: {
    fontFamily: 'Avenir',
    color: '#44d7b6',
  },
});
