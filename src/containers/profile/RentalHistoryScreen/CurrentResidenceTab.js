import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {AppIcon} from '/AppStyles';

export default function CurrentResidenceTab() {
  return (
    <View style={styles.container}>
      <Image source={AppIcon.images.artwork} style={styles.coverImage} />
      <Text style={styles.title}>{'Rental History Will Live Here.'}</Text>
      <Text style={styles.description}>
        {
          'As you grow with us, your rental history\nwill appear here! Positive Rental History\nhelps build trust with everyone in the\n3DRS Community and will become part of\n your 3DRS RentScore.'
        }
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  coverImage: {
    width: '100%',
    aspectRatio: 1.41,
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: '#0d0d0d',
    fontWeight: 'bold',
  },
  description: {
    marginTop: 16,
    fontSize: 16,
    textAlign: 'center',
    color: '#0d0d0d',
  },
});
