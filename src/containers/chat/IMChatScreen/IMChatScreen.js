import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Platform,
  Alert,
  BackHandler,
  StyleSheet,
  Text,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import MessageDetails from '../IMChat/MessageDetails';

import LinearGradient from 'react-native-linear-gradient';
import {IMIconButton} from '@Components/truly-native';
import IMChat from '@Containers/chat/IMChat/IMChat';
import {channelManager, firebaseStorage} from '@Firebase';
import {reportingManager} from '@Containers/user-reporting';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {notificationManager} from '@Containers/notifications';
import {TouchableOpacity} from 'react-native-gesture-handler';

class IMChatScreen extends Component {
  static navigationOptions = ({}) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.channel = this.props.navigation.getParam('channel');
    this.appStyles = this.props.navigation.getParam('appStyles');
    this.state = {
      thread: [],
      inputValue: '',
      channel: this.channel,
      downloadUrl: '',
      uploadProgress: 0,
      isMediaViewerOpen: false,
      isRenameDialogVisible: false,
      selectedMediaIndex: null,
    };
    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.groupSettingsActionSheetRef = React.createRef();
    this.privateSettingsActionSheetRef = React.createRef();
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSettingsPress: this.onSettingsPress,
    });
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
    this.threadUnsubscribe = channelManager.subscribeThreadSnapshot(
      this.channel,
      this.onThreadCollectionUpdate,
    );
  }

  componentWillUnmount() {
    this.threadUnsubscribe();
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onSettingsPress = () => {
    if (this.state.channel.participants.length > 1) {
      this.groupSettingsActionSheetRef.current.show();
    } else {
      this.privateSettingsActionSheetRef.current.show();
    }
  };

  onChangeName = text => {
    this.showRenameDialog(false);

    const channel = {...this.state.channel};
    delete channel.participants;
    channel.name = text;

    channelManager.onRenameGroup(
      text,
      channel,
      ({success, error, newChannel}) => {
        if (success) {
          this.setState({channel: newChannel});
          this.props.navigation.setParams({
            channel: newChannel,
          });
        }

        if (error) {
          console.error(error);
        }
      },
    );
  };

  onLeave = () => {
    Alert.alert(
      IMLocalized(`Leave ${this.state.channel.name || 'group'}`),
      IMLocalized('Are you sure you want to leave this group?'),
      [
        {
          text: 'Yes',
          onPress: this.onLeaveDecided,
          style: 'destructive',
        },
        {text: 'No'},
      ],
      {cancelable: false},
    );
  };

  onLeaveDecided = () => {
    channelManager.onLeaveGroup(
      this.state.channel.id,
      this.props.user.id,
      ({success, error}) => {
        if (success) {
          this.props.navigation.goBack(null);
        }

        if (error) {
          console.error(error);
        }
      },
    );
  };

  showRenameDialog = show => {
    this.setState({isRenameDialogVisible: show});
  };

  onThreadCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const message = doc.data();
      data.push({...message, id: doc.id});
    });

    this.setState({thread: data});
  };

  onChangeTextInput = text => {
    this.setState({
      inputValue: text,
    });
  };

  createOne2OneChannel = () => {
    return new Promise(resolve => {
      channelManager
        .createChannel(this.props.user, this.state.channel.participants)
        .then(response => {
          this.setState({channel: response.channel});
          this.threadUnsubscribe = channelManager.subscribeThreadSnapshot(
            response.channel,
            this.onThreadCollectionUpdate,
          );
          resolve(response.channel);
        });
    });
  };

  onSendInput = async () => {
    if (
      this.state.thread.length > 0 ||
      this.state.channel.participants.length > 1
    ) {
      this.sendMessage();
      return;
    }
    // If we don't have a chat id, we need to create it first together with the participations
    this.createOne2OneChannel().then(_response => {
      this.sendMessage();
    });
  };

  sendMessage = () => {
    const inputValue = this.state.inputValue;
    const downloadURL = this.state.downloadUrl;
    this.setState({
      inputValue: '',
      downloadUrl: '',
    });
    channelManager
      .sendMessage(this.props.user, this.state.channel, inputValue, downloadURL)
      .then(response => {
        if (response.error) {
          console.error(response.error);
          this.setState({
            inputValue: inputValue,
            downloadUrl: downloadURL,
          });
        } else {
          this.broadcastPushNotifications(inputValue, downloadURL);
        }
      });
  };

  broadcastPushNotifications = (inputValue, downloadURL) => {
    const channel = this.state.channel;
    const participants = channel.participants;
    if (!participants || participants.length === 0) {
      return;
    }
    const sender = this.props.user;
    const isGroupChat = channel.name && channel.name.length > 0;
    const fromTitle = isGroupChat
      ? channel.name
      : sender.firstName + ' ' + sender.lastName;
    var message;
    if (isGroupChat) {
      if (downloadURL) {
        if (downloadURL.mime && downloadURL.mime.startsWith('video')) {
          message =
            sender.firstName +
            ' ' +
            sender.lastName +
            ' ' +
            IMLocalized('sent a video.');
        } else {
          message =
            sender.firstName +
            ' ' +
            sender.lastName +
            ' ' +
            IMLocalized('sent a photo.');
        }
      } else {
        message = sender.firstName + ' ' + sender.lastName + ': ' + inputValue;
      }
    } else {
      if (downloadURL) {
        if (downloadURL.mime && downloadURL.mime.startsWith('video')) {
          message = sender.firstName + ' ' + IMLocalized('sent you a video.');
        } else {
          message = sender.firstName + ' ' + IMLocalized('sent you a photo.');
        }
      } else {
        message = inputValue;
      }
    }

    participants.forEach(participant => {
      if (participant.id !== this.props.user.id) {
        notificationManager.sendPushNotification(
          participant,
          fromTitle,
          message,
          'chat_message',
          {fromUser: sender},
        );
      }
    });
  };

  onAddMediaPress = photoUploadDialogRef => {
    photoUploadDialogRef.current.show();
  };

  onLaunchCamera = () => {
    const {id, firstName, profilePictureURL} = this.props.user;

    ImagePicker.openCamera({
      cropping: false,
    })
      .then(image => {
        const source = image.path;
        const mime = image.mime;

        const data = {
          content: '',
          created: channelManager.currentTimestamp(),
          senderFirstName: firstName,
          senderID: id,
          senderLastName: '',
          senderProfilePictureURL: profilePictureURL,
          url: 'http://fake',
        };

        this.startUpload({source, mime}, data);
      })
      .catch(function(error) {
        console.log(error);
        this.setState({loading: false});
      });
  };

  onOpenPhotos = () => {
    const {id, firstName, profilePictureURL} = this.props.user;

    ImagePicker.openPicker({
      cropping: false,
      multiple: false,
    })
      .then(image => {
        const source = image.path;
        const mime = image.mime;

        const data = {
          content: '',
          created: channelManager.currentTimestamp(),
          senderFirstName: firstName,
          senderID: id,
          senderLastName: '',
          senderProfilePictureURL: profilePictureURL,
          url: 'http://fake',
        };

        this.startUpload({source, mime}, data);
      })
      .catch(function(error) {
        console.log(error);
        this.setState({loading: false});
      });
  };

  startUpload = ({source, mime}, data) => {
    const filename =
      new Date() + '-' + source.substring(source.lastIndexOf('/') + 1);
    const uploadUri =
      Platform.OS === 'ios' ? source.replace('file://', '') : source;

    firebaseStorage.uploadFileWithProgressTracking(
      filename,
      uploadUri,
      async (snapshot, taskSuccess) => {
        const uploadProgress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.setState({uploadProgress});

        if (snapshot.state === taskSuccess) {
          const url = await snapshot.ref.getDownloadURL();
          this.setState({downloadUrl: {url, mime}, uploadProgress: 0});
          this.onSendInput();
        }
      },
      error => {
        this.setState({uploadProgress: 0});
        Alert.alert(
          IMLocalized('Oops! An error has occured. Please try again.'),
        );
        console.log(error);
      },
    );
  };

  sortMediafromThread = () => {
    this.imagesUrl = [];
    this.images = [];

    this.state.thread.forEach(item => {
      if (item.url && item.url !== '') {
        if (item.url.mime && item.url.mime.startsWith('image')) {
          this.imagesUrl.push(item.url.url);
          this.images.push({
            id: item.id,
            url: item.url,
          });
        } else if (!item.url.mime && item.url.startsWith('https://')) {
          // To handle old format before video feature
          this.imagesUrl.push(item.url);
          this.images.push({
            id: item.id,
            url: item.url,
          });
        }
      }
    });

    return this.imagesUrl;
  };

  onChatMediaPress = item => {
    const index = this.images.findIndex(image => {
      return image.id === item.id;
    });

    this.setState({
      selectedMediaIndex: index,
      isMediaViewerOpen: true,
    });
  };

  onMediaClose = () => {
    this.setState({isMediaViewerOpen: false});
  };

  onUserBlockPress = () => {
    this.reportAbuse('block');
  };

  onUserReportPress = () => {
    this.reportAbuse('report');
  };

  reportAbuse = type => {
    const participants = this.state.channel.participants;
    if (!participants || participants.length !== 1) {
      return;
    }
    const myID = this.props.user.id || this.props.user.userID;
    const otherUserID = participants[0].id || participants[0].userID;
    reportingManager.markAbuse(myID, otherUserID, type).then(response => {
      if (!response.error) {
        this.props.navigation.goBack(null);
      }
    });
  };

  render() {
    return (
      <View style={styles.screen}>
        <LinearGradient
          style={styles.header}
          colors={['#009aca', '#069dc9', '#aeefaa']}
          locations={[0.1, 0.2, 1]}>
          <Text style={styles.messageDetails}>Message Details</Text>
        </LinearGradient>
        <IMChat
          appStyles={this.appStyles}
          user={this.props.user}
          thread={this.state.thread}
          inputValue={this.state.inputValue}
          onAddMediaPress={this.onAddMediaPress}
          onSendInput={this.onSendInput}
          onChangeTextInput={this.onChangeTextInput}
          onLaunchCamera={this.onLaunchCamera}
          onOpenPhotos={this.onOpenPhotos}
          uploadProgress={this.state.uploadProgress}
          sortMediafromThread={this.sortMediafromThread()}
          isMediaViewerOpen={this.state.isMediaViewerOpen}
          selectedMediaIndex={this.state.selectedMediaIndex}
          onChatMediaPress={this.onChatMediaPress}
          onMediaClose={this.onMediaClose}
          isRenameDialogVisible={this.state.isRenameDialogVisible}
          groupSettingsActionSheetRef={this.groupSettingsActionSheetRef}
          privateSettingsActionSheetRef={this.privateSettingsActionSheetRef}
          showRenameDialog={this.showRenameDialog}
          onChangeName={this.onChangeName}
          onLeave={this.onLeave}
          onUserBlockPress={this.onUserBlockPress}
          onUserReportPress={this.onUserReportPress}
          navigation={this.props.navigation}
          channel={this.state.channel}
        />
      </View>
    );
  }
}

IMChatScreen.propTypes = {
  thread: PropTypes.array,
  setChatThread: PropTypes.func,
  createThread: PropTypes.func,
  createChannel: PropTypes.func,
  user: PropTypes.object,
};

const mapStateToProps = ({chat, auth}) => {
  return {
    user: auth.user,
    thread: chat.thread,
  };
};

const styles = StyleSheet.create({
  screen: {
    height: '100%',
  },
  // header: {
  //   height: '19%',
  //   width: '100%',
  //   position: 'relative',
  //   alignItems: 'center',
  //   paddingTop: 55,
  // },
  // messageDetails: {
  //   color: 'white',
  //   fontFamily: 'Avenir',
  //   fontSize: 20,
  //   fontWeight: 'bold',
  // },
  header: {
    height: '19%',
    width: '100%',
    position: 'relative',
    alignItems: 'center',
    paddingTop: 55,
  },
  messageDetails: {
    color: 'white',
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default connect(mapStateToProps)(IMChatScreen);
