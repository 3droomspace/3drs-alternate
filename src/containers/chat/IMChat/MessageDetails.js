import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image';

const MessageDetails = props => {
  const {participants} = props.channel;
  if (participants === undefined) {
    return <View />;
  }

  const defaultAvatar =
    'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

  let receiverProfilePicture = defaultAvatar;

  let isOnline = true;
  if (participants[0] == undefined) {
    isOnline = false;
  } else if (participants[0].isOnline == false) {
    isOnline = false;
  }

  let firstAvatarUri =
    participants.length > 0 &&
    participants[0].profilePictureURL &&
    participants[0].profilePictureURL.length > 0
      ? participants[0].profilePictureURL
      : defaultAvatar;
  let title = props.channel.name;

  if (props.channel.name) {
    title = props.channel.name;
  } else if (props.channel.participants[0]) {
    title =
      props.channel.participants[0].firstName +
      ' ' +
      props.channel.participants[0].lastName;
  }

  return (
    <TouchableOpacity onPress={props.onPress} style={styles.modal}>
      <View>
        <FastImage
          style={styles.profilePicture}
          source={{uri: `${firstAvatarUri}`}}
        />
        {isOnline && <View style={styles.onlineMark} />}
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.chatName}>{title}</Text>
        <Text style={styles.propertyName}>
          {props.channel.propertyName && props.channel.propertyName}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  modal: {
    position: 'absolute',
    zIndex: 999,
    height: 90,
    width: 380,
    top: -70,
    left: 16,
    backgroundColor: 'white',
    borderRadius: 8,
    alignItems: 'center',
    shadowColor: 'black',
    shadowOffset: {
      height: 2,
    },
    shadowRadius: 10,
    shadowOpacity: 0.3,
    flexDirection: 'row',
    paddingLeft: '15%',
  },
  chatName: {
    fontSize: 18,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
  },
  profilePicture: {
    height: 50,
    width: 50,
    marginRight: 30,
    borderRadius: 50,
  },
  propertyName: {
    color: 'grey',
    fontFamily: 'Avenir',
    fontSize: 15,
  },
  onlineMark: {
    position: 'absolute',
    backgroundColor: '#80ea4e',
    height: 21,
    width: 21,
    borderRadius: 18,
    borderWidth: 3,
    borderColor: 'white',
    left: 35,
    bottom: 1,
  },
});

export default MessageDetails;
