import * as CONSTANTS from './constants';

const initialState = {
  channels: [],
  areChannelsSubcribed: false,
  channelParticipations: [],
};

export const chat = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.SET_CHANNELS_SUBCRIBED:
      return {
        ...state,
        areChannelsSubcribed: action.data,
      };

    case CONSTANTS.SET_CHANNELS:
      return {
        ...state,
        channels: [...action.data],
      };

    case CONSTANTS.SET_CHANNELS_PARTICIPATIONS:
      return {
        ...state,
        channelParticipations: [...action.data],
      };

    default:
      return state;
  }
};
