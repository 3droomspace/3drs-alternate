import * as CONSTANTS from './constants';

export const setChannels = data => ({
  type: CONSTANTS.SET_CHANNELS,
  data,
});

export const setChannelParticipations = data => ({
  type: CONSTANTS.SET_CHANNELS_PARTICIPATIONS,
  data,
});

export const setChannelsSubcribed = data => ({
  type: CONSTANTS.SET_CHANNELS_SUBCRIBED,
  data,
});
