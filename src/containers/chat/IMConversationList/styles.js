import {DynamicStyleSheet} from 'react-native-dark-mode';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    scrollContainer: {
      height: '100%',
      flex: 1,
      paddingLeft: 10,
      paddingTop: 10,
    },
    userImageContainer: {
      borderWidth: 0,
    },
    chatsChannelContainer: {
      // flex: 1,
      padding: 0,
    },
    chatItemContainer: {
      flexDirection: 'row',
      marginBottom: 20,
    },
    chatItemContent: {
      flex: 1,
      alignSelf: 'center',
      marginLeft: 10,
    },
    chatFriendName: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: 17,
    },
    content: {
      flexDirection: 'row',
    },
    message: {
      flex: 2,
      color: appStyles.colorSet.mainSubtextColor,
    },
    emptyViewContainer: {
      marginTop: height / 5,
      alignItems: 'center',
    },
    imageContainer: {
      // borderWidth: 1,
      height: 250,
      width: 300,
    },
    noChatImage: {
      height: 180,
      width: 280,
      marginLeft: 15,
    },
    emptyText: {
      textAlign: 'center',
      width: '85%',
      fontSize: 15,
      fontFamily: 'Avenir',
    },
    emptyTitle: {
      fontSize: 18,
      marginBottom: 15,
      fontWeight: 'bold',
      fontFamily: 'Avenir',
    },
  });
};

export default dynamicStyles;
