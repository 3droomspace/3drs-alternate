import React from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  View,
  FlatList,
  ActivityIndicator,
  Text,
  Image,
} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';

import IMConversationView from '@Containers/chat/IMConversationView';
import dynamicStyles from './styles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {TNEmptyStateView} from '@Components/truly-native';
import MessageSearchBar from '@Components/MessageSearchBar';

function IMConversationList(props) {
  const {
    onConversationPress,
    emptyStateConfig,
    conversations,
    loading,
    appStyles,
  } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const formatMessage = item => {
    if (
      item.lastMessage &&
      item.lastMessage.mime &&
      item.lastMessage.mime.startsWith('video')
    ) {
      return IMLocalized('Someone sent a video.');
    } else if (
      item.lastMessage &&
      item.lastMessage.mime &&
      item.lastMessage.mime.startsWith('image')
    ) {
      return IMLocalized('Someone sent a photo.');
    } else if (item.lastMessage) {
      return item.lastMessage;
    }
    return '';
  };

  const renderConversationView = ({item}) => (
    <IMConversationView
      formatMessage={formatMessage}
      onChatItemPress={onConversationPress}
      item={item}
      appStyles={appStyles}
    />
  );

  let searchConversations = conversations;

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={{marginTop: 15}} size="small" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <MessageSearchBar conversations={searchConversations} />
      <View style={styles.scrollContainer}>
        <ScrollView
          style={styles.container}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.chatsChannelContainer}>
            {conversations && conversations.length > 0 && (
              <FlatList
                vertical={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={conversations}
                renderItem={renderConversationView}
                keyExtractor={item => `${item.id}`}
              />
            )}
            {conversations && conversations.length <= 0 && (
              <View style={styles.emptyViewContainer}>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.noChatImage}
                    source={require('../assets/meditate.png')}
                  />
                </View>
                <Text style={styles.emptyTitle}>Your inbox is Empty</Text>
                <Text style={styles.emptyText}>
                  That could be a good thing or a bad thing depending on how you
                  look at it
                </Text>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

IMConversationList.propTypes = {
  onConversationPress: PropTypes.func,
  conversations: PropTypes.array,
};

export default IMConversationList;
