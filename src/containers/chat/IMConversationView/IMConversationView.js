import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {useDynamicStyleSheet} from 'react-native-dark-mode';

import IMConversationIconView from './IMConversationIconView/IMConversationIconView';
import {timeFormat} from '@Helpers/timeFormat';
import dynamicStyles from './styles';

function IMConversationView(props) {
  const {onChatItemPress, formatMessage, item, appStyles} = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const [timeFrom, timeFromManager] = useState();

  let title = item.name;
  if (!title) {
    if (item.participants.length > 0) {
      let friend = item.participants[0];
      title = friend.firstName + ' ' + friend.lastName;
    }
  }

  const toDateTime = secs => {
    var t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
  };

  const lastMessageTime = () => {
    if (item.lastMessageDate === null) {
      return;
    }
    const now = Date.now() / 1000;
    const secs = item.lastMessageDate._seconds;
    const timeBetween = now - secs;
    const min = timeBetween / 60;
    const hour = min / 60;
    const day = hour / 24;
    const month = day / 30;

    if (timeBetween < 3600) {
      let answer = min.toString().split('.');
      if (answer[0] === '1') {
        return answer[0] + ' minute';
      } else {
        return answer[0] + ' minutes';
      }
    } else if (timeBetween < 86400) {
      let answer = hour.toString().split('.');
      if (answer[0] === '1') {
        return answer[0] + 'hour';
      } else {
        return answer[0] + ' hours';
      }
    } else if (timeBetween < 2592000) {
      let answer = day.toString().split('.');
      if (answer[0] === '1') {
        return answer[0] + ' day';
      } else {
        return answer[0] + ' days';
      }
    } else {
      let answer = month.toString().split('.');
      if (answer[0] === '1') {
        return answer[0] + ' month';
      } else {
        return answer[0] + ' month';
      }
    }
  };

  return (
    <TouchableOpacity
      onPress={() => onChatItemPress(item)}
      style={styles.chatItemContainer}>
      <IMConversationIconView
        participants={item.participants}
        appStyles={appStyles}
      />
      <View style={styles.chatItemContent}>
        <View style={styles.title}>
          <Text style={styles.chatFriendName}>{title}</Text>
          <Text style={{fontFamily: 'Avenir-Book'}}>{lastMessageTime()}</Text>
        </View>
        <View style={styles.content}>
          <Text
            numberOfLines={1}
            ellipsizeMode={'middle'}
            style={styles.message}>
            {formatMessage(item)} {''}
            {/* {timeFormat(item.lastMessageDate)} */}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

IMConversationView.propTypes = {
  formatMessage: PropTypes.func,
  item: PropTypes.object,
  onChatItemPress: PropTypes.func,
};

export default IMConversationView;
