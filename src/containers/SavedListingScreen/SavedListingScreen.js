import React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';

import {AppStyles, TwoColumnListStyle} from '/AppStyles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FastImage from 'react-native-fast-image';

import SavedButton from '@Components/SavedButton';
import DynamicAppStyles from '/DynamicAppStyles';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import {firebaseListing} from '@Firebase';

class SavedListingScreen extends React.Component {
  static navigationOptions = ({screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('Favorites'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      listings: [],
      savedListings: [],
      loading: false,
      error: null,
      refreshing: false,
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.savedListingsUnsubscribe = firebaseListing.subscribeSavedListings(
      this.props.user.id,
      this.onSavedListingsCollectionUpdate,
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }

    if (this.savedListingsUnsubscribe) {
      this.savedListingsUnsubscribe();
    }

    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onSavedListingsCollectionUpdate = querySnapshot => {
    let savedListingdata = [];
    const savedListingList = [];
    querySnapshot.forEach(doc => {
      const savedListing = doc.data();
      savedListingList.push(savedListing);
    });

    savedListingList.sort((al, bl) => {
      const alSaved = al.saved ? al.saved._seconds : 0;
      const blSaved = bl.saved ? bl.saved._seconds : 0;
      return alSaved - blSaved;
    });

    savedListingdata = savedListingList.map(sl => sl.listingID);

    if (this.listingsUnsubscribe) {
      this.listingsUnsubscribe();
    }
    this.listingsUnsubscribe = firebaseListing.subscribeListings(
      {},
      this.onListingsCollectionUpdate,
    );

    this.setState({
      savedListings: savedListingdata,
      loading: false,
    });
  };

  onListingsCollectionUpdate = querySnapshot => {
    const fullList = [];
    querySnapshot.forEach(doc => {
      const listing = doc.data();
      fullList.push({...listing, id: doc.id});
    });

    const data = this.state.savedListings.map(sl => ({
      ...fullList.find(listing => listing.id === sl),
      saved: true,
    }));

    this.setState({
      listings: data,
      loading: false,
    });
  };

  onPressListingItem = item => {
    this.props.navigation.navigate('Detail', {item});
  };

  onPressSavedIcon = item => {
    firebaseListing.saveUnsaveListing(item, this.props.user.id);
  };

  renderListingItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => this.onPressListingItem(item)}
        onLongPress={() => this.onLongPressListingItem(item)}
        style={[
          TwoColumnListStyle.mainConatiner,
          {
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 3,
          },
        ]}>
        <View
          style={[
            TwoColumnListStyle.listingItemContainer1,
            {borderRadius: 20, overflow: 'hidden', alignSelf: 'center'},
          ]}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto1}
            source={{uri: item.photo}}
          />
          <SavedButton
            style={TwoColumnListStyle.savedIcon1}
            onPress={() => this.onPressSavedIcon(item)}
            item={item}
          />
          <View style={{paddingLeft: 10, paddingBottom: 10}}>
            <Text style={TwoColumnListStyle.dollar}>
              <Text style={{fontSize: 13, color: '#737373'}}>$ </Text>
              {item.price}
              <Text style={{fontSize: 11, color: '#777677'}}> per month.</Text>
            </Text>
            <Text
              style={{
                ...TwoColumnListStyle.listingName,
                maxHeight: 40,
                marginTop: 4,
              }}>
              {item.title}
            </Text>
            <Text style={TwoColumnListStyle.listingPlace1}>
              {`${
                item.address?.street_number
                  ? item.address?.street_number.long_name
                  : ''
              } ${
                item.address?.street ? item.address?.street.long_name : ''
              }, ${item.address?.city ? item.address?.city.long_name : ''}, ${
                item.address?.county ? item.address?.county.long_name : ''
              }, ${
                item.address?.state ? item.address?.state.short_name : ''
              }, ${
                item.address?.country ? item.address?.country.short_name : ''
              }`}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <View
                style={[TwoColumnListStyle.blueContainer, {marginRight: 10}]}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  {item.filters?.Bedrooms === 'Any'
                    ? 1
                    : item.filters?.Bedrooms}{' '}
                  Bed,{' '}
                  {item.filters?.Bathrooms === 'Any'
                    ? 1
                    : item.filters?.Bathrooms}{' '}
                  Bath
                </Text>
              </View>
              <View style={TwoColumnListStyle.blueContainer}>
                <Text style={TwoColumnListStyle.listingPlace}>
                  <Icon name="star" size={12} color="#324cf2" /> 4.6
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.listings.length > 0 ? (
          <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            data={this.state.listings}
            renderItem={this.renderListingItem}
            keyExtractor={item => `${item.id}`}
            contentContainerStyle={{paddingTop: 20}}
            ListEmptyComponent={
              <Text style={styles.placeholder}>
                {'All your saved or favorite listings here'}
              </Text>
            }
          />
        ) : (
          <View style={styles.placeholderWrapper}>
            <Text style={styles.placeholder}>
              {'All your saved or favorite listings here'}
            </Text>
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  rightButton: {
    marginRight: 10,
    color: AppStyles.color.main,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
  starStyle: {
    tintColor: DynamicAppStyles.colorSet.mainThemeForegroundColor,
  },
  placeholder: {
    alignSelf: 'center',
    fontSize: 18,
    fontFamily: 'Avenir',
  },
  placeholderWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(SavedListingScreen);
