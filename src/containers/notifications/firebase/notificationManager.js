import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

const notificationsRef = firebase.firestore().collection('notifications');

const fcmURL = 'https://fcm.googleapis.com/fcm/send';
const firebaseServerKey =
  'AAAAQfLnWt4:APA91bFXUv9AOZcviTxS2y5IxmTmDthEJsa2uZ9_UqBDIlDnwpP1qjapZwdsQqS0SCZbKszhPbD8eNSgRFGapNPbtV12T3kQPkvUHoeotjU2z6ddiQMPxbCmhLS7IIXbkIhM1uNcbz8f';

const sendPushNotification = async (
  toUser,
  title,
  body,
  type,
  metadata = {},
) => {
  if (metadata && metadata.fromUser && toUser.id === metadata.fromUser.id) {
    return;
  }
  const notification = {
    toUserID: toUser.id,
    title,
    body,
    metadata,
    toUser,
    type,
    seen: false,
  };

  const ref = await notificationsRef.add({
    ...notification,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
  });
  notificationsRef.doc(ref.id).update({id: ref.id});
  if (!toUser.pushToken) {
    return;
  }

  const pushNotification = {
    to: toUser.pushToken,
    notification: {
      title: title,
      body: body,
    },
    data: {...metadata, type, toUserID: toUser.id},
  };
  const fcmFetch = fetch(fcmURL, {
    method: 'post',
    headers: {
      Authorization: 'key=' + firebaseServerKey,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(pushNotification),
  });

  fcmFetch.then(c => c.json()).then(console.log);
};

export const notificationManager = {
  sendPushNotification,
};
