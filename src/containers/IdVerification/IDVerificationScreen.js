import React, {Component} from 'react';
import {
  Alert,
  BackHandler,
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import authManager from '@Containers/onboarding/utils/authManager';
import DynamicAppStyles from '/DynamicAppStyles';
import ListingAppConfig from '/ListingAppConfig';
import {logout, setUserData} from '@Containers/onboarding/redux';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import GradientButton from '@Components/Buttons/GradientButton';
import {UserPhotoSelector, IDPictureSelector} from '@Components/truly-native';
import idVerificationManager from '../../idVerification';
import sendGridManager from '../../sendGrid';
import Icon from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    // bottom: 0,
    left: 16,
    right: 16,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginHorizontal: 24,
    justifyContent: 'space-between',
    marginTop: 20,
  },
  subtile: {
    fontSize: 20,
    margin: 15,
    marginTop: 20,
  },
  note: {
    margin: 10,
    lineHeight: 20,
    fontSize: 15,
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonLabel: {
    fontSize: 18,
    marginLeft: -5,
    marginTop: -2,
  },
});

var UploadIDPicture = '';
var UploadUserPhoto = '';

class IDVerificationScreen extends Component {
  static navigationOptions = ({navigation, screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];
    return {
      title: IMLocalized('ID Verification'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {
        color: currentTheme.fontColor,
        flex: 1,
        textAlign: 'center',
        marginLeft: -10,
      },
      headerLeft: (
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}>
          <Icon
            name={'chevron-left'}
            size={32}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.backButtonLabel}>{'Back'}</Text>
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    // this.state = {
    //   changePwd: false,
    // };
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  updateUserPictureURL = photoURI => {
    if (photoURI == null) {
      return;
    }
    // If we have a photo, we upload it to Firebase, and then update the user
    UploadUserPhoto = photoURI;
  };

  updateIDPictureURL = photoURI => {
    if (photoURI == null) {
      return;
    }
    // If we have a photo, we upload it to Firebase, and then update the user
    UploadIDPicture = photoURI;
  };

  onSubmit = () => {
    if (UploadUserPhoto == '' || UploadIDPicture == '') {
      Alert.alert('Please upload ID and User images correctly!');
    } else {
      idVerificationManager
        .idVerification(
          UploadUserPhoto,
          UploadIDPicture,
          this.props.user.firstName,
          this.props.user.lastName,
          this.props.user.userID,
          this.props.user.userID,
          this.props.user.email,
        )
        .then(response => {
          console.log('onSubmit -> response', response);
          if (response.error) {
            Alert.alert(
              'SORRY!.  ID CANNOT BE VERIFIED.',
              ' PLEASE CONTACT TO OUR CUSTOMER SUCCESS TEAM.',
            );
          } else {
            Alert.alert('ID VERIFICATION SUCCESSFULLY!');
            sendGridManager.VerifycationConfirmation(this.props.user.email);
            this.props.navigation.navigate('MainStack', {
              user: this.props.user,
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  onCancel = () => {
    this.props.navigation.goBack();
  };

  render() {
    console.log(this.props.user);
    const {
      profilePictureURL,
      firstName,
      lastName,
      fullname,
      userID,
    } = this.props.user;
    const IDURL =
      'https://cdn4.vectorstock.com/i/1000x1000/37/68/id-card-user-logo-icon-design-vector-22953768.jpg';
    const USERURL = profilePictureURL;
    return (
      <ScrollView>
        <View style={{flex: 1, marginBottom: 16}}>
          <View>
            <Text style={styles.subtile}>ID PICTURE: </Text>
            <IDPictureSelector
              setIDPictureURL={this.updateIDPictureURL}
              appStyles={DynamicAppStyles}
              IDPictureURL={IDURL}
            />
            <Text style={styles.note}>
              NOTE: PLEASE SCAN FRONT OF YOUR GOVT. DL OR PASSPORT.
            </Text>
          </View>

          <View>
            <Text style={styles.subtile}>USER PHOTO: </Text>
            <UserPhotoSelector
              setUserPictureURL={this.updateUserPictureURL}
              appStyles={DynamicAppStyles}
              UserPictureURL={USERURL}
            />
          </View>
          <Text style={styles.note}>
            {' '}
            NOTE: PLEASE TAKE A PHOTOGRAPH OPEN MOUTH TO VERIFY USER PRESENCE.
          </Text>
          <View style={styles.buttonContainer}>
            <GradientButton
              style={{width: 160}}
              color="blueGreen"
              onPress={this.onSubmit}>
              SUBMIT
            </GradientButton>
            <GradientButton
              style={{width: 160, paddingHorizontal: 10}}
              color="green"
              onPress={this.onCancel}>
              CANCEL
            </GradientButton>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {
    user: auth.user,
    isAdmin: auth.user && auth.user.isAdmin,
  };
};

export default connect(mapStateToProps, {
  logout,
  setUserData,
})(IDVerificationScreen);
