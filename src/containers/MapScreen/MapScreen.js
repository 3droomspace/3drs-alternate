import React from 'react';
import {
  StyleSheet,
  BackHandler,
  View,
  Text,
  Alert,
  TextInput,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import MapView, {Marker, Callout} from 'react-native-maps';
import FastImage from 'react-native-fast-image';
import Geolocation from '@react-native-community/geolocation';
import LinearGradient from 'react-native-linear-gradient';

import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/AntDesign';
import {firebaseListing} from '@Firebase';
import {setFilter} from '@Redux/app';
import {AppStyles, CustomMapStyle} from '/AppStyles';
import {Configuration} from '/Configuration';
import {IMLocalized} from '@Containers/localization/IMLocalization';
import DynamicAppStyles from '/DynamicAppStyles';
import HeaderButton from '@Components/HeaderButton';
import GPSButton from '@Components/GPSButton';
import FilterViewModal from '@Components/FilterViewModal';
import LocationSelect from '@Components/LocationSelect';
import PostModal from '@Components/PostModal';

import {filterLocation, filterItem} from '../../helpers/Filters';
import {FontIcon} from '@Helpers/FontAwesome';
import {isIphoneX} from 'react-native-iphone-x-helper';

class MapScreen extends React.Component {
  static navigationOptions = ({navigation, screenProps}) => {
    let currentTheme = DynamicAppStyles.navThemeConstants[screenProps.theme];

    return {
      title: IMLocalized('Map View'),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: {color: currentTheme.fontColor},
      header: null,
      showLocationSelect: false,
    };
  };

  constructor(props) {
    super(props);

    this.unsubscribe = null;

    this.state = {
      data: [],
      latitude: Configuration.map.origin.latitude,
      longitude: Configuration.map.origin.longitude,
      latitudeDelta: Configuration.map.delta.latitude,
      longitudeDelta: Configuration.map.delta.longitude,
      refreshing: false,
      showSearch: true,
      filterModalVisible: false,
      filter: props.filter,
      shouldUseOwnLocation: false, // Set this to false if you don't want the current user's location to be considered
      postModalVisible: false,
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  //  handlePress = () => {
  //    this.setState({
  //      showSearch: true
  //    })
  //  }

  onChangeLocation = location => {
    this.setState({
      latitude: location.latitude,
      longitude: location.longitude,
    });
  };

  componentDidMount() {
    this.loadItems();

    if (this.state.shouldUseOwnLocation) {
      Geolocation.getCurrentPosition(
        position => {
          this.onChangeLocation(position.coords);
        },
        error => console.log(error.message),
        {enableHighAccuracy: false, timeout: 5000},
      );
    }

    this.props.navigation.setParams({
      showFilter: this.showFilter,
    });

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      ...prevState,
      filter: nextProps.filter,
      location: nextProps.location,
    };
  }

  loadItems = () => {
    this.unsubscribe = firebaseListing.subscribeListings(
      {},
      this.onCollectionUpdate,
    );
  };

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  showFilter = () => {
    this.setState({
      filterModalVisible: true,
    });
  };

  onSelectFilterCancel = () => {
    this.setState({filterModalVisible: false});
  };

  onSelectFilterDone = filter => {
    this.setState({filter});
    this.setState({filterModalVisible: false});

    this.props.setFilter(filter);
    this.loadItems();
  };

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onCollectionUpdate = querySnapshot => {
    const data = [];
    let max_latitude = -90;
    let min_latitude = 90;
    let max_longitude = -180;
    let min_longitude = 180;
    if (this.props.location == null) {
      return;
    }
    // const {addressComponents} = this.props.location
    //   ? this.props.location
    //   : 'San Francisco, CA';
    const {filter} = this.state;

    querySnapshot.forEach(doc => {
      const listing = doc.data();

      if (!listing.address || !listing.address.location) {
        return;
      }

      if (!filterItem(listing, filter)) {
        return;
      }
      // const isInLocation = filterLocation(listing.address, addressComponents);
      // if (!isInLocation) {
      //   return;
      // }

      const {coordinates} = listing.address.location;
      if (max_latitude < coordinates.latitude) {
        max_latitude = coordinates.latitude;
      }
      if (min_latitude > coordinates.latitude) {
        min_latitude = coordinates.latitude;
      }
      if (max_longitude < coordinates.longitude) {
        max_longitude = coordinates.longitude;
      }
      if (min_longitude > coordinates.longitude) {
        min_longitude = coordinates.longitude;
      }

      data.push({...listing, id: doc.id});
    });

    this.setState({
      data,
    });

    if (!this.state.shouldUseOwnLocation || !this.state.latitude) {
      if (data.length === 0) {
        const {bounds} = this.props.location.geometry;
        max_latitude = bounds.northeast.lat;
        max_longitude = bounds.northeast.lng;
        min_latitude = bounds.southwest.lat;
        min_longitude = bounds.southwest.lng;
      }

      let latitude = (max_latitude + min_latitude) / 2;
      let longitude = (max_longitude + min_longitude) / 2;
      let latitudeDelta = Math.max(Math.abs(max_latitude - min_latitude), 1);
      let longitudeDelta = Math.max(Math.abs(max_longitude - min_longitude), 1);

      this.setState({
        latitude,
        longitude,
        latitudeDelta,
        longitudeDelta,
      });
    }
  };

  onPress = item => {
    this.props.navigation.navigate('Detail', {
      item: item,
      customLeft: true,
      headerLeft: null,
      routeName: 'Map',
    });
  };

  getMapRegion = () => ({
    latitude: parseFloat(this.state.latitude),
    longitude: parseFloat(this.state.longitude),
    latitudeDelta: this.state.latitudeDelta,
    longitudeDelta: this.state.longitudeDelta,
  });

  getPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        this.onChangeLocation(position.coords);
      },
      error => Alert.alert(error.message),
      {
        enableHighAccuracy: false,
        timeout: 1000,
      },
    );
  };

  openListing = () => {
    this.props.navigation.navigate('Landing');
  };

  toggleLocationSelect = e => {
    this.loadItems();

    this.setState({
      showLocationSelect: !this.state.showLocationSelect,
    });
  };

  onFocusMap = () => {
    const currentLocation = {
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      latitudeDelta: this.state.latitudeDelta,
      longitudeDelta: this.state.longitudeDelta,
    };

    this.mapView.animateToRegion(currentLocation, 1000);
  };

  onPostCancel = () => {
    this.setState({postModalVisible: false});
  };

  onAddListing = () => {
    this.setState({
      postModalVisible: true,
    });
  };

  render() {
    const {location} = this.props;

    const markerArr = this.state.data.map((listing, index) => {
      return (
        <Marker
          title={listing.title}
          description={listing.description}
          onCalloutPress={() => this.onPress(listing)}
          coordinate={{
            latitude: parseFloat(listing.address.location.coordinates.latitude),
            longitude: parseFloat(
              listing.address.location.coordinates.longitude,
            ),
          }}
          style={{...styles.marker, zIndex: index}}>
          <View style={styles.markerView}>
            <Text style={styles.markerText}>${listing.price}</Text>
          </View>
          <Callout
            style={styles.calloutView}
            onPress={() => this.onPress(listing)}>
            <View style={styles.calloutImageView}>
              <FastImage
                source={{uri: listing.photo}}
                resizeMode="cover"
                style={styles.calloutImage}
              />
            </View>
            <View style={styles.calloutTextView}>
              <Text style={styles.calloutPriceText}>${listing.price}</Text>
              <Text style={styles.calloutDetailText}>
                {listing.filters.Bedrooms} bd, {listing.filters.Bathrooms} ba
              </Text>
              <Text style={styles.calloutDetailText}>
                {listing.filters['Square Feet']} sqft
              </Text>
            </View>
          </Callout>
        </Marker>
      );
    });

    // * Removed custom map style as it's slowing the performance
    return (
      <View>
        <LinearGradient
          style={styles.header}
          colors={['#009aca', '#069dc9', '#aeefaa']}
          locations={[0.1, 0.2, 1]}>
          <TouchableOpacity onPress={this.showFilter}>
            <Icon2 name="filter" size={25} color="white" />
          </TouchableOpacity>

          <View style={styles.searchBox}>
            <Icon name="search" size={25} color="#3a3a3a" />
            <TextInput
              placeholder="City, zip, or neighborhood"
              placeholderTextColor="#069dc9"
              onFocus={this.toggleLocationSelect}
              style={styles.locationInput}
              defaultValue={location?.address}
            />
          </View>

          <TouchableOpacity onPress={this.openListing}>
            <Icon2 name="list-ul" size={25} color="white" />
          </TouchableOpacity>
        </LinearGradient>

        <MapView
          ref={ref => (this.mapView = ref)}
          style={styles.mapView}
          showsUserLocation={true}
          region={this.getMapRegion()}>
          {markerArr}
        </MapView>

        <TouchableOpacity
          style={styles.plusButtonContaienr}
          onPress={this.onAddListing}>
          <LinearGradient
            colors={['#009aca', '#069dc9', '#aeefaa']}
            locations={[0, 0.1, 1]}
            style={styles.plusStyle}>
            <Icon3 name="plus" size={30} color="white" />
          </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.locationButtonContaienr}
          onPress={this.onFocusMap}>
          <LinearGradient
            colors={['#009aca', '#069dc9', '#aeefaa']}
            locations={[0, 0.1, 1]}
            style={[styles.plusStyle, {width: 40, height: 40}]}>
            <Icon2 name="location-arrow" size={23} color="white" />
          </LinearGradient>
        </TouchableOpacity>

        {this.state.filterModalVisible && (
          <FilterViewModal
            value={this.state.filter}
            onCancel={this.onSelectFilterCancel}
            onDone={this.onSelectFilterDone}
          />
        )}

        {this.state.showLocationSelect && (
          <LocationSelect hideModal={this.toggleLocationSelect} />
        )}

        <GPSButton getPosition={this.getPosition} />
        {this.state.postModalVisible && (
          <PostModal
            onCancel={this.onPostCancel}
            navigation={this.props.navigation}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  plusButtonContaienr: {
    position: 'absolute',
    top: isIphoneX() ? 120 : 100,
    right: 25,
  },
  locationButtonContaienr: {
    position: 'absolute',
    bottom: isIphoneX() ? 160 : 140,
    right: 25,
  },
  plusStyle: {
    width: 50,
    height: 50,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mapView: {
    width: '100%',
    height: '100%',
    backgroundColor: AppStyles.color.grey,
  },
  header: {
    height: isIphoneX() ? 100 : 76,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: isIphoneX() ? 24 : 0,
  },
  searchBox: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 20,
  },
  locationInput: {
    padding: 0,
    paddingLeft: 4,
  },
  headerIcon: {
    marginRight: 13,
    marginLeft: 7,
  },
  currentPosition: {
    position: 'absolute',
    fontSize: 20,
    padding: 10,
    bottom: 0,
    right: 0,
    backgroundColor: 'white',
    margin: 10,
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 0.5,
  },
  marker: {
    position: 'absolute',
    backgroundColor: 'transparent',
  },
  markerView: {
    backgroundColor: AppStyles.color.background,
    paddingVertical: 2,
    paddingHorizontal: 8,
    borderRadius: 10,
    borderWidth: 0.3,
    borderColor: 'rgba(0, 0, 0, 0.5)',
  },
  markerText: {
    color: 'black',
  },
  calloutView: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  calloutImage: {
    width: 50,
    height: 50,
  },
  calloutTextView: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    minWidth: 80,
    marginLeft: 10,
  },
  calloutPriceText: {
    fontWeight: 'bold',
  },
  calloutDetailText: {},
});

const mapStateToProps = state => ({
  user: state.auth.user,
  location: state.app.location,
  startDate: state.app.startDate,
  endDate: state.app.endDate,
  filter: state.app.filter,
});

const mapDispatchToProps = {
  setFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen);
