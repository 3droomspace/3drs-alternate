import * as CONSTANTS from './constants';

const initialState = {
  notifications: [],
  mode: 'light',
  location: null,
  startDate: null,
  endDate: null,
  filter: {},
  userType: 'renter',
};

export const app = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.SET_USER_TYPE:
      return {
        ...state,
        userType: action.data,
      };
    case CONSTANTS.SET_MODE:
      return {
        ...state,
        mode: action.data,
      };
    case CONSTANTS.SET_NOTIFICATIONS:
      return {
        ...state,
        notifications: [...action.data],
      };
    case CONSTANTS.SET_LOCATION:
      return {
        ...state,
        location: action.data,
      };
    case CONSTANTS.SET_START_DATE:
      return {
        ...state,
        startDate: action.data,
      };
    case CONSTANTS.SET_END_DATE:
      return {
        ...state,
        endDate: action.data,
      };
    case CONSTANTS.SET_FILTER:
      return {
        ...state,
        filter: action.data,
      };
    default:
      return state;
  }
};
