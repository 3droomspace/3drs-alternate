export const SET_MODE = 'App/SET_MODE';
export const SET_NOTIFICATIONS = 'App/SET_NOTIFICATIONS';
export const SET_LOCATION = 'App/SET_LOCATION';
export const SET_START_DATE = 'App/SET_START_DATE';
export const SET_END_DATE = 'App/SET_END_DATE';
export const SET_FILTER = 'App/SET_FILTER';
export const SET_USER_TYPE = 'App/SET_USER_TYPE';
