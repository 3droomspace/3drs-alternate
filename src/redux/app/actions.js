import * as CONSTANTS from './constants';

export const setMode = data => ({
  type: CONSTANTS.SET_MODE,
  data,
});

export const setNotifications = data => ({
  type: CONSTANTS.SET_NOTIFICATIONS,
  data,
});

export const setLocation = data => ({
  type: CONSTANTS.SET_LOCATION,
  data,
});

export const setStartDate = data => ({
  type: CONSTANTS.SET_START_DATE,
  data,
});

export const setEndDate = data => ({
  type: CONSTANTS.SET_END_DATE,
  data,
});

export const setFilter = data => ({
  type: CONSTANTS.SET_FILTER,
  data,
});

export const setUserType = data => ({
  type: CONSTANTS.SET_USER_TYPE,
  data,
});
