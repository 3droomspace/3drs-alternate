import {combineReducers} from 'redux';
import {createNavigationReducer} from 'react-navigation-redux-helpers';

import {RootNavigator} from '@Navigators/AppNavigation';
import {app} from './app';
import {auth} from '@Containers/onboarding/redux';
import {chat} from '@Containers/chat/redux';
import {userReports} from '@Containers/user-reporting/redux';

import {LOG_OUT} from '@Containers/onboarding/redux/constants';

const nav = createNavigationReducer(RootNavigator);

// combine reducers to build the state
const appReducer = combineReducers({
  nav,
  auth,
  app,
  chat,
  userReports,
});

const rootReducer = (state, action) => {
  if (action.type === LOG_OUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
