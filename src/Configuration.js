export const Configuration = {
  google_map_key: 'AIzaSyCyyfSMl88xGTwh9DXBIqXS67DUzLUOmZk',
  home: {
    tab_bar_height: 50,
    initial_show_count: 4,
    listing_item: {
      height: 130,
      offset: 15,
      saved: {
        position_top: 5,
        size: 25,
      },
    },
  },
  map: {
    origin: {
      latitude: 34.014668,
      longitude: -118.181305,
    },
    delta: {
      latitude: 0.5422,
      longitude: 0.5221,
    },
  },
  baseUri: 'https://account-d.docusign.com/oauth',
  // DOCUSIGN_INTEGRATION_KEY: '28d48e98-5385-4fd1-96e1-f2792ef947ef',
  // DOCUSIGN_SECRET_KEY: '50174b01-f3f0-4773-8637-288773c88f61',
  DOCUSIGN_INTEGRATION_KEY: 'f9083809-ec4e-4aee-b6b1-e81adcb6f276',
  DOCUSIGN_SECRET_KEY: '5a05ab9a-7f63-4470-960e-51ce2c0e2ff0',
  DOCUSIGN_LOGIN_URL: 'https://demo.docusign.net/restapi/v2/login_information',
  DOCUSIGN_USERINFO_URL: 'https://account-d.docusign.com/oauth/userinfo',
  DOCUSIGN_USERNAME: 'thomas.work10@gmail.como',
  DOCUSIGN_PASSWORD: 'Thomas1994',
  // serverUri: 'https://pacific-journey-22079.herokuapp.com/sendAgreement',
  serverUri: 'https://test-3drs-docusign.herokuapp.com/sendAgreement',
  // GET_PDF_BASE64_URL: 'http://localhost:3000/getPdf2Base64',
  // serverUri:
  //   'https://us-central1-d-room-space-firebase.cloudfunctions.net/app/sendAgreement',
  GET_PDF_BASE64_URL:
    'https://us-central1-d-room-space-firebase.cloudfunctions.net/app/getPdf2Base64',
};
