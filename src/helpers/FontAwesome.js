import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '@Assets/FontAwesomePro/selection.json';

export const FontIcon = createIconSetFromIcoMoon(
  icoMoonConfig,
  'ProFontAwesome',
  'ProFontAwesome.ttf',
);
