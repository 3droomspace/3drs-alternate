import {AsyncStorage} from 'react-native';
import {Buffer} from 'buffer';
import {Configuration} from '../Configuration';

export const uploadDocumentToServer = (
  requestSign,
  docFile,
  renterEmail = null,
  renterName = null,
  data,
  booking,
) => {
  return new Promise((resolve, reject) => {
    const formdata = new FormData();
    formdata.append('renterEmail', renterEmail);
    formdata.append('renterName', renterName);
    formdata.append('requestSign', requestSign);

    docFile.forEach(doc => {
      if (!doc.uri.startsWith('https://')) {
        formdata.append('docs', doc);
      }
    });
    fetch(Configuration.serverUri, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formdata,
    })
      .then(response => {
        console.log('response from server', response);

        resolve();
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const getOrRenewAccessToken = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('docusignAuth').then(docusignAuthStr => {
      const docusignAuth = docusignAuthStr ? JSON.parse(docusignAuthStr) : null;

      if (
        !docusignAuth ||
        !docusignAuth.access_token ||
        docusignAuth.expires_in + docusignAuth.updated >= Date.now()
      ) {
        if (docusignAuth && docusignAuth.refresh_token) {
          fetch(`${Configuration.baseUri}/token`, {
            method: 'POST',
            headers: {
              Authorization:
                'Basic ' +
                Buffer.from(
                  `${Configuration.DOCUSIGN_INTEGRATION_KEY}:${Configuration.DOCUSIGN_SECRET_KEY}`,
                ).toString('base64'),
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              grant_type: 'refresh_token',
              refresh_token: docusignAuth.refresh_token,
            }),
          })
            .then(resp => resp.json())
            .then(response => {
              response = {
                ...response,
                updated: Date.now(),
              };

              AsyncStorage.setItem(
                'docusignAuth',
                JSON.stringify(response),
              ).then(() => resolve(response.access_token));
            })
            .catch(err => {
              reject(err);
            });
        } else {
          resolve(null);
        }
      } else {
        resolve(docusignAuth.access_token);
      }
    });
  });
};
