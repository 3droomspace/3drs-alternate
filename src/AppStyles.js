import {Platform, StyleSheet, Dimensions} from 'react-native';
import {Configuration} from './Configuration';
import {isIphoneX} from 'react-native-iphone-x-helper';

const {width, height} = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const numColumns = 2;

export const AppStyles = {
  color: {
    main: '#2169c0',
    text: '#4b4f52',
    title: '#464646',
    subtitle: '#545454',
    categoryTitle: '#161616',
    tint: '#2169c0',
    description: '#bbbbbb',
    filterTitle: '#8a8a8a',
    starRating: '#2169c0',
    location: '#a9a9a9',
    white: 'white',
    facebook: '#4267b2',
    grey: 'grey',
    lightgrey: '#e2e2e2',
    greenBlue: '#2169c0',
    placeholder: '#a0a0a0',
    background: '#f2f2f2',
    blue: '#3293fe',
    border: '#e6e6e6',
  },
  fontSize: {
    title: 30,
    content: 20,
    normal: 16,
  },
  buttonWidth: {
    main: '70%',
  },
  textInputWidth: {
    main: '80%',
  },
  fontName: {
    main: 'System',
    bold: 'System',
  },
  borderRadius: {
    main: 25,
    small: 5,
  },
};

export const AppIcon = {
  container: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 8,
    marginRight: 10,
  },
  containerMapBtn: {
    backgroundColor: 'white',
    borderRadius: 20,
    paddingLeft: 8,
    paddingRight: 8,
    marginRight: 10,
  },
  containerAddListingBtn: {
    backgroundColor: 'white',
    borderRadius: 20,
    paddingLeft: 8,
    paddingRight: 8,
  },
  style: {
    tintColor: AppStyles.color.tint,
    width: 25,
    height: 25,
  },
  images: {
    home: require('../assets/icons/home.png'),
    calendar: require('../assets/icons/calendar.png'),
    categories: require('../assets/icons/categories.png'),
    chart: require('../assets/icons/chart.png'),
    collections: require('../assets/icons/collections.png'),
    compose: require('../assets/icons/compose.png'),
    filter: require('../assets/icons/filter.png'),
    filters: require('../assets/icons/filters.png'),
    heart: require('../assets/icons/heart.png'),
    heartFilled: require('../assets/icons/heart-filled.png'),
    map: require('../assets/icons/map.png'),
    search: require('../assets/icons/search.png'),
    renovation: require('../assets/icons/renovation.png'),
    review: require('../assets/icons/review.png'),
    list: require('../assets/icons/list.png'),
    starFilled: require('../assets/icons/star_filled.png'),
    // TODO
    starHalfFilled: require('../assets/icons/star_nofilled.png'),
    starNoFilled: require('../assets/icons/star_nofilled.png'),
    defaultUser: require('../assets/icons/default_user.jpg'),
    logout: require('../assets/icons/shutdown.png'),
    rightArrow: require('../assets/icons/right-arrow.png'),
    accountDetail: require('../assets/icons/account-detail.png'),
    wishlistFilled: require('../assets/icons/wishlist-filled.png'),
    orderDrawer: require('../assets/icons/order-drawer.png'),
    settings: require('../assets/icons/settings.png'),
    contactUs: require('../assets/icons/contact-us.png'),
    delete: require('../assets/icons/delete.png'),
    communication: require('../assets/icons/communication.png'),
    comment: require('../assets/icons/comment.png'),
    cameraFilled: require('../assets/icons/camera-filled.png'),
    send: require('../assets/icons/send.png'),
    boederImgSend: require('../assets/icons/borderImg1.png'),
    boederImgReceive: require('../assets/icons/borderImg2.png'),
    textBoederImgSend: require('../assets/icons/textBorderImg1.png'),
    textBoederImgReceive: require('../assets/icons/textBorderImg2.png'),
    emptyAvatar: require('../assets/icons/empty-avatar.jpg'),
    checklist: require('../assets/icons/checklist.png'),

    // Rental History
    artwork: require('../assets/Images/rental/artwork.png'),
  },
};

export const HeaderButtonStyle = StyleSheet.create({
  multi: {
    flexDirection: 'row',
  },
  container: {
    padding: 10,
  },
  image: {
    justifyContent: 'center',
    width: 35,
    height: 35,
    margin: 6,
  },
  rightButton: {
    color: AppStyles.color.tint,
    marginRight: 10,
    fontWeight: 'normal',
    fontFamily: AppStyles.fontName.main,
  },
});

export const ListStyle = StyleSheet.create({
  title: {
    fontSize: 16,
    color: AppStyles.color.subtitle,
    fontFamily: AppStyles.fontName.bold,
    fontWeight: 'bold',
  },
  subtitleView: {
    minHeight: 55,
    flexDirection: 'row',
    paddingTop: 5,
    marginLeft: 10,
  },
  leftSubtitle: {
    flex: 2,
  },
  time: {
    color: AppStyles.color.description,
    fontFamily: AppStyles.fontName.main,
    flex: 1,
    textAlignVertical: 'bottom',
  },
  place: {
    fontWeight: 'bold',
    color: AppStyles.color.location,
  },
  price: {
    flex: 1,
    fontSize: 14,
    color: AppStyles.color.subtitle,
    fontFamily: AppStyles.fontName.bold,
    textAlignVertical: 'bottom',
    alignSelf: 'flex-end',
    textAlign: 'right',
  },
  avatarStyle: {
    height: 80,
    width: 80,
  },
});

export const TwoColumnListStyle = {
  listings: {
    marginTop: 15,
    width: '100%',
    flex: 1,
  },
  showAllButtonContainer: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: AppStyles.color.greenBlue,
    height: 50,
    width: '100%',
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  showAllButtonText: {
    textAlign: 'center',
    color: AppStyles.color.greenBlue,
    fontFamily: AppStyles.fontName.bold,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
  listingItemContainer: {
    justifyContent: 'center',
    marginBottom: 20,
    marginRight: 7,
    width:
      (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns,
    backgroundColor: 'white',
  },
  listingItemContainer1: {
    justifyContent: 'center',
    marginBottom: 20,
    marginRight: 7,
    width: SCREEN_WIDTH * 0.93,
    backgroundColor: 'white',
  },
  mainConatiner: {
    shadowColor: '#8c8c8c',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.6,
    shadowRadius: 20.3,
    width: SCREEN_WIDTH,
  },
  mainConatiner1: {
    shadowRadius: 20.3,
  },
  photo: {
    // position: "absolute",
  },
  listingPhoto: {
    width:
      (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns,
    height: Configuration.home.listing_item.height,
  },
  listingPhoto1: {
    width: SCREEN_WIDTH * 0.93,
    height: Configuration.home.listing_item.height,
  },
  savedIcon: {
    position: 'absolute',
    top: Configuration.home.listing_item.saved.position_top,
    left:
      (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns -
      Configuration.home.listing_item.offset -
      Configuration.home.listing_item.saved.size,
    width: Configuration.home.listing_item.saved.size,
    height: Configuration.home.listing_item.saved.size,
  },
  savedIcon1: {
    position: 'absolute',
    top: Configuration.home.listing_item.saved.position_top,
    left:
      SCREEN_WIDTH * 0.93 -
      Configuration.home.listing_item.offset -
      Configuration.home.listing_item.saved.size,
    width: Configuration.home.listing_item.saved.size,
    height: Configuration.home.listing_item.saved.size,
  },
  dollar: {
    fontSize: 24,
    color: 'black',
    fontFamily: 'CircularStd-Book',
    marginTop: 5,
  },
  listingName: {
    fontSize: 17,
    fontFamily: 'CircularStd-Book',
    color: AppStyles.color.categoryTitle,
    marginTop: -4,
  },
  listingPlace: {
    fontFamily: 'CircularStd-Book',
    color: '#2a3ebd',
    fontSize: 12,
  },
  listingPlace1: {
    fontFamily: 'CircularStd-Book',
    color: '#808080',
    fontSize: 12,
    marginTop: -1,
  },
  listingPlace2: {
    fontFamily: 'CircularStd-Book',
    color: '#0c0c0c',
    fontSize: 13,
    marginTop: -3,
  },
  blueContainer: {
    backgroundColor: '#E7EAF7',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 4,
    paddingBottom: 4,
    alignSelf: 'flex-start',
    borderRadius: 100,
    marginTop: 8,
    marginLeft: -5,
  },
};

export const ModalSelectorStyle = {
  optionTextStyle: {
    color: AppStyles.color.subtitle,
    fontSize: 16,
    fontFamily: AppStyles.fontName.main,
  },
  selectedItemTextStyle: {
    fontSize: 18,
    color: AppStyles.color.blue,
    fontFamily: AppStyles.fontName.main,
    fontWeight: 'bold',
  },
  optionContainerStyle: {
    backgroundColor: AppStyles.color.white,
  },
  cancelContainerStyle: {
    backgroundColor: AppStyles.color.white,
    borderRadius: 10,
  },
  sectionTextStyle: {
    fontSize: 21,
    color: AppStyles.color.title,
    fontFamily: AppStyles.fontName.main,
    fontWeight: 'bold',
  },

  cancelTextStyle: {
    fontSize: 21,
    color: AppStyles.color.blue,
    fontFamily: AppStyles.fontName.main,
  },
};

export const ModalHeaderStyle = {
  bar: {
    height: 50,
    marginTop: Platform.OS === 'ios' ? 40 : 0,
    justifyContent: 'center',
  },
  title: {
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
    fontSize: 20,
    color: 'black',
    fontFamily: 'CircularStd-Book',
  },
  body: {
    marginTop: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
  },
  rightButton: {
    top: 0,
    right: 0,
    backgroundColor: 'transparent',
    alignSelf: 'flex-end',
    color: AppStyles.color.tint,
    fontWeight: 'normal',
    fontFamily: AppStyles.fontName.main,
  },
  leftButton: {
    top: 0,
    right: 0,
    backgroundColor: 'transparent',
    alignSelf: 'flex-start',
    color: AppStyles.color.tint,
    fontWeight: 'normal',
    fontFamily: AppStyles.fontName.main,
  },
};

export const CustomMapStyle = [
  {
    featureType: 'all',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#7c93a3',
      },
      {
        lightness: '-10',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#a0a4a5',
      },
    ],
  },
  {
    featureType: 'administrative.province',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#62838e',
      },
    ],
  },
  {
    featureType: 'landscape',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#dde3e3',
      },
    ],
  },
  {
    featureType: 'landscape.man_made',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#3f4a51',
      },
      {
        weight: '0.30',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'all',
    stylers: [
      {
        visibility: 'simplified',
      },
    ],
  },
  {
    featureType: 'poi.attraction',
    elementType: 'all',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'poi.business',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'poi.government',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'all',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'poi.place_of_worship',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'poi.school',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'poi.sports_complex',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'all',
    stylers: [
      {
        saturation: '-100',
      },
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#bbcacf',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [
      {
        lightness: '0',
      },
      {
        color: '#bbcacf',
      },
      {
        weight: '0.50',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'road.highway.controlled_access',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#ffffff',
      },
    ],
  },
  {
    featureType: 'road.highway.controlled_access',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#a9b4b8',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels.icon',
    stylers: [
      {
        invert_lightness: true,
      },
      {
        saturation: '-7',
      },
      {
        lightness: '3',
      },
      {
        gamma: '1.80',
      },
      {
        weight: '0.01',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'all',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#a3c7df',
      },
    ],
  },
];

export default AppStyles;
