import {sendGridEmail} from 'react-native-sendgrid';

const SENDGRIDAPIKEY =
  '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';
const FROMEMAIL = 'info@3droomspace.com';

const signupConfirmation = TOEMAIL => {
  const SUBJECT = 'Signup Successfully!';
  const ContactDetails =
    'As an Owner(Manager), you signed up successfully. You can use this account.';

  const sendRequest = sendGridEmail(
    SENDGRIDAPIKEY,
    TOEMAIL,
    FROMEMAIL,
    SUBJECT,
    ContactDetails,
  );
  sendRequest
    .then(response => {
      console.log('Email confirm successfully!');
    })
    .catch(error => {
      console.log(error);
    });
};

const VerifycationConfirmation = TOEMAIL => {
  const SUBJECT = 'ID Verification Successfully!';
  const ContactDetails =
    'Your ID was successfully verified. You can use your account safely.';

  const sendRequest = sendGridEmail(
    SENDGRIDAPIKEY,
    TOEMAIL,
    FROMEMAIL,
    SUBJECT,
    ContactDetails,
  );

  sendRequest
    .then(response => {
      console.log('Email sent successfully');
    })
    .catch(error => {
      console.log(error);
    });
};

const contactSupportConfirmation = (TOEMAIL, DATA) => {
  const SUBJECT = 'You Contacted Support Successfully!';
  const ContactDetails =
    'We will review your request and get back to you shortly.';

  const sendRequest = sendGridEmail(
    SENDGRIDAPIKEY,
    TOEMAIL,
    FROMEMAIL,
    SUBJECT,
    ContactDetails,
  );
  sendRequest
    .then(response => {
      console.log('Success Verification Email sent');
    })
    .catch(error => {
      console.log(error);
    });
};

const deleteListingConfirmation = TOEMAIL => {
  const SUBJECT = 'You deleted your listing successfully';
  const ContactDetails = 'Thanks for using 3DRoomSpace!';

  const sendRequest = sendGridEmail(
    SENDGRIDAPIKEY,
    TOEMAIL,
    FROMEMAIL,
    SUBJECT,
    ContactDetails,
  );
  sendRequest
    .then(response => {
      console.log('Email sent successfully');
    })
    .catch(error => {
      console.log(error);
    });
};

const sendGridManager = {
  signupConfirmation,
  contactSupportConfirmation,
  deleteListingConfirmation,
  VerifycationConfirmation,
};

export default sendGridManager;
