// const Client = require('@vouched.id/vouched-nodejs').default;
// const client = Client('ix@DQ-42!bbJFJH5eURGHm1k9*~A9W');

// const utils = require('@vouched.id/vouched-nodejs').utils;

import ImgToBase64 from 'react-native-image-base64';
import axios from 'axios';

const private_key = 'ix@DQ-42!bbJFJH5eURGHm1k9*~A9W';

var userPhotoBase64 = '';
var idPhotoBase64 = '';

const idVerification = async (
  userPhoto,
  idPhoto,
  firstName,
  lastName,
  internal_id,
  internal_username,
  email,
) => {
  userPhotoBase64 = await getBase64String(userPhoto);
  idPhotoBase64 = await getBase64String(idPhoto);

  const data = {
    type: 'id-verification',
    properties: [
      {
        name: internal_id,
        value: internal_username,
      },
    ],
    params: {
      userPhoto: userPhotoBase64,
      idPhoto: idPhotoBase64,
      firstName: firstName,
      lastName: lastName,
      email: email,
    },
  };

  // return 1;

  return new Promise((resolve, reject) => {
    axios
      .post('https://verify.vouched.id/api/jobs', data, {
        headers: {
          'X-Api-Key': 'ix@DQ-42!bbJFJH5eURGHm1k9*~A9W',
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => reject(err));
  });
};

function getBase64String(data) {
  return new Promise((resolve, reject) => {
    ImgToBase64.getBase64String(data)
      .then(base64String => resolve(base64String))
      .catch(err => reject(err));
  });
}

const idVerificationManager = {
  idVerification,
};

export default idVerificationManager;
