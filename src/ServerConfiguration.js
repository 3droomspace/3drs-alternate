/**
 * If isApprovalProcessEnabled is set to true,
 * you are advised to visit your firebase console,
 * under your users collection and a particular document id,
 * add the field "isAdmin" of type boolean
 * with a value of true, to give a user an admin power of
 * approving listings.
 */

const ServerConfiguration = {
  isApprovalProcessEnabled: false,
  database: {
    collection: {
      LISTINGS: 'real_estate_listings',
      SAVED_LISTINGS: 'real_estate_saved_listings',
      CATEGORIES: 'real_estate_categories',
      FILTERS: 'real_estate_filters',
      REVIEWS: 'real_estate_reviews',
      REVIEWS_USERS: 'users_reviews',
      BOOKINGS: 'bookings',
      USERS: 'users',
      CHANNELS: 'channels',
      CHANNEL_PARTICIPATION: 'channel_participation',
    },
  },
};

export default ServerConfiguration;
