import React, {useEffect, useState} from 'react';
import {Provider} from 'react-redux';
import {AppRegistry} from 'react-native';
import {initialMode, eventEmitter} from 'react-native-dark-mode';
import * as RNLocalize from 'react-native-localize';

import {setI18nConfig} from '@Containers/localization/IMLocalization';
import configureStore from '@Redux/store';
import {AppNavigator} from '@Navigators/AppNavigation';

// Integrate stripe
import stripe from 'tipsi-stripe';
import {STRIPE_PUBLISH_KEY} from './keys';
stripe.setOptions({
  publishableKey: STRIPE_PUBLISH_KEY,
  androidPayMode: 'test',
});

const store = configureStore();

const App = props => {
  const [mode, setMode] = useState(initialMode);
  const [forceUpdate, setForceUpdate] = useState(false);

  useEffect(() => {
    console.disableYellowBox = true;
    setI18nConfig();
    RNLocalize.addEventListener('change', handleLocalizationChange);
    eventEmitter.on('currentModeChanged', handleModeChange);
    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
      eventEmitter.removeListener('currentModeChanged', handleModeChange);
    };
  }, []);

  const handleModeChange = m => {
    setMode(m);
  };

  const handleLocalizationChange = () => {
    setI18nConfig();
    setForceUpdate(val => !val);
  };

  return (
    <Provider store={store}>
      <AppNavigator screenProps={{theme: mode}} forceUpdate={forceUpdate} />
    </Provider>
  );
};

App.propTypes = {};

App.defaultProps = {};

AppRegistry.registerComponent('App', () => App);

export default App;
